﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;
using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.ControlFlowGraphAnalysis;

namespace TestByKA
{
    class Program
    {
        private static GraphBB MakeBagCFG()
        {
            LinkedList<Addr3Command> commands = new LinkedList<Addr3Command>();
            commands.AddLast(new Addr3Command(true, "cond", "L1"));
            commands.AddLast(new Addr3Command(true, true, "L2", "L1"));
            commands.AddLast(new Addr3Command(true, true, "L1", "L2"));
            GraphBB cfg = BasicBlocksTools.MakeBasicBlockGraph(commands);
            return cfg;

        }

        static void Main(string[] args)
        {
            Console.WriteLine(CFGAnalysisTools.IsReducible(MakeBagCFG()));
            Console.ReadLine();
        }
    }
}
