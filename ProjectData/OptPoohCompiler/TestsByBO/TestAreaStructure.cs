﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.AreaBasedAnalysis;
using OptPoohCompiler.AreaBasedAnalysis.Areas;

namespace TestsByBO
{
    /// <summary>
    /// Тест для структуры областей
    /// </summary>
    class TestAreaStructure
    {
        /// <summary>
        /// B0 -> B1
        /// B1 -> B2, B3
        /// B2 -> B3, B4
        /// B3 -> B4, B1
        /// </summary>
        public string Example1()
        {
            // формируем граф CFG
            GraphBBVertex[] vB = new GraphBBVertex[5];
            for (int i = 0; i < vB.Length; ++i)
                vB[i] = new GraphBBVertex(
                    new BasicBlock(string.Format("B{0}", i + 1)));
            BasicBlocksTools.ConnectVertices(vB[0], vB[1]);
            BasicBlocksTools.ConnectVertices(vB[1], vB[2]);
            BasicBlocksTools.ConnectVertices(vB[1], vB[3]);
            BasicBlocksTools.ConnectVertices(vB[2], vB[3]);
            BasicBlocksTools.ConnectVertices(vB[2], vB[4]);
            BasicBlocksTools.ConnectVertices(vB[3], vB[4]);
            BasicBlocksTools.ConnectVertices(vB[3], vB[1]);
            GraphBB cfg = new GraphBB(vB, vB[0]);

            // получаем первый граф
            VertexArea[] vAreas1 = new VertexArea[vB.Length];
            for (int i = 0; i < vAreas1.Length; ++i)
                vAreas1[i] = new VertexArea(
                    new AreaBasicBlock(string.Format("R{0}", i + 1), vB[i]));
            AreasTools.ConnectVertices(vAreas1[0], vAreas1[1]);
            AreasTools.ConnectVertices(vAreas1[1], vAreas1[2]);
            AreasTools.ConnectVertices(vAreas1[1], vAreas1[3]);
            AreasTools.ConnectVertices(vAreas1[2], vAreas1[3]);
            AreasTools.ConnectVertices(vAreas1[2], vAreas1[4]);
            AreasTools.ConnectVertices(vAreas1[3], vAreas1[4]);
            AreasTools.ConnectVertices(vAreas1[3], vAreas1[1]);
            vAreas1[4].IsOut = true;
            
            // меняем граф: R1, R2, R3 делаем областью тела
            VertexArea[] R6Areas = new VertexArea[3];
            R6Areas[0] = vAreas1[1];
            R6Areas[1] = vAreas1[2];
            R6Areas[2] = vAreas1[3];
            R6Areas[1].IsOut = true;
            R6Areas[2].IsOut = true;
            R6Areas[0].InVerticies.Clear();
            R6Areas[2].OutVerticies.Clear();
            R6Areas[1].OutVerticies.Remove(vAreas1[4]);
            GraphArea graphR6 = new GraphArea(R6Areas, R6Areas[0]);
            AreaBody R6 = new AreaBody("R6", graphR6);
            VertexArea vertexR6 = new VertexArea(R6);

            // R6 превращаем в R7 (петлю прячем)
            AreasTools.ConnectVertices(vertexR6, vertexR6);
            AreaCycle R7 = new AreaCycle("R7", vertexR6);
            VertexArea vertexR7 = new VertexArea(R7);

            // получаем второй граф R1 -> R7 -> R5
            VertexArea[] vAreas2 = new VertexArea[3];
            vAreas2[0] = vAreas1[0];
            vAreas2[1] = vertexR7;
            vAreas2[2] = vAreas1[4];
            vAreas2[0].OutVerticies.Clear();
            AreasTools.ConnectVertices(vAreas2[0], vAreas2[1]);
            vAreas2[2].InVerticies.Clear();
            AreasTools.ConnectVertices(vAreas2[1], vAreas2[2]);

            // последний граф-точка
            GraphArea graphR8 = new GraphArea(vAreas2, vAreas2[0]);
            AreaBody R8 = new AreaBody("R8", graphR8);

            HashSet<GraphBBVertex> B4Ancestors = R6.GetSubAreaAncestors(R6Areas[2]);
            HashSet<GraphBBVertex> B5Ancestors = R8.GetSubAreaAncestors(vAreas2[2]);
            HashSet<GraphBBVertex> R6Outputs = R6.GetOutBlockVertices();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("B4 <-");
            foreach (GraphBBVertex vert in B4Ancestors)
                sb.Append(vert.Data.Name + " ");
            sb.AppendLine();
            sb.AppendLine("B5 <-");
            foreach (GraphBBVertex vert in B5Ancestors)
                sb.Append(vert.Data.Name + " ");
            sb.AppendLine();
            sb.AppendLine("R6 outs");
            foreach (GraphBBVertex vert in R6Outputs)
                sb.Append(vert.Data.Name + " ");
            return sb.ToString();
        }

        public GraphBB MakeGraph1()
        {
            // формируем граф CFG
            GraphBBVertex[] vB = new GraphBBVertex[5];
            for (int i = 0; i < vB.Length; ++i)
                vB[i] = new GraphBBVertex(
                    new BasicBlock(string.Format("B{0}", i + 1)));
            BasicBlocksTools.ConnectVertices(vB[0], vB[1]);
            BasicBlocksTools.ConnectVertices(vB[1], vB[2]);
            BasicBlocksTools.ConnectVertices(vB[1], vB[3]);
            BasicBlocksTools.ConnectVertices(vB[2], vB[3]);
            BasicBlocksTools.ConnectVertices(vB[2], vB[4]);
            BasicBlocksTools.ConnectVertices(vB[3], vB[4]);
            BasicBlocksTools.ConnectVertices(vB[3], vB[1]);
            GraphBB cfg = new GraphBB(vB, vB[0]);
            return cfg;
        }

        public Area MakeArea1()
        {
            // формируем граф CFG
            GraphBBVertex[] vB = new GraphBBVertex[5];
            for (int i = 0; i < vB.Length; ++i)
                vB[i] = new GraphBBVertex(
                    new BasicBlock(string.Format("B{0}", i + 1)));
            BasicBlocksTools.ConnectVertices(vB[0], vB[1]);
            BasicBlocksTools.ConnectVertices(vB[1], vB[2]);
            BasicBlocksTools.ConnectVertices(vB[1], vB[3]);
            BasicBlocksTools.ConnectVertices(vB[2], vB[3]);
            BasicBlocksTools.ConnectVertices(vB[2], vB[4]);
            BasicBlocksTools.ConnectVertices(vB[3], vB[4]);
            BasicBlocksTools.ConnectVertices(vB[3], vB[1]);
            GraphBB cfg = new GraphBB(vB, vB[0]);

            // получаем первый граф
            VertexArea[] vAreas1 = new VertexArea[vB.Length];
            for (int i = 0; i < vAreas1.Length; ++i)
                vAreas1[i] = new VertexArea(
                    new AreaBasicBlock(string.Format("R{0}", i + 1), vB[i]));
            AreasTools.ConnectVertices(vAreas1[0], vAreas1[1]);
            AreasTools.ConnectVertices(vAreas1[1], vAreas1[2]);
            AreasTools.ConnectVertices(vAreas1[1], vAreas1[3]);
            AreasTools.ConnectVertices(vAreas1[2], vAreas1[3]);
            AreasTools.ConnectVertices(vAreas1[2], vAreas1[4]);
            AreasTools.ConnectVertices(vAreas1[3], vAreas1[4]);
            AreasTools.ConnectVertices(vAreas1[3], vAreas1[1]);
            vAreas1[4].IsOut = true;

            // меняем граф: R1, R2, R3 делаем областью тела
            VertexArea[] R6Areas = new VertexArea[3];
            R6Areas[0] = vAreas1[1];
            R6Areas[1] = vAreas1[2];
            R6Areas[2] = vAreas1[3];
            R6Areas[1].IsOut = true;
            R6Areas[2].IsOut = true;
            R6Areas[0].InVerticies.Clear();
            R6Areas[2].OutVerticies.Clear();
            R6Areas[1].OutVerticies.Remove(vAreas1[4]);
            GraphArea graphR6 = new GraphArea(R6Areas, R6Areas[0]);
            AreaBody R6 = new AreaBody("R6", graphR6);
            VertexArea vertexR6 = new VertexArea(R6);

            // R6 превращаем в R7 (петлю прячем)
            AreasTools.ConnectVertices(vertexR6, vertexR6);
            AreaCycle R7 = new AreaCycle("R7", vertexR6);
            VertexArea vertexR7 = new VertexArea(R7);

            // получаем второй граф R1 -> R7 -> R5
            VertexArea[] vAreas2 = new VertexArea[3];
            vAreas2[0] = vAreas1[0];
            vAreas2[1] = vertexR7;
            vAreas2[2] = vAreas1[4];
            vAreas2[0].OutVerticies.Clear();
            AreasTools.ConnectVertices(vAreas2[0], vAreas2[1]);
            vAreas2[2].InVerticies.Clear();
            AreasTools.ConnectVertices(vAreas2[1], vAreas2[2]);

            // последний граф-точка
            GraphArea graphR8 = new GraphArea(vAreas2, vAreas2[0]);
            AreaBody R8 = new AreaBody("R8", graphR8);
            return R8;
        }

        public void TestAreaBasedAnalysis1()
        {
            GraphBB cfg = MakeGraph1();
            Area outerArea = MakeArea1();
            AvailableExprsAreaBasedAlgo algo = new AvailableExprsAreaBasedAlgo(cfg);
            algo.SetOuterArea(outerArea);
            algo.Execute();
        }

    }
}
