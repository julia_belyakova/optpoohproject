﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestsByBO
{
    class Program
    {
        /// <summary>
        /// Класс с тестами для глубинного остовного дерева и дуг
        /// </summary>
        private static TestDFSTAnsArcs arcsAndDFSTTester = new TestDFSTAnsArcs();

        static void Main(string[] args)
        {
            Console.WriteLine(arcsAndDFSTTester.TestBagCFG());
            Console.ReadLine();
            TestAreaStructure testArea = new TestAreaStructure();
            Console.WriteLine(testArea.Example1());
            Console.ReadLine();
            testArea.TestAreaBasedAnalysis1();
            Console.ReadLine();
        }
    }
}
