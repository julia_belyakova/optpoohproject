﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;
using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.ControlFlowGraphAnalysis;

namespace TestsByBO
{
    /// <summary>
    /// Тесты для глубинного остовного дерева и анализа дуг
    /// </summary>
    class TestDFSTAnsArcs
    {
        /// <summary>
        /// Неприводимый граф
        /// </summary>
        public string TestBagCFG()
        {
            /*
                 if cond goto L1;
             L2: goto L1;
             L1: goto L2;
             */
            GraphBB cfg = MakeBagCFG();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(cfg.DFST.ToString());
            DFSTAndArcsCFGData<BasicBlock, GraphBBVertex, GraphBB> arcsData = 
                new DFSTAndArcsCFGData<BasicBlock, GraphBBVertex, GraphBB>(cfg);
            sb.AppendLine(arcsData.ToString());
            return sb.ToString();
        }

        private GraphBB MakeBagCFG()
        {
            LinkedList<Addr3Command> commands = new LinkedList<Addr3Command>();
            commands.AddLast(new Addr3Command(true, "cond", "L1"));
            commands.AddLast(new Addr3Command(true, true, "L2", "L1"));
            commands.AddLast(new Addr3Command(true, true, "L1", "L2"));
            GraphBB cfg = BasicBlocksTools.MakeBasicBlockGraph(commands);
            return cfg;

        }
    }
}
