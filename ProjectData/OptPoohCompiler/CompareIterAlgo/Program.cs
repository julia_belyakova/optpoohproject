﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace CompareIterAlgo
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            IterAlgoChecker algoChecker = new IterAlgoChecker();
            while (true)
            {
                Console.WriteLine("Enter 'quit' if you would like to quit, else select file");
                string answer = Console.ReadLine();
                if (answer == "quit")
                {
                    Console.WriteLine("Bye!");
                    return;
                }
                if (openFile.ShowDialog() == DialogResult.OK)
                {
                    string result;
                    if (!algoChecker.InitFile(openFile.FileName, out result))
                    {
                        Console.WriteLine(result);
                        continue;
                    }
                    Console.WriteLine("Good numeration: {0}", algoChecker.ExecIterAlgoGoodSort());
                    Console.WriteLine("Bad numeration: {0}", algoChecker.ExecIterAlgoBadSort());
                }
            }
        }
    }
}
