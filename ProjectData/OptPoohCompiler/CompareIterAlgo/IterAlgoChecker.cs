﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using OptPoohCompiler.ParserScanner;
using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.DataFlowAnalysis;
using OptPoohCompiler.DataFlowAnalysis.AvailableExpressions;

namespace CompareIterAlgo
{
    /// <summary>
    /// Запускатель итерационного алгоритма с разной нумерацией вершин
    /// </summary>
    class IterAlgoChecker
    {
        /// <summary>
        /// Менеджер парсера
        /// </summary>
        private PascalParserManager pascalParserManager = new PascalParserManager();

        /// <summary>
        /// Граф базовых блоков
        /// </summary>
        private GraphBB cfg;

        /// <summary>
        /// Запускатель итерационного алгоритма с разной нумерацией вершин
        /// </summary>
        public IterAlgoChecker()
        { }

        /// <summary>
        /// Пытается распарсить программу на языке паскаль из файла fileName
        /// </summary>
        /// <param name="srcFileName">Файл исходного кода на Паскале</param>
        /// <returns>Истину, если удалось распарсить</returns>
        public bool InitFile(string srcFileName, out string message)
        {
            if (!File.Exists(srcFileName))
            {
                message = "Файл с именем '" + srcFileName + "' не существует";
                return false;
            }
            try
            {
                pascalParserManager.ReadSourceCode(srcFileName);
                var b = pascalParserManager.Parse();
                if (!b)
                {
                    message = "Не удалось распарсить файл";
                    return false;
                }
                cfg = BasicBlocksTools.MakeBasicBlockGraph(
                    (pascalParserManager.GetProgramTreeRoot()).GetAddr3Code());
                message = "OK";
                return true;
            }
            catch (Exception e)
            {
                message = "Неожиданная ошибка" + Environment.NewLine + e.ToString();
                return false; 
            }
        }

        /// <summary>
        /// Выполняет алгоритм в порядке топологической сортировки
        /// </summary>
        /// <returns>Число итераций</returns>
        public int ExecIterAlgoGoodSort()
        {
            AvailableExprIterAlgo algo = new AvailableExprIterAlgo(cfg);
            algo.Execute();
            return algo.StepsCount;
        }

        /// <summary>
        /// Выполняет алгоритм в порядке топологической сортировки
        /// </summary>
        /// <returns>Число итераций</returns>
        public int ExecIterAlgoBadSort()
        {
            AvailableExprIterAlgo algo = new AvailableExprIterAlgo(cfg);
            int[] backNumeration = new int[cfg.Verticies.Count];
            for (int i = 0; i < backNumeration.Length; ++i)
                backNumeration[i] = backNumeration.Length - i - 1;
            algo.SetVertexNumeration(backNumeration);
            algo.Execute();
            return algo.StepsCount;
        }

    }
}
