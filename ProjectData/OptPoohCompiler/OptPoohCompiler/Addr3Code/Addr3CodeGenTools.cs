﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OptPoohCompiler.ProgramTree;
using System.Windows.Forms;

namespace OptPoohCompiler.Addr3Code
{
    /// <summary>
    /// Вспомогательный класс для генерации трехадресного кода
    /// </summary>
    public static class Addr3CodeGenTools
    {
        /// Имя метки
        private const string LABEL_NAME = "L";
        /// Имя временной переменной
        private const string TEMP_VAR_NAME = "%t";

        /// Номер последней выданной метки
        private static int currLabelNum = 0;
        /// Номер последней выданной временной переменной
        private static int currTempVarNum = 0;

        /// <summary>
        /// Генерирует новое имя метки
        /// </summary>
        /// <returns>Имя для метки, которое еще не использовалось</returns>
        public static string NextLabelName()
        {
            ++currLabelNum;
            return (LABEL_NAME + currLabelNum.ToString());
        }
        /// <summary>
        /// Генерирует новое имя временной переменной
        /// </summary>
        /// <returns>Имя для временной переменной, которое еще не использовалось</returns>
        public static string NextTempVarName()
        {
            ++currTempVarNum;
            return (TEMP_VAR_NAME + currTempVarNum.ToString());
        }

        /// <summary>
        /// Сбрасывает генератор имен меток в начальное значение
        /// </summary>
        public static void ResetLabelGenerator()
        {
            currLabelNum = 0;
        }
        /// <summary>
        /// Сбрасывает генератор имен переменных в начальное значение
        /// </summary>
        public static void ResetTempVarGenerator()
        {
            currTempVarNum = 0;
        }

        /// <summary>
        /// Определяет значение оператора трехадресного кода,
        /// соответствующее унарной операции
        /// </summary>
        /// <param name="unOp">Унарный оператор</param>
        /// <returns>Оператор трехадресного кода</returns>
        public static Addr3CmdOperator GetUnaryOperator(UnaryArithmOperator unOp)
        {
            return (unOp == UnaryArithmOperator.UPLUS)
                ? Addr3CmdOperator.PLUS
                : Addr3CmdOperator.MINUS;
        }
        /// <summary>
        /// Определяет значение оператора трехадресного кода,
        /// соответствующее бинарной операции умножения
        /// </summary>
        /// <param name="multOp">Бинарный оператор умножения</param>
        /// <returns>Оператор трехадресного кода</returns>
        public static Addr3CmdOperator GetBinaryMultOperator(ArithmMultOperator multOp)
        {
            return (multOp == ArithmMultOperator.MULT)
                ? Addr3CmdOperator.MULT
                : Addr3CmdOperator.DIVIDE;
        }
        /// <summary>
        /// Определяет значение оператора трехадресного кода,
        /// соответствующее бинарной операции сложения
        /// </summary>
        /// <param name="plusOp">Бинарный оператор сложения</param>
        /// <returns>Оператор трехадресного кода</returns>
        public static Addr3CmdOperator GetBinaryPlusOperator(ArithmPlusOperator plusOp)
        {
            return (plusOp == ArithmPlusOperator.PLUS)
                ? Addr3CmdOperator.PLUS
                : Addr3CmdOperator.MINUS;
        }
        /// <summary>
        /// Определяет значение оператора трехадресного кода,
        /// соответствующее оператору отношения
        /// </summary>
        /// <param name="relOp">Оператор отношения</param>
        /// <returns>Оператор трехадресного кода</returns>
        public static Addr3CmdOperator GetRelationOperator(Relation relOp)
        {
            switch (relOp)
            {
                case Relation.LESS: return Addr3CmdOperator.LESS;
                case Relation.LESS_EQUAL: return Addr3CmdOperator.LESS_EQUAL;
                case Relation.GREATER: return Addr3CmdOperator.GREATER;
                case Relation.GREATER_EQUAL: return Addr3CmdOperator.GREATER_EQUAL;
                case Relation.EQUAL: return Addr3CmdOperator.EQUAL;
                default: return Addr3CmdOperator.NOT_EQUAL;
            }
        }

        /// <summary>
        /// Возвращает текст, соответствующий трехадресному коду
        /// </summary>
        /// <param name="code">Список команд трехадресного кода</param>
        /// <returns>Текстовое представление кода</returns>
        public static string GetAddr3CodeText(LinkedList<Addr3Command> code)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Addr3Command cmd in code)
                sb.AppendLine(cmd.ToString());
            return sb.ToString();
        }

        /// <summary>
        /// Приклеивает второй двусвязный список к первому
        /// </summary>
        /// <typeparam name="T">Тип элементов в списке</typeparam>
        /// <param name="first">Первый список -- получатель</param>
        /// <param name="second">Второй список -- приклеиваемый</param>
        public static void MergeLinkedLists<T>(LinkedList<T> first, LinkedList<T> second)
        {
            int secondLen = second.Count;
            for (int i = 0; i < secondLen; ++i)
            {
                var elem = second.First();
                second.RemoveFirst();
                first.AddLast(elem);
            }
        }

        /// <summary>
        /// Подготавливает трехадресный код для бинарного выражения
        /// </summary>
        /// <param name="left">Первый аргумент-выражение</param>
        /// <param name="right">Второй аргумент-выражение</param>
        /// <param name="op">Оператор</param>
        /// <param name="code">Список трехадресных команд</param>
        /// <param name="currHolder">Имя всего выражения</param>
        public static void PrepareBinExprAddr3Code(ExprNode left, ExprNode right, Addr3CmdOperator op, out LinkedList<Addr3Command> code, out string currHolder)
        {
            // получаем код и имена подвыражений
            Addr3CodeExprData subData1 = left.GetAddr3CodeData();
            Addr3CodeExprData subData2 = right.GetAddr3CodeData();
            // склеиваем код подвыражений
            code = subData1.code;
            Addr3CodeGenTools.MergeLinkedLists(code, subData2.code);
            // пусть t[n] -- имя выражения, хранящегося в left,
            // t[m] -- right
            // нужно добавить трехадресную команду
            // t[new] = t[n] <Operator> t[m]
            currHolder = Addr3CodeGenTools.NextTempVarName();
            Addr3Command binOpCmd = new Addr3Command(currHolder, 
                subData1.holder, subData2.holder, op);
            code.AddLast(binOpCmd);
        }


        /// <summary>
        /// Убирает лишние разбиения на BIN_OPERATOR и ASSIGN
        /// </summary>
        /// <param name="code">код</param>
        /// <returns>испарвленный код</returns>
        public static LinkedList<Addr3Command> MergeExtraOperators(LinkedList<Addr3Command> code)
        {
            LinkedList<Addr3Command> newCode = new LinkedList<Addr3Command>();
            var f = false;
            int i;
            for (i = 1; i < code.Count; ++i)
            {
                var cmd1 = code.ElementAt(i - 1);
                var cmd2 = code.ElementAt(i);
                if (cmd1.cmdType == Addr3CmdType.BIN_OPERATOR && cmd2.cmdType == Addr3CmdType.ASSIGN && cmd2.arg1 == cmd1.result && cmd2.arg1[0] == '%' && cmd2.arg2 == "")
                {
                    newCode.AddLast(new Addr3Command(true, cmd1.cmdLabel, cmd2.result, cmd1.arg1, cmd1.arg2, cmd1.cmdOperator));
                    ++i;
                    f = true;
                }
                else
                {
                    newCode.AddLast(cmd1);
                    f = false;
                }
            }
            //if (code.Last().cmdOperator == Addr3CmdOperator.EMPTY && code.Last().HasLabel() )
            //if (code.Last().cmdType != Addr3CmdType.ASSIGN )
            //f (f)
                //newCode.AddLast(code.Last());
            if (i == code.Count)
                newCode.AddLast(code.Last());
            return newCode;
        }

        /// <summary>
        /// Стирает пустые операторы
        /// </summary>
        /// <param name="code">код</param>
        /// <returns>испарвленный код</returns>
        public static LinkedList<Addr3Command> RemoveEmptyCommands(LinkedList<Addr3Command> code)
        {
            
            LinkedList<Addr3Command> newCode = new LinkedList<Addr3Command>();
            for (int i = 0; i < code.Count - 1; i++)
            {
                //MessageBox.Show("");
                var cmd = code.ElementAt(i);
                if (cmd.cmdType == Addr3CmdType.NOP)
                {
                    
                    var oldCmdLabel = cmd.cmdLabel;
                    string newCmdLabel;
                    if (code.ElementAt(i + 1).cmdLabel != "")
                        newCmdLabel = code.ElementAt(i + 1).cmdLabel;
                    else
                    {
                        newCmdLabel = Addr3CodeGenTools.NextLabelName();
                        var nextCmd = code.ElementAt(i + 1);
                        code.Remove(nextCmd);
                        nextCmd.cmdLabel = newCmdLabel;
                        code.AddAfter(code.Find(cmd),nextCmd);
                    }
                    //code.ElementAt(i + 1).cmdLabel = newCmdLabel;

                    for (int j = 0; j < code.Count - 1; j++)
                    {
                        if (code.ElementAt(j).result == oldCmdLabel)
                        {
                            var curCmd = code.ElementAt(j);
                            curCmd.result = newCmdLabel;
                            code.Remove(code.ElementAt(j));
                            code.AddBefore(code.Find(code.ElementAt(j)), curCmd);
                        }
                    }
                    code.Remove(cmd);
                }
            }
            return code;
        }

    }
}
