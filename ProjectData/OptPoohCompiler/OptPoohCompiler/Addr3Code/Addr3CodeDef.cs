﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.Addr3Code
{

    /// <summary>
    /// Тип трехадресной команды
    /// </summary>
    /// <value UNKNOWN = > sakjasd </value>
    public enum Addr3CmdType { UNKNOWN, NOP, BIN_OPERATOR, UNARY_OPERATOR, ASSIGN, IF, GOTO };

    /// <summary>
    /// Оператор в трехадресной команде
    /// </summary>
    public enum Addr3CmdOperator { EMPTY, PLUS, MINUS, MULT, DIVIDE,
        LESS, LESS_EQUAL, GREATER, GREATER_EQUAL, EQUAL, NOT_EQUAL };
    /// <summary>
    /// Вспомогательный класс для ипа оператора 3AC-команды Addr3CmdOperator
    /// </summary>
    public static class Addr3CmdOperatorTools
    {
        /// <summary>
        /// Определяет строковое описание оператора 3AC-команды
        /// </summary>
        /// <param name="op">Оператор 3AC-команды</param>
        /// <returns>Строковое описание 3AC-команды</returns>
        public static string ToString(Addr3CmdOperator op)
        {
            switch (op)
            {
                case Addr3CmdOperator.PLUS: return "+";
                case Addr3CmdOperator.MINUS: return "-";
                case Addr3CmdOperator.MULT: return "*";
                case Addr3CmdOperator.DIVIDE: return "/";
                case Addr3CmdOperator.LESS: return "<";
                case Addr3CmdOperator.LESS_EQUAL: return "<=";
                case Addr3CmdOperator.GREATER: return ">";
                case Addr3CmdOperator.GREATER_EQUAL: return ">=";
                case Addr3CmdOperator.EQUAL: return "==";
                case Addr3CmdOperator.NOT_EQUAL: return "!=";
                default: return "<empty>";
            }
        }
    }

    /// <summary>
    /// Трехадресная команда
    /// </summary>
    public struct Addr3Command
    {
        private const string ASSIGN_STR_VAL = " = ";

        /// <summary>
        /// Метка команды
        /// </summary>
        public string cmdLabel;
        /// <summary>
        /// Тип команды
        /// </summary>
        public Addr3CmdType cmdType;
        /// <summary>
        /// Результат команды (левая часть присваивания или метка в if-goto/goto)
        /// </summary>
        public string result;
        /// <summary>
        /// Первый операнд (правая часть присваивания или условие в if-goto)
        /// </summary>
        public string arg1;
        /// <summary>
        /// Второй операнд (правая часть присваивания с оператором)
        /// </summary>
        public string arg2;
        /// <summary>
        /// Оператор в команде присваивания с оператором
        /// </summary>
        public Addr3CmdOperator cmdOperator;
        /// <summary>
        /// Вспомогательная информация для команды
        /// </summary>
        public IAddr3CommandInfo info;

        /// <summary>
        /// Трехадресная команда со значениями по умолчанию
        /// </summary>
        private Addr3Command(bool empty)
        {
            this.cmdLabel = "";
            this.cmdType = Addr3CmdType.UNKNOWN;
            this.result = "";
            this.arg1 = "";
            this.arg2 = "";
            this.cmdOperator = Addr3CmdOperator.EMPTY;
            this.info = null;
        }

        /// <summary>
        /// Трехадресная команда (полный конструктор)
        /// </summary>
        /// <param name="cmdLabel">Метка команды</param>
        /// <param name="cmdType">Тип команды</param>
        /// <param name="result">Результат команды (левая часть присваивания или метка в if-goto/goto)</param>
        /// <param name="arg1">Первый операнд (правая часть присваивания или условие в if-goto)</param>
        /// <param name="arg2">Второй операнд (правая часть присваивания с оператором)</param>
        /// <param name="cmdOperator">Оператор в команде присваивания с оператором</param>
        /// <param name="gotoLabel">Вспомогательная информация для команды</param>
        public Addr3Command(string cmdLabel, Addr3CmdType cmdType, 
            string val1, string val2, string val3, Addr3CmdOperator cmdOperator)
        {
            this.cmdLabel = cmdLabel;
            this.cmdType = cmdType;
            this.result = val1;
            this.arg1 = val2;
            this.arg2 = val3;
            this.cmdOperator = cmdOperator;
            this.info = null;
        }

        /// <summary>
        /// Трехадресная команда NOP
        /// </summary>
        /// <param name="isNop">Фиктивный параметр для правильной перегрузки конструктора</param>
        public Addr3Command(int isNop)
            : this(true)
        {
            this.cmdType = Addr3CmdType.NOP;
        }
        /// <summary>
        /// Трехадресная команда NOP c меткой
        /// </summary>
        /// <param name="hasLabel">Фиктивный параметр для правильной перегрузки конструктора</param>
        /// <param name="cmdLabel">Метка команды</param>
        /// <param name="isNop">Фиктивный параметр для правильной перегрузки конструктора</param>
        public Addr3Command(bool hasLabel, string cmdLabel, int isNop)
            : this(isNop)
        {
            this.cmdLabel = cmdLabel;
        }

        /// <summary>
        /// Трехадресная команда ASSIGN
        /// </summary>
        /// <param name="left">Переменная слева</param>
        /// <param name="right">Аргумент справа</param>
        public Addr3Command(string left, string right)
            : this(true)
        {
            this.cmdType = Addr3CmdType.ASSIGN;
            this.result = left;
            this.arg1 = right;
        }
        /// <summary>
        /// Трехадресная команда ASSIGN
        /// </summary>
        /// <param name="hasLabel">Фиктивный параметр для правильной перегрузки конструктора</param>
        /// <param name="cmdLabel">Метка команды</param>
        /// <param name="left">Переменная слева</param>
        /// <param name="right">Аргумент справа</param>
        public Addr3Command(bool hasLabel, string cmdLabel, string left, string right)
            : this(left, right)
        {
            this.cmdLabel = cmdLabel;
        }

        /// <summary>
        /// Трехадресная команда X = op Z
        /// </summary>
        /// <param name="left">Переменная слева</param>
        /// <param name="right">Аргумент справа</param>
        /// <param name="unarOp">Унарный оператор</param>
        public Addr3Command(string left, string right, Addr3CmdOperator unarOp)
            : this(true)
        {
            cmdType = Addr3CmdType.UNARY_OPERATOR;
            this.result = left;
            this.arg1 = right;
            this.cmdOperator = unarOp;
        }
        /// <summary>
        /// Трехадресная команда X = op Z
        /// </summary>
        /// <param name="hasLabel">Фиктивный параметр для правильной перегрузки конструктора</param>
        /// <param name="cmdLabel">Метка команды</param>
        /// <param name="left">Переменная слева</param>
        /// <param name="right">Аргумент справа</param>
        /// <param name="unarOp">Унарный оператор</param>
        public Addr3Command(bool hasLabel, string cmdLabel, string left, string right, Addr3CmdOperator unarOp)
            : this(left, right, unarOp)
        {
            this.cmdLabel = cmdLabel;
        }

        /// <summary>
        /// Трехадресная команда X = Y op Z
        /// </summary>
        /// <param name="left">Переменная слева</param>
        /// <param name="right1">Первый аргумент справа</param>
        /// <param name="right2">Второй аргумент справа</param>
        /// <param name="binOp">Бинарный оператор</param>
        public Addr3Command(string left, string right1, string right2, Addr3CmdOperator binOp)
            : this(true)
        {
            cmdType = Addr3CmdType.BIN_OPERATOR;
            this.result = left;
            this.arg1 = right1;
            this.arg2 = right2;
            this.cmdOperator = binOp;
        }
        /// <summary>
        /// Трехадресная команда X = Y op Z
        /// </summary>
        /// <param name="hasLabel">Фиктивный параметр для правильной перегрузки конструктора</param>
        /// <param name="cmdLabel">Метка команды</param>
        /// <param name="left">Переменная слева</param>
        /// <param name="right1">Первый аргумент справа</param>
        /// <param name="right2">Второй аргумент справа</param>
        /// <param name="binOp">Бинарный оператор</param>
        public Addr3Command(bool hasLabel, string cmdLabel, string left, string right1, string right2, Addr3CmdOperator binOp)
            : this(left, right1, right2, binOp)
        {
            this.cmdLabel = cmdLabel;
        }
        
        /// <summary>
        /// Трехадресная команда IF
        /// </summary>
        /// <param name="hasLabel">Фиктивный параметр для правильной перегрузки конструктора</param>
        /// <param name="cond">Переменная-условие</param>
        /// <param name="gotoLabel">Метка перехода в goto</param>
        public Addr3Command(bool isIf, string cond, string gotoLabel)
            : this(true)
        {
            cmdType = Addr3CmdType.IF;
            this.result = gotoLabel;
            this.arg1 = cond;
        }
        /// <summary>
        /// Трехадресная команда IF
        /// </summary>
        /// <param name="hasLabel">Фиктивный параметр для правильной перегрузки конструктора</param>
        /// <param name="cmdLabel">Метка команды</param>
        /// <param name="cond">Переменная-условие</param>
        /// <param name="gotoLabel">Метка перехода в goto</param>
        public Addr3Command(bool isIf, bool hasLabel, string cmdLabel, string cond, string gotoLabel)
            : this(true, cond, gotoLabel)
        {
            this.cmdLabel = cmdLabel;
        } 
        
        /// <summary>
        /// Трехадресная команда GOTO
        /// </summary>
        /// <param name="gotoLabel">Метка перехода в goto</param>
        public Addr3Command(bool isGoto, string gotoLabel)
            : this(true)
        {
            this.cmdType = Addr3CmdType.GOTO;
            this.result = gotoLabel;
        }
        /// <summary>
        /// Трехадресная команда GOTO  с меткой
        /// </summary>
        /// <param name="hasLabel">Фиктивный параметр для правильной перегрузки конструктора</param>
        /// <param name="cmdLabel">Метка команды</param>
        /// <param name="gotoLabel">Метка перехода в goto</param>
        public Addr3Command(bool isGoto, bool hasLabel, string cmdLabel, string gotoLabel)
            : this(true, gotoLabel)
        {
            this.cmdLabel = cmdLabel;
        }

        /// <summary>
        /// Проверяет, имеет ли команда метку
        /// </summary>
        /// <returns>Истину, если команда имеет метку</returns>
        public bool HasLabel()
        {
            return this.cmdLabel != "";
        }

        public bool HasGoto()
        {
            return (this.cmdType == Addr3CmdType.GOTO)
                || (this.cmdType == Addr3CmdType.IF);
        }

        public bool HasAssign()
        {
            return (this.cmdType == Addr3CmdType.ASSIGN)
                || (this.cmdType == Addr3CmdType.UNARY_OPERATOR)
                || (this.cmdType == Addr3CmdType.BIN_OPERATOR);
        }

        /// <summary>
        /// Оборачивает строковое значение в пробелы
        /// </summary>
        /// <param name="val">Некоторая строка</param>
        /// <param name="both">Если истина, то пробелы добавляются с двух сторон, иначе только после</param>
        /// <returns>Значение, обернутое пробелами</returns>
        private string _WrapInSpaces(string val, bool both = true)
        {
            return (both ? " " : "") + (val + " ");
        }

        public override string ToString()
        {
            string value = "";
            switch (cmdType)
            {
                case Addr3CmdType.NOP:
                    value = "nop";
                    break;
                case Addr3CmdType.ASSIGN: 
                    value = result + ASSIGN_STR_VAL + arg1;
                    break;
                case Addr3CmdType.UNARY_OPERATOR:
                    value = result + ASSIGN_STR_VAL
                        + _WrapInSpaces(Addr3CmdOperatorTools.ToString(this.cmdOperator), false)
                        + arg1;
                    break;
                case Addr3CmdType.BIN_OPERATOR:
                    value = result + ASSIGN_STR_VAL 
                        + arg1
                        + _WrapInSpaces(Addr3CmdOperatorTools.ToString(this.cmdOperator))
                        + arg2;
                    break;
                case Addr3CmdType.IF:
                    value = "if " + arg1 + " goto " + result;
                    break;
                case Addr3CmdType.GOTO:
                    value = "goto " + result;
                    break;
                default:
                    value = "<unknown>";
                    break;
            }
            return String.Format("{0,4}: {1}\n", cmdLabel, value);
        }
 
    }
}