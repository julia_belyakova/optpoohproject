﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.Addr3Code
{
    /// <summary>
    /// Структура с информацией, необходимой при генерации 
    /// трехадресного кода для выражений
    /// </summary>
    public struct Addr3CodeExprData
    {
        /// Список команд
        public LinkedList<Addr3Command> code;
        /// Имя, в котором хранится выражение 
        public string holder;

        /// <summary>
        /// Трехадресный код для выражений (конструктор без параметров)
        /// <param name="code">Список команд</param>
        /// <param name="holder">Имя, в котором хранится выражение </param>
        /// </summary>
        public Addr3CodeExprData(LinkedList<Addr3Command> code, string holder)
        {
            this.code = code;
            this.holder = holder;
        }
    }
}
