﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

namespace OptPoohCompiler.GraphTools
{
    /// <summary>
    /// Граф
    /// </summary>
    /// <typeparam name="Vertex">Тип вершин</typeparam>
    /// <typeparam name="AnalysisData">Тип данных в вершине</typeparam>
    public class Graph<Vertex, Data> : IGraph<Vertex, Data>
        where Vertex : IVertex<Vertex, Data>
    {
        /// <summary>
        /// Все вершины графа
        /// </summary>
        private HashSet<Vertex> vertices;
        /// <summary>
        /// Входная вершина графа
        /// </summary>
        private Vertex inputVertex;

        /// <summary>
        /// Перенумерованные вершины графа в порядке топологической сортировки
        /// </summary>
        private List<Vertex> sortedVerticies;
        /// <summary>
        /// Глубинное остовное дерево графа
        /// </summary>
        private DepthFirstSearchTree<Vertex, Data> dfst;

        /// <summary>
        /// Входная вершина графа
        /// </summary>
        public Vertex InputVertex
        {
            get { return this.inputVertex; }
        }

        /// <summary>
        /// Все вершины графа в порядке формирования базовых блоков
        /// </summary>
        public HashSet<Vertex> Verticies
        {
            get { return this.vertices; }
        }
        /// <summary>
        /// Все вершины графа в порядке топологической сортировки
        /// </summary>
        public List<Vertex> SortedVerticies
        {
            get { return this.sortedVerticies; }
        }

        /// <summary>
        /// Глубинное остовное дерево графа
        /// </summary>
        public DepthFirstSearchTree<Vertex, Data> DFST
        {
            get { return dfst; }
        }

        /// <summary>
        /// Граф базовых блоков
        /// </summary>
        public Graph(ICollection<Vertex> vertexCollection, Vertex inputVertex)
        {
            if (vertexCollection == null)
                throw new ArgumentNullException("Список вершин не может быть null");
            if (inputVertex == null)
                throw new ArgumentNullException("Входная вершина не может быть null");
            if (!vertexCollection.Contains(inputVertex))
                throw new ArgumentNullException("Входная вершина должна принадлежать графу (набору vertexCollection)");
            this.vertices = new HashSet<Vertex>(vertexCollection);
            this.inputVertex = inputVertex;
            _SortVertices();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (sortedVerticies.Count == 0)
                sb.AppendLine("<Граф пуст>");
            else
            {
                for (int i = 0; i < sortedVerticies.Count - 1; ++i)
                    sb.AppendLine("(" + i.ToString() + ") " 
                        + sortedVerticies[i].ToString() + Environment.NewLine);
                sb.AppendLine("(" + (sortedVerticies.Count - 1).ToString() + ") "
                    + sortedVerticies[sortedVerticies.Count - 1].ToString());
            }
            return sb.ToString();
        }

        /// <summary>
        /// Информация, необходимая для сортировки вершин
        /// </summary>
        private class VertexSortInfo
        {
            public bool visited;
            public int number;

            public VertexSortInfo()
            {
                visited = false;
                number = -1;
            }
        }

        /// <summary>
        /// Выполняет топологическую сортировку графа
        /// </summary>
        private void _SortVertices()
        {
            // глубинное остовное дерево
            dfst = new DepthFirstSearchTree<Vertex, Data>();
            // метки посещения вершин
            Dictionary<Vertex, VertexSortInfo> visitInfo = new Dictionary<Vertex, VertexSortInfo>(vertices.Count);
            // сначала все вершины не помечены
            foreach (Vertex vertex in vertices)
                visitInfo.Add(vertex, new VertexSortInfo());

            int n = vertices.Count - 1;
            // запускаем алгоритм для каждой вершины
            foreach (Vertex vertex in vertices)
                _SortVerticesAuxiliary(visitInfo, vertex, ref n);
            Debug.Assert(n == -1, "Неправильный алгоритм топологической сортировки");
            Vertex[] sorted = new Vertex[vertices.Count];
            foreach (Vertex vertex in vertices)
                sorted[visitInfo[vertex].number] = vertex;
            sortedVerticies = sorted.ToList();
        }

        private void _SortVerticesAuxiliary(Dictionary<Vertex, VertexSortInfo> visitInfo,
            Vertex currVertex, ref int currN)
        {
            if (visitInfo[currVertex].visited)
                return;
            visitInfo[currVertex].visited = true;
            foreach (Vertex descendant in currVertex.OutVerticies)
            {
                if (!visitInfo[descendant].visited)
                {
                    dfst.AddArc(currVertex, descendant);
                    _SortVerticesAuxiliary(visitInfo, descendant, ref currN);
                }
            }
            visitInfo[currVertex].number = currN;
            --currN;
        }
    }
}
