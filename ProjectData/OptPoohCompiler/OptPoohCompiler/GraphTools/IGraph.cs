﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.GraphTools
{
    /// <summary>
    /// Граф
    /// </summary>
    /// <typeparam name="Vertex">Тип вершин</typeparam>
    /// <typeparam name="AnalysisData">Тип значений в вершинах</typeparam>
    public interface IGraph<Vertex, Data>
        where Vertex : IVertex<Vertex, Data>
    {
        /// <summary>
        /// Все вершины графа в порядке формирования
        /// </summary>
        HashSet<Vertex> Verticies { get; }
        /// <summary>
        /// Все вершины графа в порядке топологической сортировки
        /// </summary>
        List<Vertex> SortedVerticies { get; }
        /// <summary>
        /// Входная вершина графа
        /// </summary>
        Vertex InputVertex { get; }

        /// <summary>
        /// Глубинное остовное дерево графа
        /// </summary>
        DepthFirstSearchTree<Vertex, Data> DFST { get; }
    }
}
