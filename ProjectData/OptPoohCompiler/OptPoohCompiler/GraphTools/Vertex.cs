﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.GraphTools
{
    /// <summary>
    /// Вершина графа
    /// </summary>
    /// <typeparam name="VertexValueType">Тип данных в вершине</typeparam>
    public class Vertex<D>
    {
        // ---------------------------------------------- Поля

        /// <summary>
        /// Данные в вершине
        /// </summary>
        protected D data;

        /// <summary>
        /// Вершины-потомки (исходящие дуги)
        /// </summary>
        protected HashSet<Vertex<D>> outVerticies = new HashSet<Vertex<D>>();
        /// <summary>
        /// Вершины-предшественники (входящие дуги)
        /// </summary>
        protected HashSet<Vertex<D>> inVerticies = new HashSet<Vertex<D>>();

        // ---------------------------------------------- Свойства

        /// <summary>
        /// Данные в вершине
        /// </summary>
        public D Data
        {
            get { return this.data; }
        }

        /// <summary>
        /// Список выходных дуг
        /// (Не использовать для изменения списка!)
        /// </summary>
        public HashSet<Vertex<D>> OutVerticies
        {
            get { return this.outVerticies; }
        }
        /// <summary>
        /// Список входных дуг
        /// (Не использовать для изменения списка!)
        /// </summary>
        public HashSet<Vertex<D>> InVerticies
        {
            get { return this.inVerticies; }
        }

        // ---------------------------------------------- Конструкторы

        /// <summary>
        /// Вершина графа 
        /// </summary>
        /// <param name="data">Данные в вершине</param>
        public Vertex(D data)
        {
            this.data = data;
        }

        // ---------------------------------------------- Методы

        /// <summary>
        /// Добавляет входящую дугу (ссылку на вершину-предшественника)
        /// </summary>
        /// <param name="vertex">Вершина-предшестенник</param>
        public virtual void AddInVertex(Vertex<D> vertex)
        {
            this.inVerticies.Add(vertex);
        }

        /// <summary>
        /// Добавляет исходящую дугу (ссылку на вершину-потомка).
        /// Исходящих дуг не может быть больше двух.
        /// </summary>
        /// <param name="vertex">Вершина-потомок</param>
        public virtual void AddOutVertex(Vertex<D> vertex)
        {
            this.outVerticies.Add(vertex);
        }

        /// <summary>
        /// Короткая строка-описатель вершины
        /// </summary>
        /// <returns>Короткую-строку описатель</returns>
        public string ShortString()
        {
            return this.ToString();
        }
    }
}
