﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

namespace OptPoohCompiler.GraphTools
{
    /// <summary>
    /// Глубинное остовное дерево
    /// </summary>
    public class DepthFirstSearchTree<Vertex, Data>
        where Vertex : IVertex<Vertex, Data>
    {
        /// <summary>
        /// Дуги остовного дерева
        /// </summary>
        private Dictionary<Vertex, HashSet<Vertex>> arcs;

        /// <summary>
        /// Глубинное остовное дерево
        /// </summary>
        public DepthFirstSearchTree()
        {
            arcs = new Dictionary<Vertex, HashSet<Vertex>>();
        }

        /// <summary>
        /// Добавляет в глубинное остовное дерево дугу source -> dest
        /// </summary>
        /// <param name="source">Вершина-источник</param>
        /// <param name="dest">Вершина-приёмник</param>
        public void AddArc(Vertex source, Vertex dest)
        {
            HashSet<Vertex> arcsFromSource;
            if (arcs.ContainsKey(source))
                arcsFromSource = arcs[source];
            else
            {
                arcsFromSource = new HashSet<Vertex>();
                arcs.Add(source, arcsFromSource);
            }
            Debug.Assert(!arcsFromSource.Contains(dest),
                "AddArc: Глубинное остовное дерево уже содрежит данную дугу");
            arcsFromSource.Add(dest);
        }

        /// <summary>
        /// Проверяет, содержит ли глубинное остовное дерево дугу source -> dest
        /// </summary>
        /// <param name="source">Вершина-источник</param>
        /// <param name="dest">Вершина-приёмник</param>
        /// <returns>Истину, если такая дуга есть в дереве</returns>
        public bool ContainsArc(Vertex source, Vertex dest)
        {
            if (!arcs.ContainsKey(source))
                return false;
            return arcs[source].Contains(dest);
        }

        /// <summary>
        /// Проверяет, является ли вершина first предком вершины по
        /// глубинному остовному дереву
        /// </summary>
        /// <param name="first">Вершина графа</param>
        /// <param name="second">Вершина графа</param>
        /// <returns>Истину, если на дереве есть путь от first к dest</returns>
        public bool FirstIsAncestorOfSecond(Vertex first, Vertex second)
        {
            if (ContainsArc(first, second))
                return true;
            bool pathFromDescendant = false;
            if (arcs.ContainsKey(first))
                foreach (Vertex desc in arcs[first])
                    pathFromDescendant = pathFromDescendant
                        || FirstIsAncestorOfSecond(desc, second);
            return pathFromDescendant;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<Vertex, HashSet<Vertex>> sourceArcs in arcs)
                foreach (Vertex dest in sourceArcs.Value)
                    sb.AppendLine(string.Format("DFST Arc: {0} -> {1}",
                        sourceArcs.Key.ShortString(), dest.ShortString()));
            return sb.ToString();
        }
    }
}
