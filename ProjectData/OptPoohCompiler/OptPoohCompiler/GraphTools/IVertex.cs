﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.GraphTools
{
    /// <summary>
    /// Вершина графа V со значением типа VertexValueType
    /// </summary>
    /// <typeparam name="V">Тип вершины</typeparam>
    /// <typeparam name="VertexValueType">Тип данных в вершине</typeparam>
    public interface IVertex<V, D> 
        where V : IVertex<V, D>
    {
        /// <summary>
        /// Данные в вершине
        /// </summary>
        D Data { get; }

        /// <summary>
        /// Список выходных дуг
        /// (Не использовать для изменения списка!)
        /// </summary>
        HashSet<V> OutVerticies { get; }
        /// <summary>
        /// Список входных дуг
        /// (Не использовать для изменения списка!)
        /// </summary>
        HashSet<V> InVerticies { get; }

        /// <summary>
        /// Входные данные анализа
        /// </summary>
        object IN { get; set; }
        /// <summary>
        /// Выходные данные анализа
        /// </summary>
        object OUT { get; set; }

        /// <summary>
        /// Добавляет входящую дугу (ссылку на вершину-предшественника)
        /// </summary>
        /// <param name="vertex">Вершина-предшестенник</param>
        void AddInVertex(V vertex);

        /// <summary>
        /// Добавляет исходящую дугу (ссылку на вершину-потомка).
        /// Исходящих дуг не может быть больше двух.
        /// </summary>
        /// <param name="vertex">Вершина-потомок</param>
        void AddOutVertex(V vertex);

        /// <summary>
        /// Короткая строка-описатель вершины
        /// </summary>
        /// <returns>Короткую-строку описатель</returns>
        string ShortString();
    }
}
