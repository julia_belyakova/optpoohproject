﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;
using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.DataFlowAnalysis
{
    /// <summary>
    /// Передаточная функция для данных анализа-множества
    /// </summary>
    /// <typeparam name="InSetAnalysisData">Тип элементов множества данных анализа</typeparam>
    /// <typeparam name="AnalysisData">Тип множества данных анализа</typeparam>
    /// <typeparam name="AuxD">Тип вспомогательных данных, необходимых для передаточной функции</typenamtypeparam>
    public class SetTransferFunction<InSetAnalysisData, AnalysisData, AuxD> : ITransferFunction<AnalysisData, Addr3Command, AuxD>, ITransferFunction<AnalysisData, BasicBlock, AuxD>
        where AnalysisData : SetDFADataType<InSetAnalysisData>, IDFADataType<AnalysisData>, new()
    {
        /// <summary>
        /// Добавляемое множество в формуле
        /// </summary>
        protected AnalysisData addingSet;
        /// <summary>
        /// Отнимаемое множество в формуле
        /// </summary>
        protected AnalysisData subtractingSet;

        /// <summary>
        /// Добавляемое множество в формуле
        /// </summary>
        public AnalysisData AddingSet
        {
            get { return addingSet; }
        }
        /// <summary>
        /// Отнимаемое множество в формуле
        /// </summary>
        public AnalysisData SubtractingSet
        {
            get { return subtractingSet; }
        }

        public virtual void GlobalInitialize(AuxD globalData)
        { 
        
        }

        /// <summary>
        /// Инициализирует множества для трехадресной команды owner
        /// </summary>
        /// <param name="owner">Трехадресная команда</param>
        public virtual void Initialize(Addr3Command cmd)
        {
            addingSet = new AnalysisData();
            // TODO
            subtractingSet = new AnalysisData();
            // TODO
            throw new NotImplementedException();
        }

        public virtual void Initialize(BasicBlock basicBlock)
        {
            throw new NotImplementedException();
        }

        public virtual AnalysisData Apply(AnalysisData x)
        {
            // если множества формулы инициализированы, то здесь просто 
            // нужно применить формулу
            // иначе нужно применять передаточные функции команд последовательно
            AnalysisData result = new AnalysisData();
            result.UnionWith(x);
            result.ExceptWith(subtractingSet);
            result.UnionWith(addingSet);
            return result;
        }
    }
}
