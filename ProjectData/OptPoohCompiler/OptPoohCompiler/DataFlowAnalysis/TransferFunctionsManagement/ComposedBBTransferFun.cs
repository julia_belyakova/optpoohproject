﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;
using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.DataFlowAnalysis
{
    /// <summary>
    /// Передаточная функция базового блока -- композиция передаточных
    /// функций его команд
    /// </summary>
    /// <typeparam name="TF">Тип передаточной функции для команды</typeparam>
    /// <typeparam name="AD">Данные анализа</typeparam>
    /// <typeparam name="InitDT">Данные инициализации каждой передаточной функции</typeparam>
    public class ComposedBBTransferFun<TF, AD, InitDT> : ITransferFunction<AD, BasicBlock, InitDT>
        where AD : IDFADataType<AD>
        where TF : ITransferFunction<AD, Addr3Command, InitDT>, new()
    {
        /// <summary>
        /// Собственно композиция функций
        /// </summary>
        private TFComposition<TF, AD, Addr3Command, InitDT> composedFun;
        /// <summary>
        /// Глобальные данные для передаточного блока
        /// </summary>
        private InitDT globalData;

        /// <summary>
        /// Передаточная функция базового блока -- композиция 
        /// передаточных функций команд
        /// </summary>
        public ComposedBBTransferFun()
        {
            this.composedFun = new TFComposition<TF, AD, Addr3Command, InitDT>();
        }

        /// <summary>
        /// Инициализация глобальными данными
        /// </summary>
        /// <param name="globalData">Данные для передаточной функции</param>
        public void GlobalInitialize(InitDT globalData)
        {
            this.globalData = globalData;
        }

        /// <summary>
        /// Инициализация данными базового блока
        /// </summary>
        /// <param name="owner">Базовый блок-владелец функции</param>
        public void Initialize(BasicBlock owner)
        {
            foreach (Addr3Command cmd in owner.Body)
            {
                TF subFun = DataFlowAnalysisTools.ConstructTransferFunction<AD, Addr3Command, InitDT, TF>(
                    this.globalData, cmd);
                composedFun.Add(subFun);
            }
        }

        /// <summary>
        /// Применяет передаточную функцию к данным x
        /// </summary>
        /// <param name="x">Данные анализа</param>
        /// <returns>Преобразованные данные анализа</returns>
        public AD Apply(AD x)
        {
            return composedFun.Apply(x);
        }
    }
}
