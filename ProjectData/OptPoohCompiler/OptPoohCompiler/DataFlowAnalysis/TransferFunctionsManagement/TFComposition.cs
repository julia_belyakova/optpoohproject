﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.DataFlowAnalysis
{
    /// <summary>
    /// Композиция передаточных функций типа TF
    /// </summary>
    /// <typeparam name="TF">Тип передаточной функции</typeparam>
    /// <typeparam name="AD">Тип данных анализа</typeparam>
    /// <typeparam name="SubNodeType">Узлы привязки передаточных функций</typeparam>
    /// <typeparam name="InitDT">Данные инициализации</typeparam>
    public class TFComposition<TF, AD, SubNodeType, InitDT>
        where AD : IDFADataType<AD>
        where TF : ITransferFunction<AD, SubNodeType, InitDT>
    {
        /// <summary>
        /// Передаточные функции из композиции в порядке
        /// от внутренней к внешней: a, b, c >>> c(b(a(x)))
        /// </summary>
        private LinkedList<TF> subTransferFuns;

        /// <summary>
        /// Композиция передаточных функций
        /// </summary>
        public TFComposition()
        {
            this.subTransferFuns = new LinkedList<TF>();
        }

        /// <summary>
        /// Добавляет передаточную функцию в композицию
        /// на внешний уровень
        /// </summary>
        /// <param name="subTransferFun">Передаточная функция</param>
        public void Add(TF subTransferFun)
        {
            this.subTransferFuns.AddLast(subTransferFun);
        }

        public AD Apply(AD x)
        {
            AD y = x;
            foreach (TF f in subTransferFuns)
                y = f.Apply(y);
            return y;
        }
    }
}
