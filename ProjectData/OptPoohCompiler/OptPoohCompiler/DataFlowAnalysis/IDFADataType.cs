﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.DataFlowAnalysis
{
    /// <summary>
    /// Тип данных анализа потока данных
    /// </summary>
    /// <typeparam name="AnalysisData">Тип данных анализа</typeparam>
    public interface IDFADataType<AnalysisData> : IEquatable<AnalysisData>
        where AnalysisData : IDFADataType<AnalysisData>
    {
        /// <summary>
        /// Оператор сбора двух элементов анализа данных
        /// </summary>
        /// <param name="other">Данные анализа потока данных</param>
        /// <returns>Данные анализа потока данных</returns>
        AnalysisData Join(AnalysisData other);
    }
}
