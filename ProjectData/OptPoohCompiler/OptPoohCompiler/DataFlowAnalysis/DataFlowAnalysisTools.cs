﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.DataFlowAnalysis.DFAApplyers;

namespace OptPoohCompiler.DataFlowAnalysis
{
    /// <summary>
    /// Направление аналази данных
    /// </summary>
    public enum AnalysisDirection { DOWN, UP };

    /// <summary>
    /// Средства анализа потоков данных
    /// </summary>
    public static class DataFlowAnalysisTools
    {
        /// Все доступные глобальные оптимизации
        public static IDFAApplyer[] AllDataFlowAnalysis = { 
                  new AvailableExprIterAlgoApplyer(), new LiveVarIterAlgoApplyer(), new ConstPropagationIterAlgoApplier(),
                  new ReachDefsIterAlgoApplyer()
        };

        /// <summary>
        /// Конструирует передаточную функцию для заданного элемента
        /// </summary>
        /// <typeparam name="AnalysisData">Тип данных анализа</typeparam>
        /// <typeparam name="VertexValueType">Тип объекта-владельца передаточной функции</typeparam>
        /// <typeparam name="AuxD">Тип вспомогательных данных, необходимых для передаточной функции</typeparam>
        /// <typeparam name="F">Тип семейства передаточных функций</typeparam>
        /// <param name="data">Элемент-владелец передаточной функции</param>
        /// <returns>Передточную функцию элемента data</returns>
        public static F ConstructTransferFunction<AD, D, AuxD, F>(AuxD globalData, D data)
            where AD : IDFADataType<AD>
            where F : ITransferFunction<AD, D, AuxD>, new()
        {
            F transferFunc = new F();
            transferFunc.GlobalInitialize(globalData);
            transferFunc.Initialize(data);
            return transferFunc;
        }

        /// <summary>
        /// Оператор сбора нескольких входных элементов
        /// </summary>
        /// <typeparam name="DT">Тип данных анализа</typeparam>
        /// <param name="inputs">Набор элементов анализа потока данных</param>
        /// <returns>Собранный элемент анализа потока данных</returns>
        public static DT Join<DT>(ICollection<DT> inputs)
            where DT : IEquatable<DT>, IDFADataType<DT>
        {
            if (inputs.Count == 0)
                throw new ArgumentException("Оператор сболра должен получить хотя бы один вход");
            IEnumerator<DT> node = inputs.GetEnumerator();
            node.MoveNext();
            DT result = node.Current;
            while (node.MoveNext())
                result.Join(node.Current);
            return result;
        }

    }
}
