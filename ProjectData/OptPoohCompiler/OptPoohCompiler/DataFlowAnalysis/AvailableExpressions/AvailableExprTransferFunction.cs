﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.OptimizationsInBB.SharedExpressions;
using OptPoohCompiler.Addr3Code;

namespace OptPoohCompiler.DataFlowAnalysis.AvailableExpressions
{
    /// <summary>
    /// Передаточная функция для задачи о доступных выражениях
    /// </summary>
    public class AvailableExprTransferFunction : SetTransferFunction<SharedExpression, AvailableExprDFADataType, HashSet<SharedExpression>>
    {
        /// <summary>
        /// Все выражения, встречающиеся в графе
        /// </summary>
        private HashSet<SharedExpression> allGraphExprs;

        /// <summary>
        /// Передаточная функция задачи о доступных выражениях
        /// </summary>
        public AvailableExprTransferFunction()
        {
            // e_gen[C]
            addingSet = new AvailableExprDFADataType();
            // e_kill[C]
            subtractingSet = new AvailableExprDFADataType();
        }

        /// <summary>
        /// Инициализирует множество всех выражений, встречающихся в графе
        /// </summary>
        /// <param name="allGraphExprs">Множество всех выражений графа</param>
        public override void GlobalInitialize(HashSet<SharedExpression> allGraphExprs)
        {
            this.allGraphExprs = allGraphExprs;
        }

        /// <summary>
        /// Инициализирует множества e_gen[C], e_kill[C] 
        /// для трехадресной команды cmd
        /// </summary>
        /// <param name="cmd">Трехадресная команда</param>
        public override void Initialize(Addr3Command cmd)
        {
            // если нет присваивания -- нам не интересно
            if (!cmd.HasAssign())
                return;
            // если это не в точности команда присваивания
            if (cmd.cmdType != Addr3CmdType.ASSIGN) {
                // выражение в присваивании
                SharedExpression expr = new SharedExpression(cmd);
                // если левая часть присваивания не содержится в выражении,
                // то это выражение должно попасть в e_gen[C]
                if (!expr.ContainsVariable(cmd.result))
                    addingSet.Add(expr);
            }
            // в e_kill[C] должны попасть все выражения, которые содержат
            // левую часть присваивания
            // берем все выражения, которые убиваются текущей командой присваивания
            HashSet<SharedExpression> killedByExpr = new HashSet<SharedExpression>(
                allGraphExprs.Where(expr => expr.ContainsVariable(cmd.result)));
            // добавляем эти выражение во множество e_kill[B]
            subtractingSet.UnionWith(killedByExpr);
        }

        /// <summary>
        /// Инициализирует передаточную функцию базовым блоком
        /// </summary>
        /// <param name="basicBlock"></param>
        public override void Initialize(BasicBlock basicBlock)
        {
            // обрабатываем все команды с присваиванием
            foreach (Addr3Command cmd in basicBlock.Body)
                if (cmd.HasAssign())
                    _ProcessCmdInBasicBlock(cmd);
        }

        /// <summary>
        /// Обрабатывает трехадресную команду с присваиванием при построении
        /// передаточной функции для базового блока
        /// </summary>
        /// <param name="cmd">Трехадресная команда с присваиванием</param>
        private void _ProcessCmdInBasicBlock(Addr3Command cmd)
        {
            // это команда с выражением
            if (cmd.cmdType != Addr3CmdType.ASSIGN)
            {
                // выражение в присваивании
                SharedExpression expr = new SharedExpression(cmd);
                // добавляем его в e_gen[B]
                addingSet.Add(expr);
                // поскольку выражение вычислено, нужно убрать его из 
                // e_kill[B]
                subtractingSet.Remove(expr);
            }
            // нужно убрать из e_gen[B] все выражения, в которых встречается 
            // переменная из левой части команды
            addingSet.RemoveWhere(expr => expr.ContainsVariable(cmd.result));
            // берем все выражения, которые убиваются текущей командой присваивания
            HashSet <SharedExpression> killedByExpr = new HashSet<SharedExpression>(
                allGraphExprs.Where(expr => expr.ContainsVariable(cmd.result)));
            // добавляем эти выражение во множество e_kill[B]
            subtractingSet.UnionWith(killedByExpr);
        }
    }
}
