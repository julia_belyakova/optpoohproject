﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.OptimizationsInBB.SharedExpressions;
using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.Addr3Code;

namespace OptPoohCompiler.DataFlowAnalysis.AvailableExpressions
{
    /// <summary>
    /// Итерационный алгоритм в задаче о доступных выражениях
    /// </summary>
    public class AvailableExprIterAlgo : IterativeAlgorithm<
        AvailableExprDFADataType, BasicBlock, HashSet<SharedExpression>, AvailableExprTransferFunction, 
        GraphBBVertex, GraphBB>
    {
        /// <summary>
        /// Итерационный алгоритм в задаче о доступных выражениях
        /// </summary>
        /// <param name="graph">Граф базовых блоков</param>
        public AvailableExprIterAlgo(GraphBB graph)
            : base(graph)
        { 
               
        }

        /// <summary>
        /// Инициализация значения GlobalData, Top, InitVal 
        /// </summary>
        protected override void Initialize()
        {
            _CollectAllExpressions();
            this.InitVal = new AvailableExprDFADataType();
            this.Top = new AvailableExprDFADataType();
            this.Top.UnionWith(this.GlobalData);
        }

        /// <summary>
        /// Собирает во множество глобальных данных все выражения
        /// в графе ББл
        /// </summary>
        private void _CollectAllExpressions()
        {
            this.GlobalData = new HashSet<SharedExpression>();
            foreach (GraphBBVertex vertex in this.Graph.SortedVerticies)
                foreach (Addr3Command cmd in vertex.Data.Body)
                    // работаем с командами-выражениями
                    if (cmd.HasAssign() && (cmd.cmdType != Addr3CmdType.ASSIGN))
                        GlobalData.Add(new SharedExpression(cmd));
        }
    }
}
