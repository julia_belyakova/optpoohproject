﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.OptimizationsInBB.SharedExpressions;

namespace OptPoohCompiler.DataFlowAnalysis.AvailableExpressions
{
    /// <summary>
    /// Множество данных анализа в задаче о доступных выражениях
    /// </summary>
    public class AvailableExprDFADataType : SetDFADataType<SharedExpression>, IDFADataType<AvailableExprDFADataType>
    {
        /// <summary>
        /// Оператор сбора для множеств данных анализа
        /// задачи о доступных выражениях -- пересечение
        /// </summary>
        /// <param name="other">Второе множество данных анализа</param>
        /// <returns>Собранное множество данных анализа</returns>
        public AvailableExprDFADataType Join(AvailableExprDFADataType other)
        {
            AvailableExprDFADataType result = new AvailableExprDFADataType();
            result.UnionWith(this);
            result.IntersectWith(other);
            return result;
        }

        public bool Equals(AvailableExprDFADataType other)
        {
            return this.SetEquals(other);
        }
    }
}
