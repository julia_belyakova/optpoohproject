﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.DataFlowAnalysis.ConstantPropagation
{
    public class ConstPropagationTransferFunction : ITransferFunction<ConstPropagationDFADataType, Addr3Code.Addr3Command, ConstPropagationDFADataType>
    {
        /*
        this.Data = new ConstPropagationDFADataType();
            var a = LiveVariable.LiveVarIterativeAlgorithm.GetVariable(globalData);
            foreach (var c in a)
                this.Data.Table.Add(c, new VarInfo { Value = null, VarType = VarType.UNDEF });*/
        Addr3Code.Addr3Command Command;
        ConstPropagationDFADataType Data;

        public void GlobalInitialize(ConstPropagationDFADataType globalData)
        {
            this.Data = globalData;
            
        }

        public void Initialize(Addr3Code.Addr3Command owner)
        {
            this.Command = owner;
        }

        private bool TryParse(string arg, out int a)
        {
            return Int32.TryParse(arg, out a);
        }

        /// <summary>
        /// Применяет только унарный оператор "-". Во всех остальных случаях возвращает сам аргумент
        /// </summary>
        /// <param name="a"></param>
        /// <param name="com"></param>
        /// <returns></returns>
        public int ApplyOperator(int a, Addr3Code.Addr3CmdOperator com)
        {
            switch (com)
            {
                case Addr3Code.Addr3CmdOperator.MINUS:
                    return -a;
                default:
                    return a;
            }
        }

        /// <summary>
        /// Применяет оператор для 2х аргументов
        /// </summary>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="com"></param>
        /// <returns></returns>
        public int ApplyOperator(int arg1, int arg2, Addr3Code.Addr3CmdOperator com)
        {
            switch (Command.cmdOperator)
            {
                case Addr3Code.Addr3CmdOperator.PLUS:
                    return arg1 + arg2;
                case Addr3Code.Addr3CmdOperator.MINUS:
                    return arg1 - arg2;
                case Addr3Code.Addr3CmdOperator.MULT:
                    return arg1 * arg2;
                case Addr3Code.Addr3CmdOperator.DIVIDE:
                    try
                    {
                        return arg1 / arg2;
                    }
                    catch 
                    {
                        throw new Exception("Деление на ноль!");
                    }
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Применить передаточную ф-ию для протягивания констант
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public ConstPropagationDFADataType Apply(ConstPropagationDFADataType m)
        {
            if (!Command.HasAssign()) return m;

            var resTable = new Dictionary<string, VarInfo>();

            var x = Command.result;                     //левая часть присваивания
            
            foreach(var mm in m.Table)
                if (mm.Key != x)                        //если это не строка, соответствующая x
                    resTable.Add(mm.Key, mm.Value);     //просто переписываем эту строку
                else
                {
                    var arg1 = Command.arg1;
                    var arg2 = Command.arg2;

                    int a, b;

                    if (TryParse(arg1, out a))                                          // 1-ая - не переменая
                    {
                        if (arg2 == "")                                                 // 2-ой - нет          
                        {
                            resTable.Add(mm.Key, new VarInfo { VarType = VarType.CONST, Value = ApplyOperator(a, Command.cmdOperator) });
                            continue;
                        }

                        if (TryParse(arg2, out b))                                      // 2-ая - не переменая
                        {
                            resTable.Add(mm.Key, new VarInfo { VarType = VarType.CONST, Value = ApplyOperator(a, b, Command.cmdOperator) });
                            continue;
                        }
                        //if...  2-ая - переменая 

                        if (m.Table[arg2].VarType == VarType.CONST)
                        {
                            resTable.Add(mm.Key, new VarInfo { VarType = VarType.CONST, Value = ApplyOperator(a, (int)m.Table[arg2].Value, Command.cmdOperator) });
                            continue;
                        }

                        resTable.Add(mm.Key, new VarInfo { VarType = m.Table[arg2].VarType, Value = m.Table[arg2].Value });
                    }
                    else
                    {
                        if (arg2 == "")                                                 // 2-ой - нет          
                        {
                            resTable.Add(mm.Key, new VarInfo { VarType = m.Table[arg1].VarType, Value = m.Table[arg1].Value });
                            continue;
                        }

                        if (TryParse(arg2, out b))                                      // 2-ая - не переменая
                        {
                            if (m.Table[arg1].VarType == VarType.CONST)
                            {
                                resTable.Add(mm.Key, new VarInfo { VarType = VarType.CONST, Value = ApplyOperator((int)m.Table[arg1].Value, b, Command.cmdOperator) });
                                continue;
                            }

                            resTable.Add(mm.Key, new VarInfo { VarType = m.Table[arg1].VarType, Value = m.Table[arg1].Value });
                            continue;
                        }

                        if (m.Table[arg1].VarType == VarType.CONST && m.Table[arg2].VarType == VarType.CONST)
                        {
                            resTable.Add(mm.Key, new VarInfo { VarType = VarType.CONST, Value = ApplyOperator((int)m.Table[arg1].Value, (int)m.Table[arg2].Value, Command.cmdOperator) });
                            continue;
                        }
                        if(m.Table[arg1].VarType == VarType.NAC || m.Table[arg2].VarType == VarType.NAC )
                        {
                            resTable.Add(mm.Key, new VarInfo { VarType = VarType.NAC });
                            continue;
                        }

                        resTable.Add(mm.Key, new VarInfo { VarType = VarType.UNDEF });
                    }
                }

            var resClass = new ConstPropagationDFADataType();
            resClass.Table = new Dictionary<string, VarInfo>(resTable);
            return resClass;
        }

        /*public ConstPropagationDFADataType Apply(ConstPropagationDFADataType m)
        {
            if (Command.cmdType != Addr3Code.Addr3CmdType.ASSIGN)
            {
                return m;
            }

            var x = Command.result;                     //левая часть присваивания
            var res = new ConstPropagationDFADataType();
            foreach (var mm in m.Table)                 //для всех строк в таблице
            {
                if (mm.Key != x)                        //если это не строка, соответствующая x
                    res.Table.Add(mm.Key, mm.Value);    //просто переписываем эту строку
                else                                    //иначе
                { 
                    int r;
                    var b1 = Int32.TryParse(Command.arg1, out r);    //пытаемся преобразовать первый аргумент в правой части

                    if (Command.arg2 == null)           //если в правой части одно слагаемое
                    {
                        if (b1)
                        {
                            decimal val;
                            val = (decimal)r;
                            if (Command.cmdOperator == Addr3Code.Addr3CmdOperator.MINUS)
                                val = -val;
                            res.Table.Add(mm.Key, new VarInfo { VarType = VarType.CONST, Value = val });
                        }
                        else
                        {
                            var inf = res.Table[Command.arg1];
                            if (inf.VarType == VarType.UNDEF || inf.VarType == VarType.NAC)
                                res.Table.Add(mm.Key, inf);
                            else 
                            {
                                //if (Command.cmdOperator == Addr3Code.Addr3CmdOperator.MINUS)
                                    //res.Table.Add(mm.Key, new VarInfo { VarType = VarType.CONST, Value = - inf.Value });
                            }
                        }

                    }
                    else if (Command.arg2 != null)                      //если в правой части два слагаемого
                    {
                        if (m.Table[Command.arg1].VarType == VarType.CONST && m.Table[Command.arg2].VarType == VarType.CONST)
                        {
                            decimal? val;
                            switch (Command.cmdOperator)
                            {
                                case Addr3Code.Addr3CmdOperator.PLUS:
                                    val = (decimal)(m.Table[Command.arg1].Value) + (decimal)(m.Table[Command.arg2].Value);
                                    break;
                                case Addr3Code.Addr3CmdOperator.MINUS:
                                    val = (decimal)(m.Table[Command.arg1].Value) - (decimal)(m.Table[Command.arg2].Value);
                                    break;
                                case Addr3Code.Addr3CmdOperator.MULT:
                                    val = (decimal)(m.Table[Command.arg1].Value) * (decimal)(m.Table[Command.arg2].Value);
                                    break;
                                case Addr3Code.Addr3CmdOperator.DIVIDE:
                                    val = (decimal)(m.Table[Command.arg1].Value) / (decimal)(m.Table[Command.arg2].Value);
                                    break;
                                default:
                                    val = null;
                                    break; 
                            }
                            res.Table.Add(mm.Key, new VarInfo { VarType = VarType.CONST, Value = val });
                        }
                        else if (m.Table[Command.arg1].VarType == VarType.NAC || m.Table[Command.arg2].VarType == VarType.NAC)
                            res.Table.Add(mm.Key, new VarInfo { VarType = VarType.NAC, Value = null });
                        else
                            res.Table.Add(mm.Key, new VarInfo { VarType = VarType.UNDEF, Value = null });

                    }
                }
                
            }
            return res;
        }*/
    }
}
