﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.DataFlowAnalysis.ConstantPropagation
{
    /// <summary>
    /// Что может содержаться в переменной
    /// </summary>
    public enum VarType
    {
        /// <summary>
        /// значение не определенно
        /// </summary>
        UNDEF,
        /// <summary>
        /// значение переменной - константа
        /// </summary>
        CONST,
        /// <summary>
        /// (not a constant) значение переменной - не константа
        /// </summary>
        NAC
    }

    /// <summary>
    /// Пара. Содержит тип переменной и значение, если переменная - константа
    /// </summary>
    public struct VarInfo
    {
        /// <summary>
        /// Тип переменной
        /// </summary>
        public VarType VarType { get; set; }
        /// <summary>
        /// Значение переменных(если тип переменной != CONST, то null)
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Равно?
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var vi = (VarInfo)obj;
            int a,b;
            if (vi.VarType != VarType)
                return false;
            if (Value == vi.Value)
                return true;
            try
            {
                if (Int32.TryParse(vi.Value.ToString(), out a) && Int32.TryParse(Value.ToString(), out b))
                    if (a == b)
                        return true;
            }
            catch { return false; }
            return false;
        }

        public static bool operator  == (VarInfo val1, VarInfo val2)
        {
            return val1.Equals(val2);
        }

        public static bool operator !=(VarInfo val1, VarInfo val2)
        {
            return !val1.Equals(val2);
        }

        public override string ToString()
        {
            if (VarType == ConstantPropagation.VarType.CONST)
                return "CONST = " + Value;
            else
                return VarType.ToString();
        }

        


    }

    /// <summary>
    /// Множество данных анализа в задаче о распространении констант
    /// </summary>
    public class ConstPropagationDFADataType : IDFADataType<ConstPropagationDFADataType>
    {
        /// <summary>
        /// Таблица переменных
        /// </summary>
        public Dictionary<string, VarInfo> Table;

        public ConstPropagationDFADataType()
        {
            Table = new Dictionary<string, VarInfo>();
        }

        /// <summary>
        /// Оператор сбора в задаче о распространении констант
        /// </summary>
        /// <param name="other">Другое множество</param>
        /// <returns></returns>
        public ConstPropagationDFADataType Join(ConstPropagationDFADataType other)
        {
            ConstPropagationDFADataType result = new ConstPropagationDFADataType();
            //result.Table = new Dictionary<string,VarInfo>();
            foreach (var elem in other.Table)
            {
                var varName = elem.Key;
                if (Table.ContainsKey(varName))
                {
                    var thisValue = Table[varName];
                    var resultValue = new VarInfo();
                    var otherValue = elem.Value;
                    switch (otherValue.VarType)
                    {
                        case VarType.NAC:
                            resultValue.VarType = VarType.NAC;
                            //resultValue.Value = null;
                            break;
                        case VarType.CONST:
                            switch (thisValue.VarType)
                            {
                                case VarType.CONST:
                                    if ((int)thisValue.Value != (int)otherValue.Value)
                                    {
                                        resultValue.VarType = VarType.NAC;
                                        //resultValue.Value = null;
                                    }
                                    else
                                    {
                                        resultValue.VarType = VarType.CONST;
                                        resultValue.Value = thisValue.Value;
                                    }
                                    break;
                                case VarType.NAC:
                                    resultValue.VarType = VarType.NAC;
                                    //resultValue.Value = null;
                                    break;
                                default:
                                    resultValue.VarType = otherValue.VarType;
                                    resultValue.Value = otherValue.Value;
                                    break;
                            }                            
                            break;
                        default:
                            resultValue.VarType = thisValue.VarType;
                            resultValue.Value = thisValue.Value;
                            break;
                    }
                    
                    result.Table.Add(varName, resultValue);
                }
                else
                {
                    result.Table.Add(varName, elem.Value);
                }
            }
            //Table = result.Table; 
            return  result;
        }

        /// <summary>
        /// Сравнение таблиц на равенство
        /// </summary>
        /// <param name="other">Другая таблица</param>
        /// <returns></returns>
        public bool Equals(ConstPropagationDFADataType other)
        {
            if (Table.Count != other.Table.Count)
                return false;

            foreach (var val in Table)
            {
                //var b1 = other.Table.Keys.Contains(val.Key);
                //var b2 = other.Table[val.Key] == val.Value;
                //var b2 = other.Table.TryGetValue(
                var vi = new VarInfo();
                var b1 = other.Table.Keys.Contains(val.Key);
                var b2 = other.Table.TryGetValue(val.Key, out vi);
                var b3 = vi == val.Value;
                if (b1&&b2&&b3)
                    continue;
                else
                    return false;

            }
            
            return true;
        }
                
        public override string ToString()
        {
            string res = "";
            foreach (var t in Table)
                res += "(" + t.Key + " | " + t.Value + "); \r\n";
            return res;
        }
    }
}
