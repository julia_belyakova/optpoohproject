﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OptPoohCompiler.DataFlowAnalysis.ConstantPropagation;
using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.DataFlowAnalysis.ConstantPropagation
{
    class ConstPropagationIterativeAlgorithm :
        IterativeAlgorithm<
            ConstPropagationDFADataType, 
            BasicBlock,
            ConstPropagationDFADataType,
            ComposedBBTransferFun<ConstPropagationTransferFunction, ConstPropagationDFADataType, ConstPropagationDFADataType>, 
            GraphBBVertex, GraphBB>
    {

        public ConstPropagationIterativeAlgorithm(GraphBB graph)
            : base(graph)
        { }

        //private HashSet<string> GetVariables(GraphBB graph)
        //{
        //    HashSet<string> result = new HashSet<string>();
        //    foreach (var v in graph.Verticies)
        //    {
        //        var bb = v.Data;
        //        foreach (var cmd in bb.Body)
        //        {
        //            if (cmd..cmdType == Addr3Code.Addr3CmdType.)
        //        }
        //    }
        //}

        protected override void Initialize()
        {
            var table = new ConstPropagationDFADataType();
            var a = LiveVariable.LiveVarIterativeAlgorithm.GetAllVariables(Graph);
            foreach (var c in a)
                table.Table.Add(c, new VarInfo {  VarType = VarType.UNDEF });
            this.InitVal = table;
            this.Top = table;
            this.GlobalData = table;
            this.direction = AnalysisDirection.DOWN;
           // this.Top.UnionWith(this.GlobalData);
        }
    }
}
