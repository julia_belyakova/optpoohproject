﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.DataFlowAnalysis
{
    /// <summary>
    /// Данные анализа -- множество значений типа D.
    /// </summary>
    /// <typeparam name="D">Тип элементов множества</typeparam>
    public class SetDFADataType<D> : HashSet<D>, IDFADataType<SetDFADataType<D>>
    {
        /// <summary>
        /// Данные анализа -- множество значений типа VertexValueType.
        /// </summary>
        public SetDFADataType() 
            : base()
        {
        }

        /*public static virtual SetDFADataType<D> CalcAddingSet(SetDFADataType<D> adding1, SetDFADataType<D> subtracting1,
            SetDFADataType<D> adding2, SetDFADataType<D> subtracting2, SetDFADataType<D> full)
        {
            return null;
        }

        public static virtual SetDFADataType<D> CalcSubtractingSet(SetDFADataType<D> adding1, SetDFADataType<D> subtracting1,
            SetDFADataType<D> adding2, SetDFADataType<D> subtracting2, SetDFADataType<D> full)
        {
            return null;
        }*/

        /// <summary>
        /// Оператор сбора для данных анализа-множеств
        /// </summary>
        /// <param name="other">Второе множество данных анализа</param>
        /// <returns>Собранное множество данных анализа</returns>
        public SetDFADataType<D> Join(SetDFADataType<D> other)
        {
            SetDFADataType<D> result = new SetDFADataType<D>();
            result.UnionWith(this);
            result.UnionWith(other);
            return result;
        }

        public bool Equals(SetDFADataType<D> other)
        {
            return this.SetEquals(other);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (this.Count > 0)
            {
                var enumer = this.GetEnumerator();
                enumer.MoveNext();
                sb.Append(enumer.Current.ToString());
                while (enumer.MoveNext())
                    sb.Append(", " + enumer.Current.ToString());
            }
            return sb.ToString();
        }
    }
}
