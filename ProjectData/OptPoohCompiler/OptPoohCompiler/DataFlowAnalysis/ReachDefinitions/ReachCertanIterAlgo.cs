﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.OptimizationsInBB.SharedExpressions;
using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.Addr3Code;

namespace OptPoohCompiler.DataFlowAnalysis.ReachCertans
{
    /// <summary>
    /// Итерационный алгоритм в задаче о доступных выражениях
    /// </summary>
 //   public class ReachCertanIterAlgo : IterativeAlgorithm<
 //       ReachCertanDFADataType, BasicBlock, Dictionary<string, Dictionary<BasicBlock, LinkedList<Addr3Command>>>,  
 //       ComposedBBTransferFun<ReachCertanTransferFunction, ReachCertanDFADataType,  Dictionary<string, Dictionary<BasicBlock, LinkedList<Addr3Command>>>>,
 //       GraphBBVertex, GraphBB>
     public class ReachCertanIterAlgo : IterativeAlgorithm<
                                            ReachCertanDFADataType, BasicBlock, Dictionary<string, Dictionary<BasicBlock, LinkedList<Addr3Command>>>,  
         ReachCertanTransferFunction, GraphBBVertex, GraphBB>
    {
        /// <summary>
        /// Итерационный алгоритм в задаче о доступных выражениях
        /// </summary>
        /// <param name="graph">Граф базовых блоков</param>
        public ReachCertanIterAlgo(GraphBB graph)
            : base(graph)
        { 
               
        }

        /// <summary>
        /// Инициализация значения GlobalData, Top, InitVal 
        /// </summary>
        protected override void Initialize()
        {
            _CollectAllExpressions();
            this.InitVal = new ReachCertanDFADataType();
            this.Top = new ReachCertanDFADataType();
            this.direction = AnalysisDirection.DOWN;
        }

        /// <summary>
        /// Собирает во множество глобальных данных все выражения
        /// в графе ББл
        /// </summary>
        private void _CollectAllExpressions()
        {
            this.GlobalData = new Dictionary<string ,Dictionary <BasicBlock , LinkedList<Addr3Command>>>();
            foreach (GraphBBVertex vertex in this.Graph.SortedVerticies)
                foreach (Addr3Command cmd in vertex.Data.Body)
                {
                    if ((!cmd.HasAssign()) || ((cmd.result.Substring(0, 1) == "%")))
                        continue;
                    if (!this.GlobalData.ContainsKey(cmd.result))
                        this.GlobalData.Add(cmd.result, new Dictionary<BasicBlock, LinkedList<Addr3Command>>());
                    if (!this.GlobalData[cmd.result].ContainsKey(vertex.Data))
                        this.GlobalData[cmd.result].Add(vertex.Data, new LinkedList<Addr3Command>());
                    this.GlobalData[cmd.result][vertex.Data].AddLast(cmd);
                }
        }
    }
}
