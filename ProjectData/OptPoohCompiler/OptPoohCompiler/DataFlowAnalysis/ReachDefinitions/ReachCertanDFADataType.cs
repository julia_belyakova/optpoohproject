﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;
using OptPoohCompiler.BasicBlocks;
// using OptPoohCompiler.GraphTools;

namespace OptPoohCompiler.DataFlowAnalysis.ReachCertans
{
    /// <summary>
    /// Множество данных анализа в задаче о достигающих определениях
    /// </summary>
    public class ReachCertanDFADataType : SetDFADataType<KeyValuePair<Addr3Command, BasicBlock>>, IDFADataType<ReachCertanDFADataType>
    {
        /// <summary>
        /// Оператор сбора для множеств данных анализа
        /// задачи о доступных выражениях -- пересечение
        /// </summary>
        /// <param name="other">Второе множество данных анализа</param>
        /// <returns>Собранное множество данных анализа</returns>
        public ReachCertanDFADataType Join(ReachCertanDFADataType other)
        {
            ReachCertanDFADataType result = new ReachCertanDFADataType();
            result.UnionWith(this);
            result.UnionWith(other);
            return result;
        }

        public bool Equals(ReachCertanDFADataType other)
        {
            return this.SetEquals(other);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (this.Count > 0)
            {
                var enumer = this.GetEnumerator();
                enumer.MoveNext();
                sb.Append("(" + enumer.Current.Key.ToString()
                    + ", " + enumer.Current.Value.Name + ")");
                while (enumer.MoveNext())
                {
                    sb.AppendLine();
                    sb.Append("(" + enumer.Current.Key.ToString()
                        + ", " + enumer.Current.Value.Name + ")");
                }
            }
            return sb.ToString();
        }
    }
}
