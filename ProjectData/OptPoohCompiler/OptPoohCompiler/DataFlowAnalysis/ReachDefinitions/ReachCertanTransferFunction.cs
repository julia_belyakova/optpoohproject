﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.OptimizationsInBB.SharedExpressions;
using OptPoohCompiler.Addr3Code;

namespace OptPoohCompiler.DataFlowAnalysis.ReachCertans
{
    /// <summary>
    /// Передаточная функция для задачи о доступных выражениях
    /// </summary>
    public class ReachCertanTransferFunction : SetTransferFunction<KeyValuePair<Addr3Command, BasicBlock>, ReachCertanDFADataType, Dictionary<string, Dictionary<BasicBlock, LinkedList<Addr3Command>>>>
    {
        /// <summary>
        /// Все выражения, встречающиеся в графе
        /// </summary>
        private Dictionary<string, Dictionary<BasicBlock, LinkedList<Addr3Command>>> allDefs;

        /// <summary>
        /// Передаточная функция задачи о доступных выражениях
        /// </summary>
        public ReachCertanTransferFunction()
        {
            // gen[C]
            addingSet = new ReachCertanDFADataType();
            // kill[C]
            subtractingSet = new ReachCertanDFADataType();
        }

        /// <summary>
        /// Инициализирует множество всех выражений, встречающихся в графе
        /// </summary>
        /// <param name="allGraphExprs">Множество всех выражений графа</param>
        public override void GlobalInitialize(Dictionary<string, Dictionary<BasicBlock, LinkedList<Addr3Command>>> allDefs)
        {
            this.allDefs = allDefs;
        }

        /// <summary>
        /// Инициализирует множества gen[C], kill[C] 
        /// для трехадресной команды cmd
        /// </summary>
        /// <param name="cmd">Трехадресная команда</param>
        public override void Initialize(Addr3Command cmd)
        {
            /*// если нет присваивания -- нам не интересно
            if (!cmd.HasAssign())
                return;
            // если это не в точности команда присваивания
            if (cmd.cmdType != Addr3CmdType.ASSIGN) {
                // выражение в присваивании
                SharedExpressionReach expr = new SharedExpressionReach(cmd, "");
                // если левая часть присваивания не содержится в выражении,
                // то это выражение должно попасть в e_gen[C]
                if (!expr.ContainsVariable(cmd.result))
                    addingSet.Add(expr);
            }
            // в e_kill[C] должны попасть все выражения, которые содержат
            // левую часть присваивания
            // берем все выражения, которые убиваются текущей командой присваивания
            HashSet<SharedExpressionReach> killedByExpr = new HashSet<SharedExpressionReach>(
                allGraphExprs.Where(expr => expr.ContainsVariable(cmd.result)));
            // добавляем эти выражение во множество e_kill[B]
            subtractingSet.UnionWith(killedByExpr);*/
        }

        /// <summary>
        /// Инициализирует передаточную функцию базовым блоком
        /// </summary>
        /// <param name="basicBlock"></param>
        public override void Initialize(BasicBlock basicBlock)
        {
            // обрабатываем все команды с присваиванием
            foreach (Addr3Command cmd in basicBlock.Body)
            {
                if ((!cmd.HasAssign()) || ((cmd.result.Substring(0, 1) == "%")))
                    continue;
                // gen -- работаем с текущим ББл
                bool toRemove = false;
                KeyValuePair<Addr3Command, BasicBlock> removingPair = new KeyValuePair<Addr3Command,BasicBlock>();
                foreach (KeyValuePair<Addr3Command, BasicBlock> pair in addingSet)
                    if (pair.Key.result == cmd.result)
                    {
                        toRemove = true;
                        removingPair = pair;
                    }
                if (toRemove)
                {
                    
                    // true -> проверить,что addingSet содержит информацию о
                    // команде, result которой равен cmd.result
                    // то удаляем эту информацию  
                    addingSet.Remove(removingPair);
                }
                addingSet.Add(new KeyValuePair<Addr3Command, BasicBlock>(cmd, basicBlock));
                // kill
                if (!toRemove)
                {
                    // все определения в других ББЛ
                    var currVarDefs = this.allDefs[cmd.result];
                    foreach (KeyValuePair<BasicBlock, LinkedList<Addr3Command>> pair in currVarDefs)
                        if (pair.Key != basicBlock)
                        {
                            // все ББл кроме текущего
                            foreach (Addr3Command cmd1 in pair.Value)
                            {
                               // subtractingSet.Add(new KeyValuePair<Addr3Command, BasicBlock>(cmd1, basicBlock));
                                subtractingSet.Add(new KeyValuePair<Addr3Command, BasicBlock>(cmd1, pair.Key));
                            }
                        }
                }
            }
                
        }
    }
}
