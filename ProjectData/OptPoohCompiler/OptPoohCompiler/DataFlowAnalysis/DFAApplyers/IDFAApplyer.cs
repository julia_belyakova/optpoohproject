﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.DataFlowAnalysis.DFAApplyers
{
    /// <summary>
    /// Интерфейс для классов, применяющих конкретный итерационный
    /// алгоритм к CFG (для основной формы)
    /// </summary>
    public interface IDFAApplyer
    {
        /// <summary>
        /// Применяет конкретный алгоритм к графу базовых блоков
        /// </summary>
        /// <param name="CFG">Граф базовых блоков</param>
        void Apply(GraphBB CFG);
    }
}
