﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.DataFlowAnalysis.DFAApplyers
{
    /// <summary>
    /// Класс-применитель алгоритма задачи о доступных выражениях
    /// к графу базовых блоков
    /// </summary>
    public class AvailableExprIterAlgoApplyer : IDFAApplyer
    {

        public void Apply(BasicBlocks.GraphBB CFG)
        {
            AvailableExpressions.AvailableExprIterAlgo algo 
                = new AvailableExpressions.AvailableExprIterAlgo(CFG);
            algo.Execute();
        }

        public override string ToString()
        {
            return "Анализ доступных выражений";
        }
    }
}
