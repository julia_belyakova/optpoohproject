﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.DataFlowAnalysis.DFAApplyers
{
    public class LiveVarIterAlgoApplyer : IDFAApplyer
    {
        public void Apply(BasicBlocks.GraphBB CFG)
        {
            LiveVariable.LiveVarIterativeAlgorithm algo = new LiveVariable.LiveVarIterativeAlgorithm(CFG);
            algo.Execute();
        }

        public override string ToString()
        {
            return "Анализ активных переменных";
        }
    }
}
