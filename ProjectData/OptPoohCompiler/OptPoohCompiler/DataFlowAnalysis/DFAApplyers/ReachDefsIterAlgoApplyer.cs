﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.DataFlowAnalysis.DFAApplyers
{
    /// <summary>
    /// Класс-применитель алгоритма задачи о достигающих определениях
    /// к графу базовых блоков
    /// </summary>
    public class ReachDefsIterAlgoApplyer : IDFAApplyer
    {
        public void Apply(BasicBlocks.GraphBB CFG)
        {
            ReachCertans.ReachCertanIterAlgo algo = new ReachCertans.ReachCertanIterAlgo(CFG);
            algo.Execute();
        }

        public override string ToString()
        {
            return "Анализ достигающих определений";
        }
    }
}
