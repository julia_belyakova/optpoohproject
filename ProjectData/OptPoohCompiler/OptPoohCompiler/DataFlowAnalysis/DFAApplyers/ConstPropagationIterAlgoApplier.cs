﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OptPoohCompiler.DataFlowAnalysis.ConstantPropagation;

namespace OptPoohCompiler.DataFlowAnalysis.DFAApplyers
{
    class ConstPropagationIterAlgoApplier : IDFAApplyer
    {
        #region IDFAApplyer Members

        public void Apply(BasicBlocks.GraphBB CFG)
        {
            var alg = new ConstPropagationIterativeAlgorithm(CFG);
            alg.Execute();
        }

        public override string ToString()
        {
            return "Протягивание констант";
        }
        #endregion
    }
}
