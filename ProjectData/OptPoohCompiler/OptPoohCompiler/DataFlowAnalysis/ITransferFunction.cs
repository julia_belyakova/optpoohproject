﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.GraphTools;

namespace OptPoohCompiler.DataFlowAnalysis
{
    /// <summary>
    /// Передаточная функция
    /// </summary>
    /// <typeparam name="AnalysisData">Тип данных анализа</typeparam>
    /// <typeparam name="NodeType">Тип элементов привязки передаточной функции</typeparam>
    /// <typeparam name="InitDataType">Тип вспомогательных данных, необходимых для передаточной функции</typeparam>
    public interface ITransferFunction<AnalysisData, NodeType, InitDataType>
        where AnalysisData : IDFADataType<AnalysisData>
    {
        /// <summary>
        /// Инициализация передаточной функции глобальной информацией,
        /// полученной по всему графу
        /// </summary>
        /// <param name="GlobalData">Глобальная информация по графу</param>
        void GlobalInitialize(InitDataType globalData);

        /// <summary>
        /// Подготавливает функцию для заданного элемента-владельца
        /// </summary>
        /// <param name="owner">Владелец передаточной функции</param>
        void Initialize(NodeType owner);

        /// <summary>
        /// Применяет передаточную функцию к входным данным x
        /// </summary>
        /// <param name="x">Входные данные анализа</param>
        /// <returns>Выходные данные</returns>
        AnalysisData Apply(AnalysisData x);
    }
}
