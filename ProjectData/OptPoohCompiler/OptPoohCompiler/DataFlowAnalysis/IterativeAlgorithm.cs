﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.GraphTools;

namespace OptPoohCompiler.DataFlowAnalysis
{
    /// <summary>
    /// Итерационный алгоритм
    /// </summary>
    /// <typeparam name="AnalysisData">Тип данных анализа</typeparam>
    /// <typeparam name="VertexValueType">Тип значений в вершинах графа, к которому применяется алгоритм</typeparam>
    /// <typeparam name="TFInitDataType">Тип вспомогательных данных</typeparam>
    /// <typeparam name="TF">Тип передаточной функции элемента VT, работающей с данными DT</typeparam>
    /// <typeparam name="V">Тип вершин графа</typeparam>
    /// <typeparam name="G">Тип графа</typeparam>
    public abstract class IterativeAlgorithm<AnalysisData, VertexValueType, TFInitDataType, TF, V, G>
        where AnalysisData : class, IDFADataType<AnalysisData>
        where TF : ITransferFunction<AnalysisData, VertexValueType, TFInitDataType>, new()
        where V : IVertex<V, VertexValueType>
        where G : IGraph<V, VertexValueType>
    {
        /// <summary>
        /// Граф
        /// </summary>
        private G graph;

        /// <summary>
        /// Направление анализа
        /// </summary>
        protected AnalysisDirection direction;

        /// <summary>
        /// Перенумерация вершин графа. Массив должен содержать
        /// индексы вершин из массива graph.SortedVerticies
        /// </summary>
        protected List<int> vertexNumeration = null;

        /// <summary>
        /// Глобальные данные для передаточной функции
        /// </summary>
        protected TFInitDataType GlobalData;
        /// <summary>
        /// Верхняя граница множества данных анализа
        /// </summary>
        protected AnalysisData Top;
        /// <summary>
        /// Начальное значение анализа
        /// </summary>
        protected AnalysisData InitVal;

        /// <summary>
        /// Число шагов итерационного алгоритма
        /// </summary>
        private int stepsCount = -1;

        /// <summary>
        /// Граф
        /// </summary>
        protected G Graph
        {
            get { return this.graph; }
        }

        /// <summary>
        /// Число шагов итерационного алгоритма
        /// </summary>
        public int StepsCount
        {
            get { return stepsCount; }
        }

        /// <summary>
        /// Итерационный алгоритм
        /// </summary>
        /// <param name="graph">Граф</param>
        public IterativeAlgorithm(G graph) 
        {
            if (graph == null)
                throw new ArgumentNullException("Граф не может быть null");
            this.graph = graph;
            NumerateVertex();
            if (vertexNumeration == null)
                IdentNumerateVertex();
            if (vertexNumeration.Count != graph.Verticies.Count)
                throw new InvalidOperationException(
                    "Неверная перенумерация вершин графа: число вершин не соответствует");
            Initialize();
        }

        /// <summary>
        /// Задаёт нумерацию вершин графа для выполнения в итерационном алгоритме
        /// </summary>
        /// <param name="numeration">Нумерация вершин графа</param>
        public void SetVertexNumeration(int[] numeration)
        {
            if (numeration == null)
                throw new ArgumentNullException("Нумерация не может быть null");
            if (numeration.Length != graph.SortedVerticies.Count)
                throw new InvalidOperationException(
                    "Некорректная перенумерация вершин графа: число вершин не соответствует");
            HashSet<int> numbers = new HashSet<int>();
            foreach (int ind in numeration)
                numbers.Add(ind);
            int[] sortedNumeration = new int[numeration.Length];
            numeration.CopyTo(sortedNumeration, 0);
            Array.Sort(sortedNumeration);
            if ((sortedNumeration[0] != 0)
                || (sortedNumeration[graph.SortedVerticies.Count - 1] != graph.SortedVerticies.Count - 1))
                throw new InvalidOperationException(
                    "Некорректная перенумерация вершин графа");
            for (int i = 0; i < graph.SortedVerticies.Count; ++i)
                vertexNumeration[i] = numeration[i];
        }

        /// <summary>
        /// Перенумерация вершин графа по умолчанию (согласно
        /// его топологической сортировке)
        /// </summary>
        protected void IdentNumerateVertex()
        {
            vertexNumeration = new List<int>(graph.SortedVerticies.Count);
            for (int i = 0; i < graph.SortedVerticies.Count; ++i)
                vertexNumeration.Add(i);
        }

        /// <summary>
        /// Перенумерация вершин графа, если это нужно для алгоритма.
        /// Здесь используется топологическая сортировка.
        /// </summary>
        protected virtual void NumerateVertex()
        {
            IdentNumerateVertex();
        }

        /// <summary>
        /// Инициализация значений Direction, GlobalData, Top, InitVal 
        /// </summary>
        protected abstract void Initialize();

        /// <summary>
        /// Выполнение итерационного алгоритма
        /// </summary>
        public void Execute()
        { 
            // инициализируем передаточные функции для каждого элемента графа
            Dictionary<VertexValueType, TF> transferFunctions = new Dictionary<VertexValueType, TF>();
            for (int i = 0; i < vertexNumeration.Count; ++i)
            {
                V vertex = graph.SortedVerticies[vertexNumeration[i]];
                transferFunctions.Add(vertex.Data, 
                    DataFlowAnalysisTools.ConstructTransferFunction<AnalysisData, VertexValueType, TFInitDataType, TF>(
                    this.GlobalData, vertex.Data));
            }
            this.stepsCount = 0;
            // выполняем алогоритм в нужном направлении
            if (this.direction == AnalysisDirection.DOWN)
                _IterateDown(transferFunctions);
            else
                _IterateUp(transferFunctions);
        }

        /// <summary>
        /// Итерационный алгоритм направления DOWN
        /// </summary>
        private void _IterateDown(Dictionary<VertexValueType, TF> transferFunctions)
        {
            // OUT входной вершины (OUT[Input] = Vstart)
            graph.InputVertex.OUT = this.InitVal;
            // [algo] назначаем OUT'ы всех вершин графа, кроме входа
            // [algo] foreach (B != Input) OUT[B] = T
            // CFG.SortedVerticies[0] -- входной фиктивный ББл
            for (int i = 1; i < vertexNumeration.Count; ++i)
                graph.SortedVerticies[vertexNumeration[i]].OUT = this.Top;

            // [algo] пока вектор решения меняется-- работает цикл
            bool outVectorIsChanging = true;
            // [algo] while OUT[B] is changing
            while (outVectorIsChanging)
            {
                ++this.stepsCount;
                outVectorIsChanging = false;
                // [algo] обрабатываем все базовые блоки, кроме входа
                // [algo] foreach (basic block B != Input)
                for (int i = 1; i < vertexNumeration.Count; ++i)
                {
                    // текущий базовый блок B
                    V currVertex = graph.SortedVerticies[vertexNumeration[i]];
                    // [algo] нужно сделать In[B] = ^{P -- predecessor B}OUT[P]
                    // [algo] сначала присваиваем IN[B] (currOUT) верхнее значение
                    AnalysisData currIN = this.Top;
                    // теперь применяем оператор сбора Join, nextVertex -- P
                    foreach (V prevVertex in currVertex.InVerticies)
                        currIN = currIN.Join(prevVertex.OUT as AnalysisData);
                    currVertex.IN = currIN;
                    // [algo] осталось OUT[B] = fb(IN[B])
                    // fb = transferFunctions[currVertex.AnalysisData]
                    // текущее знчаение OUT[B]
                    AnalysisData oldCurrOUT = currVertex.OUT as AnalysisData;
                    // новое значение OUT[B] = fb(IN[B])
                    AnalysisData newCurrOUT = transferFunctions[currVertex.Data].Apply(currIN);
                    currVertex.OUT = newCurrOUT;
                    // если новое значение отличается от предыдущего, значит вектор решения поменялся
                    outVectorIsChanging = outVectorIsChanging || !oldCurrOUT.Equals(newCurrOUT);
                }
            }
        }

        /// <summary>
        /// Итерационный алгоритм направления UP
        /// </summary>
        private void _IterateUp(Dictionary<VertexValueType, TF> transferFunctions)
        {
            // TODO
            // как таковой вершины-выхода у нас нет. По идее проблем быть не должно,
            // потому что как на потомка на эту вершину тоже ссылки нет

            // [algo] назначаем IN'ы всех вершин графа, кроме входа, потому что 
            // он не нужен
            // [algo] foreach (B) IN[B] = T
            // CFG.SortedVerticies[0] -- входной фиктивный ББл
            for (int i = 1; i < vertexNumeration.Count; ++i)
                graph.SortedVerticies[vertexNumeration[i]].IN = this.Top;

            // [algo] пока вектор решения меняется-- работает цикл
            bool inVectorIsChanging = true;
            // [algo] while IN[B] is changing
            while (inVectorIsChanging)
            {
                inVectorIsChanging = false;
                // [algo] обрабатываем все базовые блоки, кроме входа,
                // причем снизу вверх
                // [algo] foreach (basic block B != Input)
                for (int i = vertexNumeration.Count - 1; i > 0; --i)
                {
                    // текущий базовый блок B
                    V currVertex = graph.SortedVerticies[vertexNumeration[i]];
                    // [algo] нужно сделать OUT[B] = ^{P -- descedant B}IN[P]
                    // [algo] сначала присваиваем OUT[B] (currOUT) верхнее значение
                    AnalysisData currOUT = this.Top;
                    // теперь применяем оператор сбора Join, nextVertex -- P
                    foreach (V nextVertex in currVertex.OutVerticies)
                        currOUT = currOUT.Join(nextVertex.IN as AnalysisData);
                    currVertex.OUT = currOUT;
                    // [algo] осталось IN[B] = fb(OUT[B])
                    // fb = transferFunctions[currVertex.AnalysisData]
                    // текущее знчаение IN[B]
                    AnalysisData oldCurrIN = currVertex.IN as AnalysisData;
                    // новое значение IN[B] = fb(OUT[B])
                    AnalysisData newCurrIN = transferFunctions[currVertex.Data].Apply(currOUT);
                    currVertex.IN = newCurrIN;
                    // если новое значение отличается от предыдущего, значит вектор решения поменялся
                    inVectorIsChanging = inVectorIsChanging || !oldCurrIN.Equals(newCurrIN);
                }
            }
        }

    }
}
