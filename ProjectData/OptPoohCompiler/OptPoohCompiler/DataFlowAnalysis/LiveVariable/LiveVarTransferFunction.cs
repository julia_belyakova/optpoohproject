﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OptPoohCompiler.OptimizationsInBB.SharedExpressions;

namespace OptPoohCompiler.DataFlowAnalysis.LiveVariable
{
    /// <summary>
    /// Передаточная функция для задачи о активных переменных
    /// </summary>
    public class LiveVarTransferFunction : SetTransferFunction<string, LiveVarDFADataType, HashSet<string>>
    {
        /// <summary>
        /// Все переменные, встречающиеся к графе
        /// </summary>
        HashSet<string> allGraphExprs;

        /// <summary>
        /// Инициализирует множество всех переменных, встречающихся в графе
        /// </summary>
        /// <param name="GlobalData">Множество всех переменных, встречающихся в графе</param>
        public override void GlobalInitialize(HashSet<string> globalData)
        {
            this.allGraphExprs = globalData;
        }

        /// <summary>
        /// Инициализирует множества use_c и def_c для трёхадресной комманды <code>cmd</code>
        /// </summary>
        /// <param name="cmd">Трехадресная команда</param>
        public override void Initialize(Addr3Code.Addr3Command cmd)
        {
            addingSet = new LiveVarDFADataType();           //use_c[cmd] = ∅

            subtractingSet = new LiveVarDFADataType();      //def_c[cmd] = ∅

            int tmp;                                        //Просто переменная для передачи в TryParse
            switch (cmd.cmdType)
            {
                case OptPoohCompiler.Addr3Code.Addr3CmdType.BIN_OPERATOR:
                    if (!int.TryParse(cmd.arg1, out tmp))   //Проверяем, вдруг arg1 не переменная, а число.
                        addingSet.Add(cmd.arg1);            //добавляем arg1 к use_c[cmd]
                    if (!int.TryParse(cmd.arg2, out tmp))   //Проверяем, вдруг arg2 не переменная, а число.
                        addingSet.Add(cmd.arg2);            //добавляем arg2 к use_c[cmd]
                                                            //Проверяем, не использовался ли result ранее
                    if ((cmd.result != cmd.arg2) || (cmd.result != cmd.arg1)) 
                        subtractingSet.Add(cmd.result);     //добавляем result к def_c[cmd]
                    break;
                case OptPoohCompiler.Addr3Code.Addr3CmdType.UNARY_OPERATOR:
                    if (!int.TryParse(cmd.arg1, out tmp))   //Проверяем, вдруг arg1 не переменная, а число.
                        addingSet.Add(cmd.arg1);            //добавляем arg1 к use_c[cmd]
                    if (cmd.result != cmd.arg1)             //Проверяем, не использовался ли result ранее
                        subtractingSet.Add(cmd.result);     //добавляем result к def_c[cmd]
                    break;
                case OptPoohCompiler.Addr3Code.Addr3CmdType.ASSIGN:
                    if (!int.TryParse(cmd.arg1, out tmp))   //Проверяем, вдруг arg1 не переменная, а число.
                        addingSet.Add(cmd.arg1);            //добавляем arg1 к use_c[cmd]
                    if (cmd.result != cmd.arg1)             //Проверяем, не использовался ли result ранее
                        subtractingSet.Add(cmd.result);     //добавляем result к def_c[cmd]
                    break;
                default:
                    return;
            }
        }


        /// <summary>
        /// Инициализирует множества use_b и def_b для базового блока <code>basicBlock</code>
        /// </summary>
        /// <param name="basicBlock"></param>
        public override void Initialize(BasicBlocks.BasicBlock basicBlock)
        {
            addingSet = new LiveVarDFADataType();           //use_b[basicBlock] = ∅

            subtractingSet = new LiveVarDFADataType();      //def_b[basicBlock] = ∅

                                                            //Передаточная функция для каждой трёхадресной комманды в базовом блоке
            LiveVarTransferFunction Addr3Transf = new LiveVarTransferFunction();

            foreach (Addr3Code.Addr3Command cmd in basicBlock.Body)
            {
                Addr3Transf.Initialize(cmd);                //Инициализируем передаточную функцию для текущей комманды в блоке

                                                            //Вычитаем из use_c[cmd] текущей комманды те переменные, которые были определены ранее def_b[basicBlock] 
                Addr3Transf.addingSet.ExceptWith(subtractingSet);
                                                            //Вычитаем из def_c[cmd] текущей комманды те переменные, которые были использованны ранее use_b[basicBlock]
                Addr3Transf.subtractingSet.ExceptWith(addingSet);

                addingSet.UnionWith(Addr3Transf.addingSet); //Добавляем use_c[cmd] к use_b[basicBlock]
                                                            //Добавляем def_c[cmd] к def_b[basicBlock]
                subtractingSet.UnionWith(Addr3Transf.subtractingSet);
            }
            
            //base.Initialize(basicBlock);
        }

        /// <summary>
        /// Применяет передаточную функцию к текущему элементу
        /// </summary>
        /// <param name="x">OUT текущего элемента</param>
        /// <returns>IN текущего элемента</returns>
        public override LiveVarDFADataType Apply(LiveVarDFADataType x)
        {
            LiveVarDFADataType lv_result = new LiveVarDFADataType();
            lv_result.UnionWith(x.Except(subtractingSet).Union(addingSet));
            return lv_result;
        }


    }
}
