﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.DataFlowAnalysis.LiveVariable
{
    /// <summary>
    /// Итерационный алгоритм для задачи о активных переменных
    /// </summary>
    public class LiveVarIterativeAlgorithm : IterativeAlgorithm<LiveVarDFADataType, BasicBlock, HashSet<string>, LiveVarTransferFunction, GraphBBVertex, GraphBB>
    {
        /// <summary>
        /// Конструктор итерационного алгоритма
        /// </summary>
        /// <param name="graph">Граф базовых блоков для анализа</param>
        public LiveVarIterativeAlgorithm(GraphBB graph)
            : base(graph)
        {

        }

        /// <summary>
        /// Инициализация итерационного алгоритма для задачи о активных переменных
        /// </summary>
        protected override void Initialize()
        {
            _CollectAllExpressions();
            Top = new LiveVarDFADataType();
            InitVal = new LiveVarDFADataType();
            direction = AnalysisDirection.DOWN;
        }


        private void _CollectAllExpressions()
        {
            GlobalData = GetVariable(this.Graph);
            
            //foreach (GraphBBVertex vertex in this.Graph.SortedVerticies)
            //    foreach (Addr3Code.Addr3Command cmd in vertex.Data.Body)
            //        if (cmd.HasAssign())
            //            GlobalData.Add(cmd.result);
        }

        public static HashSet<string> GetVariable(GraphBB graph)
        {
            HashSet<string> ResData = new HashSet<string>();
            foreach (GraphBBVertex vertex in graph.SortedVerticies)
                foreach (Addr3Code.Addr3Command cmd in vertex.Data.Body)
                    if (cmd.HasAssign())
                        ResData.Add(cmd.result);
            return ResData;
        }

        /// <summary>
        /// Возвращает все-все-все переменные из кода программы, даже которые не определены(по воле Саши) 
        /// </summary>
        /// <param name="graph"></param>
        /// <returns></returns>
        public static HashSet<string> GetAllVariables(GraphBB graph)
        {
            HashSet<string> ResData = new HashSet<string>();
            foreach (GraphBBVertex vertex in graph.SortedVerticies)
                foreach (Addr3Code.Addr3Command cmd in vertex.Data.Body)

                    if (cmd.HasAssign())
                    {
                        int _tr;
                        ResData.Add(cmd.result);
                        if (cmd.arg1 != "" && !Int32.TryParse(cmd.arg1,out _tr) )
                            ResData.Add(cmd.arg1);
                        if (cmd.arg2 != "" && !Int32.TryParse(cmd.arg2, out _tr))
                            ResData.Add(cmd.arg2);
                    }
            return ResData;
        }
    }
}
