﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OptPoohCompiler.OptimizationsInBB.SharedExpressions;

namespace OptPoohCompiler.DataFlowAnalysis.LiveVariable
{   
    /// <summary>
    /// Множество данных анализа в задаче о активных переменных
    /// </summary>
    public class LiveVarDFADataType : SetDFADataType<string>, IDFADataType<LiveVarDFADataType>
    {
        /// <summary>
        /// Оператор сбора для множеств данных анализа
        /// активных переменных - объединение
        /// </summary>
        /// <param name="other">Второе множество данных анализа</param>
        /// <returns>Собранное множество данных анализа</returns>
        public LiveVarDFADataType Join(LiveVarDFADataType other)
        {
            LiveVarDFADataType result = new LiveVarDFADataType();
            result.UnionWith(this);
            result.UnionWith(other);
            return result;
        }

        public bool Equals(LiveVarDFADataType other)
        {
            return this.SetEquals(other);
        }

        public override string ToString()
        {
            return string.Join(", ", this);
        }
    }
}
