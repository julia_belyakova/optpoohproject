﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.GlobalOptimizations.GlobalSharedExpressions;

namespace OptPoohCompiler.GlobalOptimizations
{
    /// <summary>
    /// Средства глобальной оптимизации
    /// </summary>
    public static class GlobalOptimizationTools
    {
        /// Все доступные глобальные оптимизации
        public static IGlobalOptimization[] AllGlobalOptimizations = { 
                  new SharedExprsGlobalOptimization() };
    }
}
