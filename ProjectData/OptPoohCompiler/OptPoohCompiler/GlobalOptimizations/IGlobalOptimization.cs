﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.GlobalOptimizations
{
    /// <summary>
    /// Интерфейс глобальной оптимизации
    /// </summary>
    public interface IGlobalOptimization
    {
        /// <summary>
        /// Применяет к графу глобальную оптимизацию
        /// </summary>
        /// <param name="graph"></param>
        void Apply(GraphBB graph);
    }
}
