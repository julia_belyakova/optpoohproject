﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.OptimizationsInBB
{
    /// <summary>
    /// Тождественная оптимизация
    /// </summary>
    class IdentBBOptimization : IBBOptimization
    {
        /// <summary>
        /// Применяет оптимизацию к базову блоку
        /// </summary>
        /// <param name="data">Базовый блок</param>
        public void Apply(BasicBlock block)
        {
            // ничего не делаем
        }

        public override string ToString()
        {
            return "Тождественная оптимизация";
        }
    }
}
