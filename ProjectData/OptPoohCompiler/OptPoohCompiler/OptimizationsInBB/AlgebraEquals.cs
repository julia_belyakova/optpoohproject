﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;
using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.OptimizationsInBB
{
    class AlgebraEquals : IBBOptimization
    {
        /// <summary>  
        /// Применяет оптимизацию к базову блоку
        /// </summary>
        /// <param name="block">Базовый блок</param>
        public void Apply(BasicBlock block)
        {
            //структура для копии модифицируемой команды 
            //  
            Addr3Command kopi = new Addr3Command(); 
            Addr3Command curr = new Addr3Command();
            int priz;
            var com = block.Body.First;          // указатель на список 3-х адр команд 
            // for (int i = 1; i <= block.Body.Count; i++)
            while (com != null)
            {
                if (com.Value.cmdType == Addr3Code.Addr3CmdType.BIN_OPERATOR)
                {
                    // копируем исх команду  
                    curr = com.Value;
                    priz = 0;

                    switch (curr.cmdOperator)
                    {
                        case Addr3Code.Addr3CmdOperator.PLUS:
                            if ((curr.arg1 == "0") || (curr.arg2 == "0"))
                            {
                                if (curr.result.Substring(0, 1) == "%")    // если рабочая переменная 
                                {
                                    // если операция "+" и один и аргументов 0 и используется раб переменнная как результат
                                    //то модифицируем след команду 
                                    // заменяем рабочую переменную в новой  на переменную из исх команды 
                                    com = com.Next;
                                    kopi = com.Value;
                                    kopi.cmdLabel = curr.cmdLabel;
                                    if ((com.Value.arg1 == curr.result) && (curr.arg2 == "0"))
                                    {
                                        kopi.arg1 = curr.arg1;                    // имя реальной переменной 
                                        //ty = block.Body.Count;
                                        com.List.AddAfter(com, kopi);    // модифицированная команда 

                                        com = com.Next;                  // на новую 
                                        com.List.Remove(com.Previous);   // удаляем старую
                                        com.List.Remove(com.Previous);   // удаляем первоначальную с Addr3Code.Addr3CmdType.BIN_OPERATOR 

                                        priz++;
                                    }
                                    else
                                    {
                                        if ((com.Value.arg1 == curr.result) && (curr.arg1 == "0"))
                                        {
                                            kopi.arg1 = curr.arg2;  // ====
                                            com.List.AddAfter(com, kopi);    // модифицированная команда 

                                            com = com.Next;                  // на новую 
                                            com.List.Remove(com.Previous);   // удаляем старую
                                            com.List.Remove(com.Previous);   // удаляем первоначальную с Addr3Code.Addr3CmdType.BIN_OPERATOR 
                                            priz++;
                                        }
                                    }
                                    if ((com.Value.cmdType != Addr3Code.Addr3CmdType.BIN_OPERATOR) && (priz == 0))
                                    {
                                        com = com.Next;
                                    };
                                }
                                else   // нет рабочих переменных - заменяем на assign
                                {
                                    //kopi.arg1 = curr.arg1;                    //  тек команда 
                                    kopi = curr;
                                    kopi.cmdType = Addr3Code.Addr3CmdType.ASSIGN;
                                    kopi.arg2 = "";                    // 

                                    if (curr.arg2 == "0")
                                    {
                                    }
                                    else
                                    {
                                        kopi.arg1 = curr.arg2;
                                    }
                                   com.List.AddAfter(com, kopi);    // модифицированная команда 
                                   com = com.Next;                  // на новую 
                                   com.List.Remove(com.Previous);   // удаляем первоначальную с Addr3Code.Addr3CmdType.BIN_OPERATOR 
                                   priz++;

                                }
                              
                            }
                           else
                            {
                                com = com.Next;
                            }
                            break;
                      
                        case Addr3Code.Addr3CmdOperator.MINUS:
                            if  (curr.arg2 == "0")
                            {
                                if (curr.result.Substring(0, 1) == "%")    // если рабочая переменная 
                                {
                                    // если операция "-" и второй  аргумент 0 и используется раб переменнная как результат
                                    //то модифицируем след команду 
                                    // заменяем рабочую переменную в новой  на переменную из исх команды 
                                    com = com.Next;
                                    kopi = com.Value;
                                    kopi.cmdLabel = curr.cmdLabel;
                                    if (com.Value.arg1 == curr.result) 
                                    {
                                        kopi.arg1 = curr.arg1;                    // имя реальной переменной 
                                        com.List.AddAfter(com, kopi);    // модифицированная команда 

                                        com = com.Next;                  // на новую 
                                        com.List.Remove(com.Previous);   // удаляем старую
                                        com.List.Remove(com.Previous);   // удаляем первоначальную с Addr3Code.Addr3CmdType.BIN_OPERATOR 

                                        priz++;
                                    }
                                    else
                                    {
                                    }
                                    if ((com.Value.cmdType != Addr3Code.Addr3CmdType.BIN_OPERATOR) && (priz == 0))
                                    {
                                        com = com.Next;
                                    };
                                }
                                else   // нет рабочих переменных - заменяем на assign
                                {
                                    //kopi.arg1 = curr.arg1;                    //  тек команда 
                                    kopi = curr;
                                    kopi.cmdType = Addr3Code.Addr3CmdType.ASSIGN;
                                    kopi.arg2 = "";                    // 

                                   com.List.AddAfter(com, kopi);    // модифицированная команда 
                                   com = com.Next;                  // на новую 
                                   com.List.Remove(com.Previous);   // удаляем первоначальную с Addr3Code.Addr3CmdType.BIN_OPERATOR 
                                   priz++;

                                }
                              
                            }
                            else
                            {
                                com = com.Next;
                            }
                            break;
                        case Addr3Code.Addr3CmdOperator.MULT:
                            if ((curr.arg1 == "1") || (curr.arg2 == "1"))
                            {
                                if (curr.result.Substring(0, 1) == "%")    // если рабочая переменная 
                                {
                                    // если операция "+" и один и аргументов 1 и используется раб переменнная как результат
                                    //то модифицируем след команду 
                                    // заменяем рабочую переменную в новой  на переменную из исх команды 
                                    com = com.Next;
                                    kopi = com.Value;
                                    kopi.cmdLabel = curr.cmdLabel;
                                    if ((com.Value.arg1 == curr.result) && (curr.arg2 == "1"))
                                    {
                                        kopi.arg1 = curr.arg1;                    // имя реальной переменной 
                                        //ty = block.Body.Count;
                                        com.List.AddAfter(com, kopi);    // модифицированная команда 

                                        com = com.Next;                  // на новую 
                                        com.List.Remove(com.Previous);   // удаляем старую
                                        com.List.Remove(com.Previous);   // удаляем первоначальную с Addr3Code.Addr3CmdType.BIN_OPERATOR 

                                        priz++;
                                    }
                                    else
                                    {
                                        if ((com.Value.arg1 == curr.result) && (curr.arg1 == "1"))
                                        {
                                            kopi.arg1 = curr.arg2;  // ====
                                            com.List.AddAfter(com, kopi);    // модифицированная команда 

                                            com = com.Next;                  // на новую 
                                            com.List.Remove(com.Previous);   // удаляем старую
                                            com.List.Remove(com.Previous);   // удаляем первоначальную с Addr3Code.Addr3CmdType.BIN_OPERATOR 
                                            priz++;
                                        }
                                    }
                                    if ((com.Value.cmdType != Addr3Code.Addr3CmdType.BIN_OPERATOR) && (priz == 0))
                                    {
                                        com = com.Next;
                                    };
                                }
                                else   // нет рабочих переменных - заменяем на assign
                                {
                                    //kopi.arg1 = curr.arg1;                    //  тек команда 
                                    kopi = curr;
                                    kopi.cmdType = Addr3Code.Addr3CmdType.ASSIGN;
                                    kopi.arg2 = "";                    // 

                                    if (curr.arg2 == "1")
                                    {
                                    }
                                    else
                                    {
                                        kopi.arg1 = curr.arg2;
                                    }
                                   com.List.AddAfter(com, kopi);    // модифицированная команда 
                                   com = com.Next;                  // на новую 
                                   com.List.Remove(com.Previous);   // удаляем первоначальную с Addr3Code.Addr3CmdType.BIN_OPERATOR 
                                   priz++;

                                }
                              
                            }
                           else
                            {
                                com = com.Next;
                            }
                            
                            break;
                        case Addr3Code.Addr3CmdOperator.DIVIDE:
                            if  (curr.arg2 == "1")
                            {
                                if (curr.result.Substring(0, 1) == "%")    // если рабочая переменная 
                                {
                                    // если операция "/" и второй  аргумент 1 и используется раб переменнная как результат
                                    //то модифицируем след команду 
                                    // заменяем рабочую переменную в новой  на переменную из исх команды 
                                    com = com.Next;
                                    kopi = com.Value;
                                    kopi.cmdLabel = curr.cmdLabel;
                                    if (com.Value.arg1 == curr.result) 
                                    {
                                        kopi.arg1 = curr.arg1;                    // имя реальной переменной 
                                        com.List.AddAfter(com, kopi);    // модифицированная команда 

                                        com = com.Next;                  // на новую 
                                        com.List.Remove(com.Previous);   // удаляем старую
                                        com.List.Remove(com.Previous);   // удаляем первоначальную с Addr3Code.Addr3CmdType.BIN_OPERATOR 

                                        priz++;
                                    }
                                    else
                                    {
                                    }
                                    if ((com.Value.cmdType != Addr3Code.Addr3CmdType.BIN_OPERATOR) && (priz == 0))
                                    {
                                        com = com.Next;
                                    };
                                }
                                else   // нет рабочих переменных - заменяем на assign
                                {
                                    //kopi.arg1 = curr.arg1;                    //  тек команда 
                                    kopi = curr;
                                    kopi.cmdType = Addr3Code.Addr3CmdType.ASSIGN;
                                    kopi.arg2 = "";                    // 

                                   com.List.AddAfter(com, kopi);    // модифицированная команда 
                                   com = com.Next;                  // на новую 
                                   com.List.Remove(com.Previous);   // удаляем первоначальную с Addr3Code.Addr3CmdType.BIN_OPERATOR 
                                   priz++;

                                }
                              
                            }

                            else
                            {
                                com = com.Next;
                            }

                            break;

                        default:
                            com = com.Next;
                            break;
                    }

                }
                else
                {
                    com = com.Next;
                }
         //         if (com.Value.cmdType != Addr3Code.Addr3CmdType.BIN_OPERATOR)
         //       if (com.Next != null)
         //       {
         //          com = com.Next;
         //        } ;
            }
       //     throw new NotImplementedException();                   
            
 
        }

        public override string ToString()
        {
            return "Алгебраические тождества";
        }
    }
}
