﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OptPoohCompiler.DataFlowAnalysis.LiveVariable;

namespace OptPoohCompiler.OptimizationsInBB
{
    class DeadCodeElimination : IBBOptimization
    {
        #region IBBOptimization Members

        int currLabelNumber=0;
        /// <summary>
        /// Информация о переменной для разбора кода
        /// </summary>
        public class VarInfo
        {
            /// <summary>
            /// Имя переменной
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Жива ли она
            /// </summary>
            public bool IsAlive { get; set; }
            /// <summary>
            /// Метка переменной
            /// </summary>
            public string Label { get; set; }

            public VarInfo()
            {
                Name = "";
                IsAlive = true;
                Label = "";
            }

            public VarInfo(string Name, string Label)
            {
                this.Name = Name;
                this.IsAlive = true;
                this.Label = Label;
            }

            public VarInfo(string Name, bool IsAlive, string Label)
            {
                this.Name = Name;
                this.IsAlive = IsAlive;
                this.Label = Label;
            }
        }

        public class ComInfo : Addr3Code.IAddr3CommandInfo
        {
            public bool isAlive1 { get; set; }
            public bool isAlive2 { get; set; }
            public bool isAliveRes { get; set; }

            public ComInfo()
            {
                isAlive1 = isAlive2 = isAliveRes = true;
            }

            public ComInfo(bool isAlive1, bool isAlive2, bool isAliveRes)
            {
                this.isAlive1 = isAlive1;
                this.isAlive2 = isAlive2;
                this.isAliveRes = isAliveRes;

            }


        }

        string GetNewCommandLabelName()
        {
            return "c" +(currLabelNumber++);
        }


        public void Apply(BasicBlocks.BasicBlock block)
        {
            while (MyApply(block));
        }

        public bool MyApply(BasicBlocks.BasicBlock block)
        {
            bool smthChanged = false;
            //какая-то проверка
            if (block.Body.Count == 0)
                return false;

            List<VarInfo> InfoTable = new List<VarInfo>();
            var com = block.Body.First;
            //заполняем переменными таблицу;
            //для всех помечаем, что они живые.
            for (int i = 0; i < block.Body.Count; ++i)
            {
                int a;
                if (com.Value.arg1 != "" && !InfoTable.Exists(x => x.Name == com.Value.arg1) && !Int32.TryParse(com.Value.arg1, out a))
                    InfoTable.Add(new VarInfo(com.Value.arg1, true, com.Value.cmdLabel));
                if (com.Value.arg2 != "" && !InfoTable.Exists(x => x.Name == com.Value.arg2) && !Int32.TryParse(com.Value.arg2, out a))
                    InfoTable.Add(new VarInfo(com.Value.arg2, true, com.Value.cmdLabel));
                if (!InfoTable.Exists(x => x.Name == com.Value.result))
                    InfoTable.Add(new VarInfo(com.Value.result, true, com.Value.cmdLabel));
                com = com.Next;
            }
            com = block.Body.Last;

            // проходим в обратном порядке;
            // записываем в инфу команды предыдущее значение переменной(берем из таблицы), стоящей слева от знака "=";
            // обновляем таблицу.
            for (int i = block.Body.Count; i > 0; --i)
            {

                if (com.Value.cmdType == Addr3Code.Addr3CmdType.ASSIGN || com.Value.cmdType == Addr3Code.Addr3CmdType.BIN_OPERATOR)
                {
                    var newCom = com.Value; //создаем новую команду, как две капли воды похожую на старую
                    //newCom.cmdLabel = com.Value.cmdLabel == "" ? Addr3Code.Addr3CodeGenTools.NextLabelName() : com.Value.cmdLabel;  //если у нее нет метки сделаем её
                    VarInfo r;
                    r = InfoTable.FirstOrDefault(x => x.Name == com.Value.arg1);
                    newCom.info = new ComInfo(
                        (r = InfoTable.FirstOrDefault(x => x.Name == com.Value.arg1)) != null ? r.IsAlive : true,
                        (r = InfoTable.FirstOrDefault(x => x.Name == com.Value.arg2)) != null ? r.IsAlive : true,
                        (r = InfoTable.FirstOrDefault(x => x.Name == com.Value.result)) != null ? r.IsAlive : true
                        ); //для новой команды заполним инфу из таблицы

                    block.Body.AddAfter(com, new LinkedListNode<Addr3Code.Addr3Command>(newCom));   //добавим её после старой, 
                    block.Body.Remove(com);             //а старую в топку

                    //меняем значения в таблице
                    if (InfoTable.Any(x => x.Name == newCom.result))
                        InfoTable.First(x => x.Name == newCom.result).IsAlive =  false;
                    if (InfoTable.Any(x => x.Name == newCom.arg1))
                        InfoTable.First(x => x.Name == newCom.arg1).IsAlive = true;
                    if (InfoTable.Any(x => x.Name == newCom.arg2))
                        InfoTable.First(x => x.Name == newCom.arg2).IsAlive = true;
                    com = block.Body.Find(newCom).Previous;
                }
                else
                    com = com.Previous;

            }

            // идем прямо по болку;
            // удаляем те строки, информация У КОТОРЫХ (не из таблицы) говорит о том, что левая часть мертва //этот коментарий надо отправить психоаналитику или логопеду
            com = block.Body.First;
            while (com != null)
            {
                if (com.Value.cmdType == Addr3Code.Addr3CmdType.ASSIGN || com.Value.cmdType == Addr3Code.Addr3CmdType.BIN_OPERATOR)
                {
                    if (!(com.Value.info as ComInfo).isAliveRes)
                    {
                        smthChanged = true; 
                        if (com != block.Body.Last)
                            block.Body.Remove((com = com.Next).Previous);
                        else
                        {
                            block.Body.RemoveLast();
                            break;
                        }
                    }
                    else
                        com = com.Next;
                }
                else
                    com = com.Next;
            }

            return smthChanged;
        }

        public override string ToString()
        {
            return "Удаление мертвого кода";
        }

        
        #endregion
    }
}
