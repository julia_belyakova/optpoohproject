﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.OptimizationsInBB
{
    /// <summary>
    /// Тип оптимизации внутри базового блока
    /// </summary>
    public enum BBOptimizationTypes { IDENT }

    /// <summary>
    /// Статический класс инструментов оптимизации внутри ББ
    /// </summary>
    public static class BBOptimizationTools
    {
        /// Все доступные оптимизации
        public static IBBOptimization[] AllBBOptimizations = { 
                  new IdentBBOptimization(), new DeadCodeElimination(), 
                  new AlgebraEquals(), new SharedExprsBBOpt() };

        
        /// <summary>
        /// Применяет коллекцию оптимизаций ко всем ББ графа
        /// </summary>
        /// <param name="graph">Граф базовых блоков</param>
        /// <param name="opts">Коллекция оптимизаций</param>
        public static void ApplyBBPOtimizations(GraphBB graph, ICollection<IBBOptimization> opts)
        {
            foreach (GraphBBVertex vertex in graph.SortedVerticies)
                foreach (IBBOptimization opt in opts)
                    opt.Apply(vertex.Data);
        }

        /// <summary>
        /// Применяет оптимизацию ко всем ББ графа
        /// </summary>
        /// <param name="graph">Граф базовых блоков</param>
        /// <param name="opt">Оптимизация</param>
        public static void ApplyBBPOtimizations(GraphBB graph, IBBOptimization opt)
        {
            foreach (GraphBBVertex vertex in graph.SortedVerticies)
                    opt.Apply(vertex.Data);
        }
    }
}
