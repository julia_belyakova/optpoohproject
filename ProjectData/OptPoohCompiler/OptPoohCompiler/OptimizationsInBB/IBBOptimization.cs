﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.OptimizationsInBB
{
    /// <summary>
    /// Оптимизация
    /// </summary>
    public interface IBBOptimization
    {
        /// <summary>
        /// Применяет оптимизацию к базову блоку
        /// </summary>
        /// <param name="data">Базовый блок</param>
        void Apply(BasicBlock block);
    }
}
