﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.OptimizationsInBB.SharedExpressions
{
    /// <summary>
    /// Таблица пар (имя переменной; список переменных, хранящих её значение)
    /// </summary>
    class VarValuesTable
    {
        /// <summary>
        /// Таблица пар 
        /// </summary>
        private SortedDictionary<string, LinkedList<string>> table;

        /// <summary>
        /// Таблица пар (имя переменной; список переменных, хранящих её значение)
        /// </summary>
        public VarValuesTable() 
        {
            this.table = new SortedDictionary<string, LinkedList<string>>();
        }

        /// <summary>
        /// Проверяет, есть ли в таблице запись о переменной varName
        /// </summary>
        /// <param name="varName">Имя переменной</param>
        /// <returns>Истину, если в таблице есть запись varName</returns>
        public bool Contains(string varName)
        {
            return table.ContainsKey(varName);
        }

        /// <summary>
        /// Добавляет в таблицу информацию о переменной, для которой
        /// список эквивалентных переменных состоит из неё самой
        /// </summary>
        /// <param name="newVarName">Имя новой переменной</param>
        public void AddIdentVarInfo(string newVarName)
        {
            LinkedList<string> varList = new LinkedList<string>();
            varList.AddLast(newVarName);
            table.Add(newVarName, varList);
        }

        /// <summary>
        /// Добавляет в таблицу информацию о переменной с 
        /// заданным списком эквивалентных переменных
        /// </summary>
        /// <param name="newVarName">Имя новой переменной</param>
        /// <param name="equalVars">Список эквивалентных переменных</param>
        public void AddVarInfo(string newVarName, LinkedList<string> equalVars)
        {
            table.Add(newVarName, equalVars);
        }

        /// <summary>
        /// Обновляет в таблице информацию о переменной с новым значением
        /// </summary>
        /// <param name="updatedVarName">Обновляемая переменная</param>
        public void UpdateIdentVarInfo(string updatedVarName)
        {
            LinkedList<string> varList = new LinkedList<string>();
            varList.AddLast(updatedVarName);
            table[updatedVarName] = varList;
        }

        /// <summary>
        /// Обновляет в таблице информацию о переменной с 
        /// заданным списком эквивалентных переменных
        /// </summary>
        /// <param name="updatedVarName">Обновляемая переменная</param>
        /// <param name="equalVars">Список эквивалентных переменных</param>
        public void UpdateVarInfo(string updatedVarName, LinkedList<string> equalVars)
        {
            table[updatedVarName] = equalVars;
        }

        /// <summary>
        /// Находит в таблице список переменных, которые хранят значение,
        /// эквивалентное переменной varName
        /// </summary>
        /// <param name="varName">Имя переменной</param>
        /// <returns>Список эквивалентных переменных</returns>
        public LinkedList<string> GetEqualVarsList(string varName)
        {
            return table[varName];
        }
        
    }
}
