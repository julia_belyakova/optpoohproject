﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;
using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.OptimizationsInBB.SharedExpressions;

namespace OptPoohCompiler.OptimizationsInBB
{
    /// <summary>
    /// Оптимизация общих подвыражений
    /// </summary>
    public class SharedExprsBBOpt : IBBOptimization
    {
        /// <summary>
        /// Таблица переменных
        /// </summary>
        private VarValuesTable varsTable;
        /// <summary>
        /// Таблица выражений
        /// </summary>
        private SharedExprsTable exprsTable;
        /// <summary>
        /// Таблица пар (имя переменной; ссылка на выражение в таблице выражений)
        /// </summary>
        private Dictionary<string, SharedExpression> exprRecsTable;

        /// <summary>
        /// Оптимизация общих подвыражений
        /// </summary>
        public SharedExprsBBOpt() 
        {
            _Reset();
        }

        /// <summary>
        /// Применяет оптимизацию к базову блоку
        /// </summary>
        /// <param name="block">Базовый блок</param>
        public void Apply(BasicBlock block)
        {
            if (block.Body.Count == 0)
                return;
            var currSourceNode = block.Body.First;
            var lastSourceNode = block.Body.Last;
            // цикл по всем командам блока 
            // сверху вниз
            while (currSourceNode != lastSourceNode.Next)
            {
                Addr3Command oldCmd = currSourceNode.Value;
                Addr3Command newCmd = oldCmd;
                // обрабатываем только присваивания
                if (oldCmd.HasAssign())
                {
                    // самое простое -- обработка обычного присваивания
                    if (oldCmd.cmdType == Addr3CmdType.ASSIGN)
                        newCmd = _ProcessAssign(oldCmd);
                    else
                        newCmd = _ProcessExpression(oldCmd);
                }
                // добавляем в конец такую же или оптимизированную команду
                block.Body.AddLast(newCmd);
                // переходим к следующей команде
                currSourceNode = currSourceNode.Next;
            }
            lastSourceNode = lastSourceNode.Next;
            // после lastSourceNode находятся оптимизированные команды блока,
            // нужно удалить все, что до
            while (block.Body.First != lastSourceNode)
                block.Body.RemoveFirst();

            // очищаем таблицы
            _Reset();
        }

        public override string ToString()
        {
            return "Оптимизация общих подвыражений";
        }

        /// <summary>
        /// Очищает содержимое таблиц
        /// </summary>
        private void _Reset()
        {
            varsTable = new VarValuesTable();
            exprsTable = new SharedExprsTable();
            exprRecsTable = new Dictionary<string, SharedExpression>();
        }

        /// <summary>
        /// Обработка команды присваивания 
        /// </summary>
        /// <param name="cmd">Команда присваивания</param>
        /// <returns>Трехадресную команду после оптимизации</returns>
        private Addr3Command _ProcessAssign(Addr3Command cmd)
        {
            var left = cmd.result;
            var right = cmd.arg1;
            // на всякий случай выбросим случай x = x
            if (left == right)
                return cmd;
            // если справа число, то обрабатываем команду как выражение
            if (_IsNumber(right))
                return _ProcessExpression(cmd);
            // теперь справа переменная

            // если справа неизвестная ранее переменная, надо добавить
            // о ней пустую запись
            if (!varsTable.Contains(right))
                _AddNewVarInfo(right);
            // если слева новая переменная
            if (!varsTable.Contains(left))
            {
                // если слева новая переменная, нужно найти список эквивалентных переменных, 
                // в которых хранится значение [right], и добавить нашу переменную
                // в этот список
                _AddEqualVariable(left, right);
                // в качестве правильной правой части нужно взять первую
                // в списке эквивалентную переменную
                right = varsTable.GetEqualVarsList(right).First.Value;
            }
            else
            { 
                // запись о переменной уже есть в таблице, нужно обновить информацию о ней.
                // Сначала нужно обработать список эквивалентных переменных, в которых
                // содержится left, удалив некоторые записи, если это нужно
                _ClearOldVarInfo(left);
                _UpdateEqualVariable(left, right);
                // в качестве правильной правой части нужно взять первую
                // в списке эквивалентную переменную
                right = varsTable.GetEqualVarsList(right).First.Value;
            }
            Addr3Command optimizedCmd = new Addr3Command(left, right);
            if (cmd.HasLabel())
                optimizedCmd.cmdLabel = cmd.cmdLabel;
            return optimizedCmd;
        }

        /// <summary>
        /// Обработка команды присваивания выражения
        /// </summary>
        /// <param name="cmd">Команда присваивания выражения</param>
        private Addr3Command _ProcessExpression(Addr3Command cmd)
        {
            Addr3Command optimizedCmd;
            string left = cmd.result;
            // нужно сформировать правильное выражение и поискать
            // его в таблице выражений.
            // (переменные в выражении нужно заменить на их первые эквиваленты)
            string arg1 = cmd.arg1;
            _PrepareExprArg(ref arg1);
            string arg2 = cmd.arg2;
            if (arg2 != "")
                _PrepareExprArg(ref arg2);
            SharedExpression expr = new SharedExpression(cmd.cmdOperator, arg1, arg2);
            // если такое выражение есть, то ок, просто используем переменную, в которой
            // оно хранится
            if (exprsTable.ContainsRecord(expr))
            {
                // если слева новая переменная
                if (!varsTable.Contains(left))
                    _AddNewVarInfo(left);
                else
                    _ClearOldVarInfo(left);
                string right = exprsTable.GetVariables(expr).First.Value;
                _UpdateEqualVariable(left, right);
                optimizedCmd = new Addr3Command(left, right);
            }
            else
            {
                // эквивалентная переменная для обновляемой, если есть
                string equalVar = "";
                // добавляем запись в таблицу переменных или очищаем
                // старую информацию:
                // если слева новая переменная
                if (!varsTable.Contains(left))
                    _AddNewVarInfo(left);
                else
                {
                    var equalVars = varsTable.GetEqualVarsList(left);
                    _ClearOldVarInfo(left);
                    varsTable.UpdateIdentVarInfo(left);
                    if (equalVars.Count > 0)
                        equalVar = equalVars.First.Value;
                }
                // нужно добавить выражение в таблицу выражений,
                // если его правая часть не содержит переменную слева
                // (в противном случае такое выражение может сразу стать недействительным)
                if (!expr.ContainsVariable(left))
                {
                    exprsTable.AddRecord(expr, varsTable.GetEqualVarsList(left));
                    exprRecsTable[left] = expr;
                }
                else 
                    // Но если для обновляемой переменной были эквивалентые, то
                    // наше выражение справа можно тоже обновить
                    if (equalVar != "")
                    {
                        expr = expr.SubstituteVariable(left, equalVar);
                        exprsTable.AddRecord(expr, varsTable.GetEqualVarsList(left));
                        exprRecsTable[left] = expr;
                    }
                // к-р для бинарного оператора
                optimizedCmd = new Addr3Command(left, arg1, arg2, cmd.cmdOperator);
                // надо еще поменять тип оператора (это может быть унарный или присваивание)
                optimizedCmd.cmdType = cmd.cmdType;

            }
            if (cmd.HasLabel())
                optimizedCmd.cmdLabel = cmd.cmdLabel;
            return optimizedCmd;
        }

        /// <summary>
        /// Добавляет информацию о переменной с неизвестным значением
        /// в нужные таблицы
        /// </summary>
        /// <param name="newVar">Имя переменной</param>
        private void _AddNewVarInfo(string newVar)
        {
            varsTable.AddIdentVarInfo(newVar);
            exprRecsTable[newVar] = null;
        }

        /// <summary>
        /// Обрабатывает аргумент выражения и заменяет его на правильное значение.
        /// (Если это переменная, то она заменяется на свой первый эквивалент)
        /// </summary>
        /// <param name="arg">Значение аргумента</param>
        private void _PrepareExprArg(ref string arg)
        {
            if (!_IsNumber(arg))
            {
                if (!varsTable.Contains(arg))
                    _AddNewVarInfo(arg);
                else
                {
                    // если для переменной есть выражение, являющееся числом [expr.IsValue()],
                    // то выгодно заменить переменную на число,
                    // иначе заменяем на первую переменную в списке эквивалентных
                    SharedExpression expr = exprRecsTable[arg];
                    if ((expr == null) || !expr.IsValue())
                        arg = varsTable.GetEqualVarsList(arg).First.Value;
                    else
                        arg = expr.GetValue();
                }
            }
        }

        /// <summary>
        /// Добавляет в таблицу переменных переменную newVar, которая
        /// эквивалентна уже имеющейся переменной valueVar
        /// </summary>
        /// <param name="newVar">Новая переменная</param>
        /// <param name="valueVar">Уже имеющаяся в таблице переменная</param>
        private void _AddEqualVariable(string newVar, string valueVar)
        {
            LinkedList<string> eqVarsList = varsTable.GetEqualVarsList(valueVar);
            // нужно добавить новую переменную в этот список эквивалентных переменных
            eqVarsList.AddLast(newVar);
            // теперь в таблицу переменных нужно добавить запись о новой переменной,
            // которая ссылается на тот же список эквивалентных переменных
            varsTable.AddVarInfo(newVar, eqVarsList);
            // запись для новой переменной такая же, как для right
            exprRecsTable.Add(newVar, exprRecsTable[valueVar]);
        }

        /// <summary>
        /// Обновляет в таблице переменных запись для existVar, которая
        /// эквивалентна уже имеющейся переменной valueVar
        /// </summary>
        /// <param name="newVar">Обновляемая переменная</param>
        /// <param name="valueVar">Уже имеющаяся в таблице переменная</param>
        private void _UpdateEqualVariable(string existVar, string valueVar)
        {
            LinkedList<string> eqVarsList = varsTable.GetEqualVarsList(valueVar);
            // нужно добавить новую переменную в этот список эквивалентных переменных
            eqVarsList.AddLast(existVar);
            // теперь в таблицу переменных нужно обновить запись о переменной,
            // которая ссылается на тот же список эквивалентных переменных
            varsTable.UpdateVarInfo(existVar, eqVarsList);
            // запись для обновляемой переменной такая же, как для right
            exprRecsTable[existVar] = exprRecsTable[valueVar];
        }

        /// <summary>
        /// Стирает из таблицы переменных данные о старом значении переменной varName,
        /// а также обновляет записи таблицы выражений, которые содержат переменную varName
        /// </summary>
        /// <param name="varName"></param>
        private void _ClearOldVarInfo(string varName)
        {
            // находим список переменных, эквивалентных varName, и убираем оттуда
            // саму varName
            LinkedList<string> eqVarsList = varsTable.GetEqualVarsList(varName);
            eqVarsList.Remove(varName);
            // при этом список мог стать пустым (если не было эквивалентных переменных).
            // Таблица выражений могла содержать выражение, сохраненное в varName,
            // а также выражения, в которые входит varName. 
            // Если эквивалентных значений больше нет, то нужно удалить все такие записи.
            // В противном случае нужно заменить выражения, содержащие varName, на
            // выражения, в которые вместо неё подставлена эквивалентная переменная

            // возможное выражение, соответствующее переменной
            SharedExpression varExpr = exprRecsTable[varName];
            // сама переменная больше не ссылается на выражение
            exprRecsTable[varName] = null;

            // у этой переменной нет эквивалентных
            if (eqVarsList.Count == 0)
            {
                // если есть выражение, запись о нём нужно удалить из таблицы выражений
                if (varExpr != null)
                    exprsTable.RemoveRecord(varExpr);
                // теперь нужно удалить все записи, в которых встречается переменная varName
                LinkedList<string> containingVars = exprsTable.RemoveRecordsWithVar(varName);
                // стираем информацию о выражениях
                foreach (string var in containingVars)
                    exprRecsTable[var] = null;
            }
            else 
            {
                // во всех выражениях, где встречается varName, её нужно заменить
                // на первую эквивалентную
                string correctEqVar = eqVarsList.First.Value;
                exprsTable.UpdateRecordsWithVar(varName, correctEqVar);
            }
        }

        /// <summary>
        /// Проверяет, является ли строка value числом
        /// </summary>
        /// <param name="value">Строка-значение</param>
        /// <returns>Истину, если value является числом</returns>
        private bool _IsNumber(string value)
        {
            int number;
            return int.TryParse(value, out number);
        }
    }
}
