﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;

/* 
 * Вспомогательные классы для оптимизации общих подвыражений внутри базового блока
 * */

namespace OptPoohCompiler.OptimizationsInBB.SharedExpressions
{
    /// <summary>
    /// Выражение с унарным или бинарным оператором, 
    /// которое используется в оптимизации общих подвыражений
    /// </summary>
    public class SharedExpression : IComparable<SharedExpression>, IEquatable<SharedExpression>
    {   
        /// <summary>
        /// Оператор команды 
        /// </summary>
        private Addr3CmdOperator exprOperator;
        /// <summary>
        /// Первый операнд
        /// </summary>
        private string arg1;
        /// <summary>
        /// Второй операнд (может быть пустым)
        /// </summary>
        private string arg2;

        /// <summary>
        /// Выражение с унарным или бинарным оператором, 
        /// которое используется в оптимизации общих подвыражений
        /// </summary>
        /// <param name="cmd">Трехадресная команда</param>
        public SharedExpression(Addr3Command cmd)
        {
            if (!cmd.HasAssign() || (cmd.cmdType == Addr3CmdType.ASSIGN))
                throw new ArgumentOutOfRangeException(
                    "Ожидалась команда присваивания выражения");
            this.exprOperator = cmd.cmdOperator;
            this.arg1 = cmd.arg1;
            this.arg2 = cmd.arg2;
        }
        /// <summary>
        /// Выражение с унарным или бинарным оператором, 
        /// которое используется в оптимизации общих подвыражений
        /// </summary>
        /// <param name="oper">Оператор (корректность не проверяется)</param>
        /// <param name="arg1">Первый операнд</param>
        /// <param name="arg2">Второй операнд (может быть пустым)</param>
        public SharedExpression(Addr3CmdOperator oper, string arg1, string arg2)
        {
            if (arg1 == "")
                throw new ArgumentNullException("Первый операнд не может быть пустым");
            this.exprOperator = oper;
            this.arg1 = arg1;
            this.arg2 = arg2;
        }
        /// <summary>
        /// Выражение-число
        /// </summary>
        /// <param name="value">Числовое значение</param>
        public SharedExpression(string value)
        {
            if (value == "")
                throw new ArgumentNullException("Числовой операнд не может быть пустым");
            this.exprOperator = Addr3CmdOperator.EMPTY;
            this.arg1 = value;
            this.arg2 = "";
        }

        /// <summary>
        /// Проверяет, является ли выражение числовым значением
        /// </summary>
        /// <returns>Истину, если выражение является числовым значением</returns>
        public bool IsValue()
        {
            return (this.exprOperator == Addr3CmdOperator.EMPTY) && (arg2 == "");
        }

        /// <summary>
        /// Возвращает числовое значение, которым является
        /// выражение (корректность выражения не проверяется)
        /// </summary>
        /// <returns>Числовое значение</returns>
        public string GetValue()
        {
            return this.arg1;
        }

        /// <summary>
        /// Проверяет, содержит ли выражение переменную varName
        /// </summary>
        /// <param name="varName">Имя переменной</param>
        /// <returns>Истину, если выражение содержит переменную varName</returns>
        public bool ContainsVariable(string varName)
        {
            return (arg1 == varName) || (arg2 == varName);
        }

        /// <summary>
        /// Производит в выражении подставновку переменной destVar вместо sourceVar
        /// </summary>
        /// <param name="sourceVar">Имя исходной переменной</param>
        /// <param name="destVar">Имя новой переменной</param>
        /// <returns>Новое выражение после подстановки</returns>
        public SharedExpression SubstituteVariable(string sourceVar, string destVar)
        {
            return new SharedExpression(
                this.exprOperator,
                (this.arg1 == sourceVar) ? destVar : arg1,
                (this.arg2 == sourceVar) ? destVar : arg2);
        }

        /// <summary>
        /// Сравнивает текущее выражение с other в лексикографическом порядке
        /// оператор, первый аргумент, второй аргумент
        /// </summary>
        /// <param name="other">Выражение</param>
        /// <returns>Возвращает 1, если текущий объект больше other</returns>
        public int CompareTo(SharedExpression other)
        {
            int cmpResult = this.exprOperator.CompareTo(other.exprOperator);
            if (cmpResult != 0)
                return cmpResult;
            cmpResult = this.arg1.CompareTo(other.arg1);
            if (cmpResult != 0)
                return cmpResult;
            return this.arg2.CompareTo(other.arg2);
        }

        public bool Equals(SharedExpression other)
        {
            return (this.exprOperator == other.exprOperator)
                && (this.arg1 == other.arg1)
                && (this.arg2 == other.arg2);
        }

        public override bool Equals(object obj)
        {
            SharedExpression other = obj as SharedExpression;
            if (other == null)
                return false;
            return this.Equals(other);
        }

        public override int GetHashCode()
        {
            return this.arg1.GetHashCode() * 1291 
                + this.arg2.GetHashCode() * 727 
                - this.exprOperator.GetHashCode() * 13;
        }

        /// <summary>
        /// Строкоое представление выражения
        /// </summary>
        /// <returns>Строку-описание выражения</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (this.exprOperator != Addr3CmdOperator.EMPTY)
                if (this.arg2 != "")
                {
                    sb.Append(arg1);
                    sb.Append(" " + Addr3CmdOperatorTools.ToString(this.exprOperator) + " ");
                    sb.Append(arg2);
                }
                else
                {
                    sb.Append(Addr3CmdOperatorTools.ToString(this.exprOperator) + " ");
                    sb.Append(arg1);
                }
            else
                sb.Append(arg1);
            return sb.ToString();
        }
    }
}
