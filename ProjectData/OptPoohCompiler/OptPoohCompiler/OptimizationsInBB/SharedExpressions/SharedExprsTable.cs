﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;

namespace OptPoohCompiler.OptimizationsInBB.SharedExpressions
{
    /// <summary>
    /// Таблица общих выражений
    /// </summary>
    class SharedExprsTable
    {
        /// <summary>
        /// Таблица пар (выражение; переменные, в которых оно хранится)
        /// </summary>
        private SortedDictionary<SharedExpression, LinkedList<string>> table;

        /// <summary>
        /// Таблица общих выражений
        /// </summary>
        public SharedExprsTable()
        {
            table = new SortedDictionary<SharedExpression, LinkedList<string>>();
        }

        /// <summary>
        /// Добавляет в таблицу выражений запись о новом выражении
        /// </summary>
        /// <param name="newExpr">Новое выражение</param>
        /// <param name="variables">Список эквивалентных переменных</param>
        public void AddRecord(SharedExpression newExpr, LinkedList<string> variables)
        {
            table.Add(newExpr, variables);
        }

        /// <summary>
        /// Проверяет, содержится ли в таблице запись для выражения expr
        /// </summary>
        /// <param name="expr">Искомое выражение</param>
        /// <returns>Истину, если в таблице есть выражение expr</returns>
        public bool ContainsRecord(SharedExpression expr)
        {
            return table.ContainsKey(expr);
        }

        /// <summary>
        /// Определяет список переменных, в которых содержится выражение expr
        /// </summary>
        /// <param name="expr">Выражение из таблицы выражений</param>
        /// <returns>Список эквивалентных переменных, содержащих expr</returns>
        public LinkedList<string> GetVariables(SharedExpression expr)
        {
            return table[expr];
        }

        /// <summary>
        /// Удаляет из таблицы выражений запись о выражении expr
        /// </summary>
        /// <param name="expr">Выражение, которое нужно удалить</param>
        public void RemoveRecord(SharedExpression expr)
        {
            table.Remove(expr);
        }

        /// <summary>
        /// Удаляет из таблицы выражений все записи, в которых содержится переменная varName.
        /// Возвращает список переменных, которые эквивалентны этим выражениям.
        /// </summary>
        /// <param name="varName">Имя переменной, которая поменяла значение</param>
        /// <returns>Список переменных, содержавший выражения с varName</returns>
        public LinkedList<string> RemoveRecordsWithVar(string varName)
        {
            LinkedList<string> containingVars = new LinkedList<string>();
            // список выражений, в которых есть varName, и которые нужно удалить
            LinkedList<SharedExpression> removingExprs = new LinkedList<SharedExpression>();
            foreach (KeyValuePair<SharedExpression, LinkedList<string>> record in table)
                if (record.Key.ContainsVariable(varName))
                {
                    removingExprs.AddLast(record.Key);
                    // запоминаем имена переменных, в которых находилось выражение
                    foreach (string var in record.Value)
                        containingVars.AddLast(var);
                }
            foreach (SharedExpression expr in removingExprs)
                table.Remove(expr);
            return containingVars;
        }

        /// <summary>
        /// Обновляет записи с выражениями, в которых содержится oldVarName, заменяя
        /// её на доступную эквивалентную переменную equalVarName
        /// </summary>
        /// <param name="oldVarName">Имя обновляемой переменной</param>
        /// <param name="equalVarName">Имя эквивалентной переменной</param>
        public void UpdateRecordsWithVar(string oldVarName, string equalVarName)
        {
            // нужно удалить старые записи и вместо них добавить новые
            // список записей, в которых есть varName, и которые нужно удалить
            LinkedList<KeyValuePair<SharedExpression, LinkedList<string>>> updatingRecs
                = new LinkedList<KeyValuePair<SharedExpression, LinkedList<string>>>();
            foreach (KeyValuePair<SharedExpression, LinkedList<string>> record in table)
                if (record.Key.ContainsVariable(oldVarName))
                    updatingRecs.AddLast(record);
            foreach (KeyValuePair<SharedExpression, LinkedList<string>> rec in updatingRecs)
            {
                table.Remove(rec.Key);
                table.Add(rec.Key.SubstituteVariable(oldVarName, equalVarName), rec.Value);
            }
        }
    }
}
