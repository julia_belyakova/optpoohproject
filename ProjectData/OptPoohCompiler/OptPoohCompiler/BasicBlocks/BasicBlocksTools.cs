﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;

namespace OptPoohCompiler.BasicBlocks
{
    /// <summary>
    /// Вспомогательный класс для работы с базовыми блоками
    /// и графом базового блока
    /// </summary>
    public static class BasicBlocksTools
    {
        /// Имя базового блока
        public const string BASIC_BLOCK_NAME = "B";
        /// Номер последнего выданного базового блока
        private static int currBasicBlockNum = -1;

        /// <summary>
        /// Генерирует новое имя базового блока
        /// </summary>
        /// <returns>Имя для ББ, которое еще не использовалось</returns>
        public static string NextBasicBlockName()
        {
            ++currBasicBlockNum;
            return (BASIC_BLOCK_NAME + currBasicBlockNum.ToString());
        }
        /// <summary>
        /// Сбрасывает генератор имен базовых блоков в начальное значение
        /// </summary>
        public static void ResetBasicBlockGenerator()
        {
            currBasicBlockNum = -1;
        }

        /// <summary>
        /// Возвращает граф базовых блоков, соответствующий
        /// списку трехадресных команд commands
        /// </summary>
        /// <param name="commands">Список трехадресных команд</param>
        /// <returns>Граф базовых блоков</returns>
        public static GraphBB MakeBasicBlockGraph(LinkedList<Addr3Command> commands)
        {
            // словарь меток и ББ, которые начинаются с этих меток
            Dictionary<string, GraphBBVertex> blocksByLabels
                = new Dictionary<string, GraphBBVertex>();

            // список вершин-базовых блоков программы
            LinkedList<GraphBBVertex> blocks = new LinkedList<GraphBBVertex>();
            // текущий базовый блок, который мы заполняем
            GraphBBVertex currBBVertex = new GraphBBVertex(
                new BasicBlock(BasicBlocksTools.NextBasicBlockName()));
            // для единообразной обработки сделаем фиктивный базовый блок входа,
            // состоящий из одной операции nop
            currBBVertex.Data.Add(new LinkedListNode<Addr3Command>(
                new Addr3Command(1)));

            // текущая команда (если список команд пуст, сразу будет null)
            LinkedListNode<Addr3Command> currCmd = commands.First;
            // флаг: команда является лидером
            bool isLider;
            // чтобы первая реальная команда вошла была лидером
            bool prevHasGoto = true;
            while (currCmd != null)
            {
                isLider = currCmd.Value.HasLabel() || prevHasGoto;
                if (isLider)
                {
                    blocks.AddLast(currBBVertex);
                    currBBVertex = new GraphBBVertex(
                        new BasicBlock(BasicBlocksTools.NextBasicBlockName()));
                }
                // если на текущую команду есть метка, нужно добавить соответствующий
                // ББ в словарь
                if (currCmd.Value.HasLabel())
                    blocksByLabels.Add(currCmd.Value.cmdLabel, currBBVertex);
                // сохраняем информацию о goto для последующей обработки
                prevHasGoto = currCmd.Value.HasGoto();

                // добавляем команду в текущий ББ
                commands.RemoveFirst();
                currBBVertex.Data.Add(currCmd);
                // сдвигаем текущую команду
                currCmd = commands.First;
            }
            // добавляем в список последний обработанный блок
            blocks.AddLast(currBBVertex);

            // у нас есть список вершин, но нет дуг межжду вершинами
            // нужно настроить переходы между вершинами-базовыми блоками
            LinkedListNode<GraphBBVertex> curr = blocks.First;
            // если программа пуста, других блоков кроме фиктивного не будет
            // реальные ББ начинаются со второго блока
            curr = curr.Next;
            while (curr != null)
            {
                // если предыдущий блок не заканчивается чистым goto, значит
                // от предыдущего к текущему есть дуга
                if (!curr.Previous.Value.Data.LastCmdIsGoto())
                    BasicBlocksTools.ConnectVertices(curr.Previous.Value, curr.Value);
                // если последняя команда текущего блока содержит переход Goto,
                // нужно связать дугой текущий блок и тот, куда совершается переход
                if (curr.Value.Data.LastCmdHasGoto())
                {
                    string destLabel = curr.Value.Data.GetGotoLabel();
                    if (!blocksByLabels.ContainsKey(destLabel))
                        throw new InvalidOperationException("Ошибка поиска целевого ББ goto");
                    GraphBBVertex destBBVertex = blocksByLabels[destLabel];
                    BasicBlocksTools.ConnectVertices(curr.Value, destBBVertex);
                }
                curr = curr.Next;
            }

            // граф программы
            GraphBB graph = new GraphBB(blocks, blocks.First.Value);
            return graph;
        }

        /// <summary>
        /// Связывает вершины source и dest Графа ББ дугой (source, dest)
        /// </summary>
        /// <param name="source">Вершина-начало дуги</param>
        /// <param name="dest">Вершина-конец дуги</param>
        public static void ConnectVertices(GraphBBVertex from, GraphBBVertex to)
        {
            from.AddOutVertex(to);
            to.AddInVertex(from);
        }
    }
}
