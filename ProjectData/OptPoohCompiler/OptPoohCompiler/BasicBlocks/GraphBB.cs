﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;
using OptPoohCompiler.GraphTools;

namespace OptPoohCompiler.BasicBlocks
{
    /// <summary>
    /// Граф базовых блоков
    /// </summary>
    public class GraphBB : Graph<GraphBBVertex, BasicBlock>
    {

        /// <summary>
        /// Граф базовых блоков
        /// </summary>
        public GraphBB(ICollection<GraphBBVertex> vertexCollection, GraphBBVertex inputVertex)
            : base(vertexCollection, inputVertex)
        {}

    }


}
