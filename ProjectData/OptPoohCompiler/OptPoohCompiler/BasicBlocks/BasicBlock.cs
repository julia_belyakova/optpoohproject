﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.Addr3Code;

namespace OptPoohCompiler.BasicBlocks
{
    /// <summary>
    /// Базовый блок
    /// </summary>
    public class BasicBlock
    {
        /// Имя базового блока
        private string name;
        /// Трехадресные команды базового блока
        private LinkedList<Addr3Command> body = new LinkedList<Addr3Command>();

        /// Имя базового блока
        public string Name 
        {
            get { return this.name; }
        }
        /// Трехадресные команды базового блока
        public LinkedList<Addr3Command> Body
        {
            get { return this.body; }
        }

        /// <summary>
        /// Базовый блок
        /// </summary>
        /// <param name="name">Имя базового блока</param>
        public BasicBlock(string name) 
        { 
            if (name == "")
                throw new ArgumentNullException("Имя ББ не может быть пустым");
            this.name = name;
        }

        /// <summary>
        /// Добавляет трехадресную команду в базовый блок
        /// </summary>
        /// <param name="command">Узел двусвязного списка с трехадресной командой</param>
        public void Add(LinkedListNode<Addr3Command> command)
        {
            if (command == null)
                throw new ArgumentNullException("Команда не может быть null");
            if (command.Value.cmdType == Addr3CmdType.UNKNOWN)
                throw new ArgumentNullException("Недопустимая трехадресная команда");
            this.body.AddLast(command);
        }

        /// <summary>
        /// Проверяет пустоту базового блока
        /// </summary>
        /// <returns>Истину, если базовый блок пуст, и ложь в противном случае</returns>
        public bool IsEmpty()
        {
            return this.body.Count == 0;
        }

        /// <summary>
        /// Проверяет, является ли последняя команда ББ в точности goto.
        /// </summary>
        /// <returns>Истину, если последняя команда блока является goto</returns>
        public bool LastCmdIsGoto()
        {
            if (this.IsEmpty())
                throw new InvalidOperationException("Базовый блок пуст");
            return this.body.Last.Value.cmdType == Addr3CmdType.GOTO;
        }

        /// <summary>
        /// Проверяет, содержит ли последняя команда ББ переход goto.
        /// (Если в ББ есть goto, он может идти только последней командой)
        /// </summary>
        /// <returns>Истину, если последняя команда блока содержит переход goto</returns>
        public bool LastCmdHasGoto()
        {
            if (this.IsEmpty())
                throw new InvalidOperationException("Базовый блок пуст");
            return this.body.Last.Value.HasGoto();
        }

        /// <summary>
        /// Возвращает метку, на которую осуществляется переход goto
        /// в последней команде блока.
        /// Проверка, является ли последняя команда действительно переходом
        /// goto, не выполняется (используйте для этого функцию LastCmdHasGoto)
        /// </summary>
        /// <returns></returns>
        public string GetGotoLabel()
        {
            if (this.IsEmpty())
                throw new InvalidOperationException("Базовый блок пуст");
            return this.body.Last.Value.result;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("[" + this.name + "] {");
            sb.Append(Addr3CodeGenTools.GetAddr3CodeText(this.body));
            sb.Append("}");
            return sb.ToString();
        }
    }
}
