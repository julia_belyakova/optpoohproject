﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.DataFlowAnalysis;
using OptPoohCompiler.GraphTools;

namespace OptPoohCompiler.BasicBlocks
{
    /// <summary>
    /// Вершина графа базовых блоков
    /// </summary>
    public class GraphBBVertex : IVertex<GraphBBVertex, BasicBlock>
    {
        /// <summary>
        /// Базовый блок
        /// </summary>
        private BasicBlock data;
        /// <summary>
        /// Блоки-потомки (исходящие дуги)
        /// </summary>
        private HashSet<GraphBBVertex> outVerticies = new HashSet<GraphBBVertex>();
        /// <summary>
        /// Блоки-предшественники (входящие дуги)
        /// </summary>
        private HashSet<GraphBBVertex> inVerticies = new HashSet<GraphBBVertex>();

        /// <summary>
        /// Базовый блок
        /// </summary>
        public BasicBlock Data
        {
            get { return this.data; }
        }

        /// <summary>
        /// Список выходных дуг
        /// (Не использовать для изменения списка!)
        /// </summary>
        public HashSet<GraphBBVertex> OutVerticies
        {
            get { return this.outVerticies; }
        }
        /// <summary>
        /// Список входных дуг
        /// (Не использовать для изменения списка!)
        /// </summary>
        public HashSet<GraphBBVertex> InVerticies
        {
            get { return this.inVerticies; }
        }

        /// <summary>
        /// Входные данные анализа
        /// </summary>
        public object IN { get; set; }
        /// <summary>
        /// Выходные данные анализа
        /// </summary>
        public object OUT { get; set; }

        // TODO: проход по спискам входящих/исходящих дуг

        // TODO: возможно, в вершине нужно завести поле для 
        // номера некоторой нумерации

        /// <summary>
        /// Вершина графа ББл
        /// </summary>
        /// <param name="data">Базовый блок вершины</param>
        public GraphBBVertex(BasicBlock block)
        {
            if (block == null)
                throw new ArgumentNullException("Базовый блок не может быть null");
            this.data = block;
        }

        /// <summary>
        /// Добавляет входящую дугу (ссылку на вершину-предшественника)
        /// </summary>
        /// <param name="vertex">Вершина-предшестенник</param>
        public void AddInVertex(GraphBBVertex vertex)
        {
            if (vertex == null)
                throw new ArgumentNullException("Вершина не может быть null");
            this.inVerticies.Add(vertex);
        }

        /// <summary>
        /// Добавляет исходящую дугу (ссылку на вершину-потомка).
        /// Исходящих дуг не может быть больше двух.
        /// </summary>
        /// <param name="vertex">Вершина-потомок</param>
        public void AddOutVertex(GraphBBVertex vertex)
        {
            if (vertex == null)
                throw new ArgumentNullException("Вершина не может быть null");
            if (outVerticies.Count == 2)
                throw new InvalidOperationException("Базовый блок не может иметь больше двух исходящих дуг");
            this.outVerticies.Add(vertex);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.data.ToString());
            if (this.outVerticies.Count > 0)
            {
                sb.AppendLine();
                sb.Append("Arcs [" + this.data.Name + "]: ");
                var currDest = this.outVerticies.GetEnumerator();
                for (int i = 1; i < this.outVerticies.Count; ++i )
                {
                    currDest.MoveNext();
                    sb.Append(currDest.Current.Data.Name + ", ");
                }
                currDest.MoveNext();
                sb.Append(currDest.Current.Data.Name);

            }
            sb.AppendLine();
            sb.Append("IN = {" 
                + ((IN != null) ? IN.ToString() : "<empty>")
                + "}");
            sb.AppendLine();
            sb.Append("OUT = {"
                + ((OUT != null) ? OUT.ToString() : "<empty>")
                + "}");
               
            return sb.ToString();
        }

        /// <summary>
        /// Короткая строка-описатель вершины
        /// </summary>
        /// <returns>Короткую-строку описатель</returns>
        public string ShortString()
        {
            return this.Data.Name;
        }
    }
}
