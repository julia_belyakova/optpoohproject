﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using OptPoohCompiler.ParserScanner;
using OptPoohCompiler.ProgramTree;
using OptPoohCompiler.Addr3Code;
using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.OptimizationsInBB;
using OptPoohCompiler.DataFlowAnalysis;
using OptPoohCompiler.DataFlowAnalysis.DFAApplyers;
using OptPoohCompiler.GlobalOptimizations;
using OptPoohCompiler.ControlFlowGraphAnalysis;
using OptPoohCompiler.AreaBasedAnalysis;

namespace OptPoohCompiler
{
    public partial class MainForm : Form
    {
        private const string INFO_DELIM =
            "-----------------------------------------";

        // доступные парсеры языков
        private static SimpleParserManager simpleParserManager = new SimpleParserManager();
        private static PascalParserManager pascalParserManager = new PascalParserManager();
        private static CParserManager cParserManager = new CParserManager();
        private BasicParserManager basicParserManager = new BasicParserManager();

        /// Список всех доступных локальных оптимизаций
        private static IBBOptimization[] Optimizations = BBOptimizationTools.AllBBOptimizations;
        /// Список всех доступных алгоритмов анализа потоков данных
        private static IDFAApplyer[] DataFlowAnalysisAlgos = DataFlowAnalysisTools.AllDataFlowAnalysis;
        /// Список всех доступных алгоритмов анализа потоков данных на основе анализа областей
        private static IAreaBasedAlgoApplyer[] AreaBasedAnalysisAlgos = AreasTools.AllDataFlowAnalysis;

        /// Выбранный менеджер парсера
        private ParserManager currParserManager = null;

        /// Корень синтаксичсекого дерева
        private ProgrNode syntaxTreeRoot = null;
        /// Список трехадресных команд
        private LinkedList<Addr3Command> addr3Code = null;
        /// Граф базовых блоков
        private GraphBB bbGraph = null;

        private string srcFileName = "../../CodeFiles/Simple/a.txt";

        private string workingFolder = "";

        public MainForm()
        {
            InitializeComponent();
        }

        private void _InitializeOptimizations()
        {
            foreach (IBBOptimization opt in Optimizations)
                listBoxOptimizations.Items.Add(opt);
            foreach (IDFAApplyer opt in DataFlowAnalysisAlgos)
                listBoxDataFlowAnalysis.Items.Add(opt);
            foreach (IAreaBasedAlgoApplyer opt in AreaBasedAnalysisAlgos)
                listBoxDataFlowAreaAnalysis.Items.Add(opt);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // настроить языки
            textBoxSrcFileName.Text = srcFileName;
            comboBoxParser.Items.Add("Simple");
            comboBoxParser.Items.Add("OptPoohPascal");
            comboBoxParser.Items.Add("OptPoohC");
            comboBoxParser.Items.Add("OptPoohBasic");
            // настроить список оптимизаций
            _InitializeOptimizations();
            // по умолчанию тестовый язык
            comboBoxParser.SelectedIndex = 0;
            // настроить доступность кнопок
            _DisableAllStageButtons();

            workingFolder = Environment.CurrentDirectory;
        }

        private void _DisableAllStageButtons()
        {
            buttonCompile.Enabled = false;
            buttonBuildTree.Enabled = false;
            buttonGenerate3AC.Enabled = false;
            buttonDivideToBB.Enabled = false;
            buttonApplyInBBOpts.Enabled = false;
            buttonApplyDFAAlgo.Enabled = false;
            buttonDFST.Enabled = false;
            buttonAnalyseArcs.Enabled = false;
            buttonApplyDFAAreaAlgo.Enabled = false;
            buttonCheckGraphReducible.Enabled = false;
            listBoxOptimizations.Enabled = false;
            listBoxDataFlowAnalysis.Enabled = false;
            listBoxDataFlowAreaAnalysis.Enabled = false;
        }
        private void _EnableFirstStageButtons()
        {
            buttonCompile.Enabled = true;
            buttonBuildTree.Enabled = true;
        }
        private void _DisableNextStageButtons()
        {
            buttonGenerate3AC.Enabled = false;
            buttonDivideToBB.Enabled = false;
            buttonGenerate3AC.Enabled = false;
            buttonDivideToBB.Enabled = false;
            buttonApplyInBBOpts.Enabled = false;
            buttonApplyDFAAlgo.Enabled = false;
            buttonDFST.Enabled = false;
            buttonAnalyseArcs.Enabled = false;
            buttonApplyDFAAreaAlgo.Enabled = false;
            buttonCheckGraphReducible.Enabled = false;
            listBoxOptimizations.Enabled = false;
            listBoxDataFlowAnalysis.Enabled = false;
            listBoxDataFlowAreaAnalysis.Enabled = false;
        }

        private void buttonSrcFile_Click(object sender, EventArgs e)
        {
            if (openFileDialogSrc.FileName == "")
                openFileDialogSrc.FileName = workingFolder;

            if (openFileDialogSrc.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                _DisableAllStageButtons();
                return;
            }
            srcFileName = openFileDialogSrc.FileName;
            textBoxSrcFileName.Text = srcFileName;
            buttonClear_Click(this, null);
            if (!_PrintSourceCode())
                return;
            _EnableFirstStageButtons();
            _DisableNextStageButtons();
        }

        /// <summary>
        /// Вывод исходного кода
        /// </summary>
        /// <returns>Истину, если файл считан</returns>
        private bool _PrintSourceCode()
        {
            if (checkBoxClearForNew.Checked)
                buttonClear_Click(this, null);
            try
            {
                string sourceCode = File.ReadAllText(srcFileName);
                textBoxCompilationResult.AppendText(INFO_DELIM + Environment.NewLine
                    + "ИСХОДНЫЙ КОД:" + Environment.NewLine + Environment.NewLine);
                textBoxCompilationResult.AppendText(sourceCode);
                textBoxCompilationResult.AppendText(Environment.NewLine);
                return true;
            }
            catch (Exception ex)
            {
                textBoxCompilationResult.AppendText("Ошибка: "
                    + ex.Message + Environment.NewLine);
                return false;
            }
        }

        /// <summary>
        /// Выполняет все доступные стадии компиляции
        /// </summary>
        private void buttonCompile_Click(object sender, EventArgs e)
        {
            if (!buttonBuildTree.Enabled)
                return;
            buttonBuildTree_Click(this, null);
            if (!buttonGenerate3AC.Enabled)
                return;
            buttonGenerate3AC_Click(this, null);
            if (!buttonDivideToBB.Enabled)
                return;
            buttonDivideToBB_Click(this, null);
        }

        /// <summary>
        /// Выбор парсера
        /// </summary>
        private void comboBoxParser_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxParser.SelectedIndex)
            {
                case 0: 
                    currParserManager = simpleParserManager;
                    break;
                case 1:
                    currParserManager = pascalParserManager;
                    break;
                case 2:
                    currParserManager = cParserManager;
                    break;
                case 3:
                    currParserManager = basicParserManager;
                    break;
                default:
                    currParserManager = simpleParserManager;
                    break;
            }
        }

        /// <summary>
        /// Очистка поля вывода
        /// </summary>
        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxCompilationResult.Clear();
        }

        /// <summary>
        /// Построение синтаксического дерева
        /// </summary>
        private void buttonBuildTree_Click(object sender, EventArgs e)
        {
            _DisableNextStageButtons();
            _PrintSourceCode();
            textBoxCompilationResult.AppendText(Environment.NewLine + INFO_DELIM + Environment.NewLine
                + "АНАЛИЗ СИНТАКСИСА:" + Environment.NewLine);
            try
            {
                currParserManager.ReadSourceCode(srcFileName);
                var b = currParserManager.Parse();
                if (!b)
                    textBoxCompilationResult.AppendText("Ошибка синтаксиса" + Environment.NewLine);
                else
                {
                    syntaxTreeRoot = currParserManager.GetProgramTreeRoot();
                    textBoxCompilationResult.AppendText("Синтаксическое дерево построено" + Environment.NewLine);
                    textBoxCompilationResult.AppendText(syntaxTreeRoot.ToString());
                    textBoxCompilationResult.AppendText(Environment.NewLine);
                    buttonGenerate3AC.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                textBoxCompilationResult.AppendText(ex.Message);
            }
        }

        /// <summary>
        /// Генерация трехадресного кода
        /// </summary>
        private void buttonGenerate3AC_Click(object sender, EventArgs e)
        {
            Addr3CodeGenTools.ResetLabelGenerator();
            Addr3CodeGenTools.ResetTempVarGenerator();
            textBoxCompilationResult.AppendText(Environment.NewLine + INFO_DELIM + Environment.NewLine
                + "ТРЕХАДРЕСНЫЙ КОД:" + Environment.NewLine);
            try
            {
                addr3Code = syntaxTreeRoot.GetAddr3Code();
                addr3Code = Addr3CodeGenTools.RemoveEmptyCommands(addr3Code);
                var a = 1;
                addr3Code = Addr3CodeGenTools.MergeExtraOperators(addr3Code);

                textBoxCompilationResult.AppendText(
                    Addr3CodeGenTools.GetAddr3CodeText(addr3Code) + Environment.NewLine);

                buttonDivideToBB.Enabled = true;

            }
            catch (Exception ex)
            {
                textBoxCompilationResult.AppendText("Ошибка: "
                    + ex.Message + Environment.NewLine);
            }
        }

        /// <summary>
        /// Вывод графа базовых блоков
        /// </summary>
        private void printBBGraph(GraphBB graph)
        {
            textBoxCompilationResult.AppendText(Environment.NewLine + INFO_DELIM + Environment.NewLine
                + "ГРАФ БАЗОВЫХ БЛОКОВ:" + Environment.NewLine + Environment.NewLine);
            textBoxCompilationResult.AppendText(graph.ToString() + Environment.NewLine);
        }

        /// <summary>
        /// Разбиение трехадресного кода на базовые блоки
        /// </summary>
        private void buttonDivideToBB_Click(object sender, EventArgs e)
        {
            BasicBlocksTools.ResetBasicBlockGenerator();
            try
            {
                bbGraph = BasicBlocksTools.MakeBasicBlockGraph(addr3Code);
                printBBGraph(bbGraph);
                buttonApplyInBBOpts.Enabled = true;
                buttonApplyDFAAlgo.Enabled = true;
                buttonDFST.Enabled = true;
                buttonAnalyseArcs.Enabled = true;
                buttonApplyDFAAreaAlgo.Enabled = true;
                buttonCheckGraphReducible.Enabled = true;
                listBoxOptimizations.Enabled = true;
                listBoxDataFlowAnalysis.Enabled = true;
                listBoxDataFlowAreaAnalysis.Enabled = true;
            }
            catch (Exception ex)
            {
                textBoxCompilationResult.AppendText("Ошибка: "
                    + ex.Message + Environment.NewLine);
            }
        }


        /// <summary>
        /// Применение оптимизации внутри базового блока
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonApplyInBBOpts_Click(object sender, EventArgs e)
        {
            if (listBoxOptimizations.SelectedIndex == -1)
            {
                MessageBox.Show("Не выбрана локальная оптимизация");
                return;
            }
            try
            {
                BBOptimizationTools.ApplyBBPOtimizations(bbGraph,  Optimizations[listBoxOptimizations.SelectedIndex]);
                printBBGraph(bbGraph);
            }
            catch (Exception ex)
            {
                textBoxCompilationResult.AppendText("Ошибка: "
                    + ex.Message + Environment.NewLine);
            }
        }

        /// <summary>
        /// Применение глобальной оптимизации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonApplyDFAAlgo_Click(object sender, EventArgs e)
        {
            if (listBoxDataFlowAnalysis.SelectedIndex == -1)
            {
                MessageBox.Show("Не выбран анализ данных");
                return;
            }
            try
            {
                var optApplyer = DataFlowAnalysisAlgos[listBoxDataFlowAnalysis.SelectedIndex];
                optApplyer.Apply(bbGraph);
                printBBGraph(bbGraph);
            }
            catch (Exception ex)
            {
                textBoxCompilationResult.AppendText("Ошибка: "
                    + ex.Message + Environment.NewLine);
            }
        }

        private void listBoxOptimizations_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            buttonApplyInBBOpts_Click(null, null);
        }

        private void listBoxDataFlowAnalysis_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            buttonApplyDFAAlgo_Click(this, null);
        }

        private void buttonDFST_Click(object sender, EventArgs e)
        {
            textBoxCompilationResult.AppendText(Environment.NewLine + INFO_DELIM + Environment.NewLine
                + "ГЛУБИННОЕ ОСТОВНОЕ ДЕРЕВО CFG:" + Environment.NewLine);
            try
            {
                textBoxCompilationResult.AppendText(bbGraph.DFST.ToString() + Environment.NewLine);
            }
            catch (Exception ex)
            {
                textBoxCompilationResult.AppendText("Ошибка: "
                    + ex.Message + Environment.NewLine);
            }
        }

        private void buttonAnalyseArcs_Click(object sender, EventArgs e)
        {
            textBoxCompilationResult.AppendText(Environment.NewLine + INFO_DELIM + Environment.NewLine
                + "АНАЛИЗ ДУГ CFG:" + Environment.NewLine);
            try
            {
                DFSTAndArcsCFGData<BasicBlock, GraphBBVertex, GraphBB> arcsData =
                    new DFSTAndArcsCFGData<BasicBlock, GraphBBVertex, GraphBB>(this.bbGraph);
                textBoxCompilationResult.AppendText(arcsData.ToString() + Environment.NewLine);
            }
            catch (Exception ex)
            {
                textBoxCompilationResult.AppendText("Ошибка: "
                    + ex.Message + Environment.NewLine);
            }
        }

        private void listBoxDataFlowAreaAnalysis_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonApplyDFAAreaAlgo_Click(this, null);
        }

        private void buttonApplyDFAAreaAlgo_Click(object sender, EventArgs e)
        {
            if (listBoxDataFlowAreaAnalysis.SelectedIndex == -1)
            {
                MessageBox.Show("Не выбран анализ данных на основе областей");
                return;
            }
            try
            {
                var optApplyer = AreaBasedAnalysisAlgos[listBoxDataFlowAreaAnalysis.SelectedIndex];
                optApplyer.Apply(bbGraph);
                printBBGraph(bbGraph);
            }
            catch (Exception ex)
            {
                textBoxCompilationResult.AppendText("Ошибка: "
                    + ex.Message + Environment.NewLine);
            }
        }

        private void buttonCheckGraphReducible_Click(object sender, EventArgs e)
        {
            textBoxCompilationResult.AppendText(Environment.NewLine + INFO_DELIM + Environment.NewLine
                + "Проврка приводимости CFG:" + Environment.NewLine);
            try
            {
                bool isReducible = CFGAnalysisTools.IsReducible<BasicBlock, GraphBBVertex, GraphBB>(bbGraph);
                textBoxCompilationResult.AppendText((isReducible ? "Приводим" : "Не приводим")
                    + Environment.NewLine);
            }
            catch (Exception ex)
            {
                textBoxCompilationResult.AppendText("Ошибка: "
                    + ex.Message + Environment.NewLine);
            }
        }


    }
}
