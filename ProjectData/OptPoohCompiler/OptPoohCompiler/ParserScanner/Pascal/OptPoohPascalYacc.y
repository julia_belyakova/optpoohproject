%{
    // ��� ���������� ����������� � ����� GPPGParser, �������������� ����� ������, ������������ �������� gppg
    public ProgrNode root; // �������� ���� ��������������� ������ 
    public Parser(AbstractScanner<ValueType, LexLocation> scanner) : base(scanner) { }
%}

%output = OptPoohPascalYacc.cs

%union { 
			public string sVal;                             // ��������� ���
            public int iVal;                                // �����
            
			public Node nVal;
			public BlockNode blVal;                         // ���� ����������
			public ProgrNode prVal;                         // ��� ���������
 
            public StatementNode stVal;                     // ��������
            
            public BinLogicExprNode logExprVal;             // ���������� ���������
            public ArithmExprNode arExprVal;                // �������������� ���������
            public Relation relateOp;				        // �������� ���������
            public ArithmPlusOperator plusOp;			    // �������� ��������
            public ArithmMultOperator multOp;				// �������� ���������
            public UnaryArithmOperator unOp;				// ������� ��������
       }

%using System.IO;
%using OptPoohCompiler.ProgramTree;

%namespace OptPoohCompiler.PascalParser

%start progr                                                // ��������� ������ � ���������, ���� � �����

%token <sVal> ID
%token <iVal> INTNUM
%token BEGIN END 
%token ASSIGN SEMICOLON LPAREN RPAREN
%token <op> PLUS MINUS MULT DIVIDE LT GT LE GE EQ NE
%token IF THEN ELSE 
%token FOR TO DOWNTO DO WHILE

%type <blVal> block stList
%type <stVal> stmnt
%type <plusOp> plusOpr					// �������� �������� 
%type <multOp> multOpr					// ������� ��������
%type <arExprVal> arExpr term sFactor factor 
%type <arExprVal> ident
%type <relateOp> relOpr				    // �������� ���������
%type <logExprVal> logExpr
%type <stVal> assign for if maybeElse while

// ��������� ��������
%left LT GT LE GE EQ NE
%left PLUS MINUS
%left MULT DIVIDE
%left UMINUS UPLUS

%%

/* ****************************************** �������� ����� ****************************************** */

progr   : block  { 
            root = new ProgrNode($1); 
        }
		;
       
block   : BEGIN stList END {
            $$ = $2;
        }
        ;

stList	: { 
			$$ = new BlockNode(); 
		}
		| stList stmnt SEMICOLON { 
				$1.Add($2); 
				$$ = $1; 
        }
		;
        
stmnt	: assign { $$ = $1; }
        | block  { $$ = $1; } 
        | if { $$ = $1; } 
        | for { $$ = $1; } 
        | while { $$ = $1; } 
        ;
    
/* ******************************************** ��������� ********************************************* */

/*
L ::= E relationOp E
E ::= E plusOp T
      | T
T ::= T multOp S
      | S
S ::= unaryOp S
      | F
F ::= id
      | num
      | (E)
*/

logExpr : arExpr relOpr arExpr {
            $$ = new BinLogicExprNode($2, $1, $3);
        }
        ;
          
relOpr  : LT {
            $$ = Relation.LESS;
        }
        | GT {
            $$ = Relation.GREATER;
        }
        | LE {
            $$ = Relation.LESS_EQUAL;
        }
        | GE {
            $$ = Relation.GREATER_EQUAL;
        }
        | EQ {
            $$ = Relation.EQUAL;
        }
        | NE {
            $$ = Relation.NOT_EQUAL;
        }
        ;

arExpr  : term {
            $$ = $1;
        }
        | arExpr plusOpr term {
            $$ = new BinArithmPlusExprNode($2, $1, $3);
        }
        ;        

plusOpr						
	: PLUS {
		$$ = ArithmPlusOperator.PLUS;
	}
	| MINUS {
		$$ = ArithmPlusOperator.MINUS;
	}
	;        
        
term    : sFactor {
            $$ = $1;
        }        
        | term multOpr sFactor {
            $$ = new BinArithmMultExprNode($2, $1, $3);
        }
        ;
        
multOpr						
	: MULT {
		$$ = ArithmMultOperator.MULT;
	}
	| DIVIDE {
		$$ = ArithmMultOperator.DIVIDE;
	}
	;

sFactor							
	: factor {
		$$ = $1;
	}
	| PLUS sFactor %prec UPLUS {
		$$ = new SignedArithmExprNode(
            UnaryArithmOperator.UPLUS, $2);
	}
	| MINUS sFactor %prec UMINUS {
		$$ = new SignedArithmExprNode(
            UnaryArithmOperator.UMINUS, $2);
	}
	;    
        
factor	: ident  { 
            $$ = $1 as IdNode; 
        }
		| INTNUM { 
            $$ = new NumNode($1); 
        }
        | LPAREN arExpr RPAREN {
            $$ = $2;
        }
		;
        
ident   : ID { 
            $$ = new IdNode($1); 
        }	
        ;
       
/* ******************************************** ��������� ********************************************* */

assign 	: ident ASSIGN arExpr { 
            $$ = new AssignNode($1 as IdNode, $3); 
        }
		;
      
for : 	FOR ident ASSIGN arExpr TO arExpr DO stmnt{
			 $$ = new ForNode($4, $6, $2 as IdNode, $8, ForNode.ForCicleType.To);  
        }
		| FOR ident ASSIGN arExpr DOWNTO arExpr DO stmnt{
			 $$ = new ForNode($4, $6, $2 as IdNode, $8, ForNode.ForCicleType.DownTo);  
        }
		;
		
		
if  :   IF logExpr THEN stmnt maybeElse{
		  $$ = new IfNode($2, $4, $5); 
		}
		;
		
maybeElse : { 
                $$ = null; 
            }
			| ELSE stmnt {
				$$ = $2;
			}
			;	
	
while   : WHILE logExpr DO stmnt{
		    $$ = new WhileNode($2, $4); 
		}
		;
		
%%

