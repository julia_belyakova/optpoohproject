%namespace OptPoohCompiler.PascalScanner

%using OptPoohCompiler.PascalParser;
%using QUT.Gppg;

/* ********************************* ���������� ��������� ******************************* */

Digit 		[0-9]                               // �����
Alpha 		[a-zA-Z_]                           // ����� (��������� ��� ��������������)
AlphaDigit 	{Alpha}|{Digit}                     // ���� ��� �����
EndLine 	\n									// ����� ������
INTNUM 		{Digit}+                            // ����� �����
ID 			{Alpha}{AlphaDigit}*                // �������������

%x COMMENT_S								// ��������� ��� ������������ ������������

%%

/* *********************************** ��������� ������� ******************************** */

// ��������� �������-�����������
":=" { return (int)Tokens.ASSIGN; }
";"  { return (int)Tokens.SEMICOLON; }
"+"  { return (int)Tokens.PLUS; }
"-"  { return (int)Tokens.MINUS; }
"*"  { return (int)Tokens.MULT; }
"/"  { return (int)Tokens.DIVIDE; }
"<"  { return (int)Tokens.LT; }
">"  { return (int)Tokens.GT; }
"<=" { return (int)Tokens.LE; }
">=" { return (int)Tokens.GE; }
"="  { return (int)Tokens.EQ; }
"<>" { return (int)Tokens.NE; }
"("  { return (int)Tokens.LPAREN; }
")"  { return (int)Tokens.RPAREN; }

"//" { 										// ������������ �����������
	BEGIN(COMMENT_S);	
}					
<COMMENT_S> {EndLine} { 
	BEGIN(INITIAL);
}

// ������� �����
{ID}  { 
  int res = ScannerHelper.GetIDToken(yytext);
  if (res == (int)Tokens.ID)
	yylval.sVal = yytext;
  return res;
}

{INTNUM} { 
  yylval.iVal = int.Parse(yytext); 
  return (int)Tokens.INTNUM; 
}

%{
  yylloc = new LexLocation(tokLin, tokCol, tokELin, tokECol);
%}

%%

/* ********************************** �������������� ��� ******************************** */

/// ��������������� ����� ��� �������
class ScannerHelper 
{
    /*
    /// ���������� ����������� ������
    public override void yyerror(string format, params object[] args) 
    {
        string errorMsg = PT.CreateErrorString(args);
        //PT.AddSyntaxError(errorMsg, yylloc);
    }
    */
  
    /// �������� �����
    private static Dictionary<string,int> keywords;

    static ScannerHelper() 
    {
        keywords = new Dictionary<string,int>();
        keywords.Add("begin",(int)Tokens.BEGIN);
        keywords.Add("end",(int)Tokens.END);
        keywords.Add("if",(int)Tokens.IF);
        keywords.Add("then",(int)Tokens.THEN);
        keywords.Add("else",(int)Tokens.ELSE);
        keywords.Add("for",(int)Tokens.FOR);
        keywords.Add("to",(int)Tokens.TO);
        keywords.Add("downto",(int)Tokens.DOWNTO);
        keywords.Add("do",(int)Tokens.DO);
        keywords.Add("while",(int)Tokens.WHILE);
    }
  
    /// <summary>
    /// ���������� ��������� �����, ��������������� ������, 
    /// ��� ����� ��������������
    /// </summary>
    public static int GetIDToken(string s)
    {
        if (keywords.ContainsKey(s.ToLower()))
            return keywords[s];
        else
            return (int)Tokens.ID;
    }
  
}
