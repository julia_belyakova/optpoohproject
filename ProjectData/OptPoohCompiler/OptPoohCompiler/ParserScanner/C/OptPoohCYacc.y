%{
    // ��� ���������� ����������� � ����� GPPGParser, �������������� ����� ������, ������������ �������� gppg
    public ProgrNode root; // �������� ���� ��������������� ������ 
    public Parser(AbstractScanner<ValueType, LexLocation> scanner) : base(scanner) { }
%}

%output = OptPoohCYacc.cs

%union { 
			public string sVal;                             // ��������� ���
            public int iVal;                                // �����
            
			public Node nVal;
			public BlockNode blVal;                         // ���� ����������
			public ProgrNode prVal;                         // ��� ���������
 
            public StatementNode stVal;                     // ��������
            
            public BinLogicExprNode logExprVal;             // ���������� ���������
            public ArithmExprNode arExprVal;                // �������������� ���������
            public Relation relateOp;				        // �������� ���������
            public ArithmPlusOperator plusOp;			    // �������� ��������
            public ArithmMultOperator multOp;				// �������� ���������
            public UnaryArithmOperator unOp;				// ������� ��������
       }

%using System.IO;
%using OptPoohCompiler.ProgramTree;

%namespace OptPoohCompiler.CParser

%start progr                                                // ��������� ������ � ���������, ���� � �����

%token <sVal> ID
%token <iVal> INTNUM
%token ASSIGN SEMICOLON LPAREN RPAREN LFPAREN RFPAREN
%token <op> PLUS MINUS MULT DIVIDE LT GT LE GE EQ NE
%token IF THEN ELSE PLUSPLUS MINUSMINUS
%token FOR WHILE

%type <blVal> block stList
%type <stVal> stmnt
%type <plusOp> plusOpr					// �������� �������� 
%type <multOp> multOpr					// ������� ��������
%type <arExprVal> arExpr term sFactor factor 
%type <arExprVal> ident
%type <relateOp> relOpr				    // �������� ���������
%type <logExprVal> logExpr
%type <stVal> assign for if maybeElse while

// ��������� ��������
%left LT GT LE GE EQ NE
%left PLUS MINUS
%left MULT DIVIDE
%left UMINUS UPLUS

%%

/* ****************************************** �������� ����� ****************************************** */

progr   : block  { 
            root = new ProgrNode($1); 
        }
		;
       
block   : LFPAREN stList RFPAREN {
            $$ = $2;
        }
        ;

stList	: { 
			$$ = new BlockNode(); 
		}
		| stList stmnt { 
				$1.Add($2); 
				$$ = $1; 
        }
		;
        
stmnt	: assign SEMICOLON{ $$ = $1; }
        | block  { $$ = $1; } 
        | if { $$ = $1; } 
        | for { $$ = $1; } 
        | while { $$ = $1; } 
        ;
    
/* ******************************************** ��������� ********************************************* */

/*
L ::= E relationOp E
E ::= E plusOp T
      | T
T ::= T multOp S
      | S
S ::= unaryOp S
      | F
F ::= id
      | num
      | (E)
*/

logExpr : arExpr relOpr arExpr {
            $$ = new BinLogicExprNode($2, $1, $3);
        }
        ;
          
relOpr  : LT {
            $$ = Relation.LESS;
        }
        | GT {
            $$ = Relation.GREATER;
        }
        | LE {
            $$ = Relation.LESS_EQUAL;
        }
        | GE {
            $$ = Relation.GREATER_EQUAL;
        }
        | EQ {
            $$ = Relation.EQUAL;
        }
        | NE {
            $$ = Relation.NOT_EQUAL;
        }
        ;

arExpr  : term {
            $$ = $1;
        }
        | arExpr plusOpr term {
            $$ = new BinArithmPlusExprNode($2, $1, $3);
        }
        ;        

plusOpr						
	: PLUS {
		$$ = ArithmPlusOperator.PLUS;
	}
	| MINUS {
		$$ = ArithmPlusOperator.MINUS;
	}
	;        
        
term    : sFactor {
            $$ = $1;
        }        
        | term multOpr sFactor {
            $$ = new BinArithmMultExprNode($2, $1, $3);
        }
        ;
        
multOpr						
	: MULT {
		$$ = ArithmMultOperator.MULT;
	}
	| DIVIDE {
		$$ = ArithmMultOperator.DIVIDE;
	}
	;

sFactor							
	: factor {
		$$ = $1;
	}
	| PLUS sFactor %prec UPLUS {
		$$ = new SignedArithmExprNode(
            UnaryArithmOperator.UPLUS, $2);
	}
	| MINUS sFactor %prec UMINUS {
		$$ = new SignedArithmExprNode(
            UnaryArithmOperator.UMINUS, $2);
	}
	;    
        
factor	: ident  { 
            $$ = $1 as IdNode; 
        }
		| INTNUM { 
            $$ = new NumNode($1); 
        }
        | LPAREN arExpr RPAREN {
            $$ = $2;
        }
		;
        
ident   : ID { 
            $$ = new IdNode($1); 
        }	
        ;
       
/* ******************************************** ��������� ********************************************* */

assign 	: ident ASSIGN arExpr { 
            $$ = new AssignNode($1 as IdNode, $3); 
        }
		;
      
for     : FOR LPAREN ident ASSIGN arExpr SEMICOLON arExpr SEMICOLON PLUSPLUS RPAREN stmnt{
			$$ = new ForNode($5, $7, $3 as IdNode, $11, ForNode.ForCicleType.To);
        }
		| FOR LPAREN ident ASSIGN arExpr SEMICOLON arExpr SEMICOLON MINUSMINUS RPAREN stmnt{
			$$ = new ForNode($5, $7, $3 as IdNode, $11, ForNode.ForCicleType.DownTo);
        }
		;
		
		
if      : IF LPAREN logExpr RPAREN stmnt maybeElse{
            $$ = new IfNode($3, $5, $6); 
		}
		;
		
maybeElse : { $$ = null;}
			| ELSE stmnt {
                $$ = $2;
			}
			;	
	
	
	
while:  WHILE LPAREN logExpr RPAREN stmnt {
			$$ = new WhileNode($3, $5); 
		}
		;
%%


