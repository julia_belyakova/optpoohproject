%namespace OptPoohCompiler.CScanner

%using OptPoohCompiler.CParser;
%using QUT.Gppg;

/* ********************************* ���������� ��������� ******************************* */

Alpha 	[a-zA-Z_]
INTNUM  [0-9]+
ID [a-zA-Z_][a-zA-Z0-9_]* 

%%

/* *********************************** ��������� ������� ******************************** */

// ��������� �������-�����������
"=" { return (int)Tokens.ASSIGN; }
";"  { return (int)Tokens.SEMICOLON; }
"+"  { return (int)Tokens.PLUS; }
"-"  { return (int)Tokens.MINUS; }
"*"  { return (int)Tokens.MULT; }
"/"  { return (int)Tokens.DIVIDE; }
"<"  { return (int)Tokens.LT; }
">"  { return (int)Tokens.GT; }
"<=" { return (int)Tokens.LE; }
">=" { return (int)Tokens.GE; }
"=="  { return (int)Tokens.EQ; }
"!=" { return (int)Tokens.NE; }
"("  { return (int)Tokens.LPAREN; }
")"  { return (int)Tokens.RPAREN; }
"{"  { return (int)Tokens.LFPAREN; }
"}"  { return (int)Tokens.RFPAREN; }
"++"  { return (int)Tokens.PLUSPLUS; }
"--"  { return (int)Tokens.MINUSMINUS; }

// ������� �����
{ID}  { 
  int res = ScannerHelper.GetIDToken(yytext);
  if (res == (int)Tokens.ID)
	yylval.sVal = yytext;
  return res;
}

{INTNUM} { 
  yylval.iVal = int.Parse(yytext); 
  return (int)Tokens.INTNUM; 
}

%{
  yylloc = new LexLocation(tokLin, tokCol, tokELin, tokECol);
%}

%%

/* ********************************** �������������� ��� ******************************** */

/// ��������������� ����� ��� �������
class ScannerHelper 
{
    /*
    /// ���������� ����������� ������
    public override void yyerror(string format, params object[] args) 
    {
        string errorMsg = PT.CreateErrorString(args);
        //PT.AddSyntaxError(errorMsg, yylloc);
    }
    */
  
    /// �������� �����
    private static Dictionary<string,int> keywords;

    static ScannerHelper() 
    {
        keywords = new Dictionary<string,int>();
        keywords.Add("if",(int)Tokens.IF);
        keywords.Add("then",(int)Tokens.THEN);
        keywords.Add("else",(int)Tokens.ELSE);
        keywords.Add("for",(int)Tokens.FOR);
        keywords.Add("while",(int)Tokens.WHILE);
    }
  
    /// <summary>
    /// ���������� ��������� �����, ��������������� ������, 
    /// ��� ����� ��������������
    /// </summary>
    public static int GetIDToken(string s)
    {
        if (keywords.ContainsKey(s))
            return keywords[s];
        else
            return (int)Tokens.ID;
    }
  
}