%namespace OptPoohCompiler.BasicScanner

%using OptPoohCompiler.BasicParser;
%using QUT.Gppg;

/* ********************************* ���������� ��������� ******************************* */

Alpha 	[a-zA-Z_]
INTNUM  [0-9]+
ID [a-zA-Z_][a-zA-Z0-9_]* 
endl \n

%x COMMENT_S								// ��������� ��� ������������ ������������
%%

/* *********************************** ��������� ������� ******************************** */

// ��������� �������-�����������
"="     { return (int)Tokens.ASSIGN_OR_EQ; }
":"     { return (int)Tokens.COLON; }
"+"     { return (int)Tokens.PLUS; }
"-"     { return (int)Tokens.MINUS; }
"*"     { return (int)Tokens.MULT; }
"/"     { return (int)Tokens.DIVIDE; }
"<"     { return (int)Tokens.LT; }
">"     { return (int)Tokens.GT; }
"<="    { return (int)Tokens.LE; }
">="    { return (int)Tokens.GE; }
"<>"    { return (int)Tokens.NE; }
//"&"     { return (int)Tokens.CONCATENATION; }
//"^"     { return (int)Tokens.POWER; }
"("     { return (int)Tokens.LPAREN; }
")"     { return (int)Tokens.RPAREN; }


"//" { BEGIN(COMMENT_S); }					
<COMMENT_S> {endl} { BEGIN(INITIAL); }

// ������� �����
{ID}  { 
  int res = ScannerHelper.GetIDToken(yytext);
  if (res == (int)Tokens.ID)
	yylval.sVal = yytext;
  return res;
}

{INTNUM} { 
  yylval.iVal = int.Parse(yytext); 
  return (int)Tokens.INTNUM; 
}

%{
  yylloc = new LexLocation(tokLin, tokCol, tokELin, tokECol);
%}

%%


/* ********************************** �������������� ��� ******************************** */

/// ��������������� ����� ��� �������
class ScannerHelper 
{
    /*
    /// ���������� ����������� ������
    public override void yyerror(string format, params object[] args) 
    {
        string errorMsg = PT.CreateErrorString(args);
        //PT.AddSyntaxError(errorMsg, yylloc);
    }
    */
  
    /// �������� �����
    private static Dictionary<string,int> keywords;

    static ScannerHelper() 
    {
        keywords = new Dictionary<string,int>();
        keywords.Add("if",(int)Tokens.IF);
        keywords.Add("then",(int)Tokens.THEN);
        keywords.Add("else",(int)Tokens.ELSE);
        keywords.Add("endif",(int)Tokens.ENDIF);
        keywords.Add("for",(int)Tokens.FOR);
        keywords.Add("to",(int)Tokens.TO);
        keywords.Add("step",(int)Tokens.STEP);
        keywords.Add("next",(int)Tokens.NEXT);
        keywords.Add("while",(int)Tokens.WHILE);
        keywords.Add("endwhile",(int)Tokens.ENDWHILE);
        keywords.Add("end",(int)Tokens.END);
    }
  
    /// <summary>
    /// ���������� ��������� �����, ��������������� ������, 
    /// ��� ����� ��������������
    /// </summary>
    public static int GetIDToken(string s)
    {
        if (keywords.ContainsKey(s))
            return keywords[s];
        else
            return (int)Tokens.ID;
    }
  
}