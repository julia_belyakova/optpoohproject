%{
    // ��� ���������� ����������� � ����� GPPGParser, �������������� ����� ������, ������������ �������� gppg
    public ProgrNode root; // �������� ���� ��������������� ������ 
    public Parser(AbstractScanner<ValueType, LexLocation> scanner) : base(scanner) { }
%}

%output = OptPoohBasicYacc.cs

%union { 
		public string sVal;                             // ��������� ���
		public int iVal;                                // �����
            
		public Node nVal;
		public BlockNode blVal;                         // ���� ����������
		public ProgrNode prVal;                         // ��� ���������
 
		public StatementNode stVal;                     // ��������
            
		public BinLogicExprNode logExprVal;             // ���������� ���������
		public ArithmExprNode arExprVal;                // �������������� ���������
		public Relation relateOp;			// �������� ���������
		public ArithmPlusOperator plusOp;		// �������� ��������
            	public ArithmMultOperator multOp;		// �������� ���������
		public UnaryArithmOperator unOp;		// ������� ��������
        }

%using System.IO;
%using OptPoohCompiler.ProgramTree;

%namespace OptPoohCompiler.BasicParser

%start progr                                                     // ��������� ������ � ���������, ���� � �����

%token <sVal> ID
%token <iVal> INTNUM
%token COLON LPAREN RPAREN
%token PLUS MINUS MULT DIVIDE LT GT LE GE ASSIGN_OR_EQ NE
%token IF THEN ELSE ENDIF END
%token FOR TO STEP NEXT WHILE ENDWHILE 

%type <blVal> stList
%type <stVal> stmnt
%type <arExprVal> arExpr monoArExpr signArExpr binArExpr plusArExpr mulArExpr
%type <arExprVal> ident 
%type <relateOp> relOpr				    // �������� ���������
%type <logExprVal> ifBracket
%type <logExprVal> whileBracket 
%type <stVal> forOperator ifOperator whileOperator

// ��������� ��������
%left LT GT LE GE EQ NE
%left PLUS MINUS
%left MULT DIVIDE
%left UMINUS UPLUS

%%

/* ****************************************** �������� ����� ****************************************** */


progr   : stList END { root = new ProgrNode($1); }
	| stList { root = new ProgrNode($1); };
       
stList	: { $$ = new BlockNode(); }
	| stList stmnt { $1.Add($2); 
			 $$ = $1; };

stmnt   : ident ASSIGN_OR_EQ arExpr { $$ = new AssignNode($1 as IdNode, $3); }
        | ifOperator { $$ = $1; } 
        | forOperator { $$ = $1; } 
        | whileOperator { $$ = $1; } 
        | stmnt COLON stmnt { $$ = new BlockNode(); ($$ as BlockNode).Add($1); ($$ as BlockNode).Add($3); };
        
//stmnt	: stm EOL { $$ = $1; };
    
/* ******************************************** ��������� ********************************************* */

ident   : ID { 
            $$ = new IdNode($1); 
        };

arExpr : monoArExpr { $$ = $1; } | signArExpr { $$ = $1; } | binArExpr { $$ = $1; };

monoArExpr : LPAREN arExpr RPAREN { $$ = $2; } | INTNUM { $$ = new NumNode($1); } | ident { $$ = $1; };

signArExpr : PLUS monoArExpr { $$ = new SignedArithmExprNode(UnaryArithmOperator.UPLUS,$2); } | MINUS monoArExpr { $$ = new SignedArithmExprNode(UnaryArithmOperator.UMINUS,$2); };

binArExpr  : plusArExpr { $$ = $1; } | mulArExpr { $$ = $1; };

plusArExpr : arExpr PLUS arExpr {$$ = new BinArithmPlusExprNode(ArithmPlusOperator.PLUS,$1,$3); } 
           | arExpr MINUS arExpr {$$ = new BinArithmPlusExprNode(ArithmPlusOperator.MINUS,$1,$3); };

mulArExpr  : arExpr MULT arExpr { $$ = new BinArithmMultExprNode(ArithmMultOperator.MULT,$1,$3); } 
           | arExpr DIVIDE arExpr { $$ = new BinArithmMultExprNode(ArithmMultOperator.DIVIDE,$1,$3); };

/* ******************************************** ��������� ********************************************* */

ifBracket : IF arExpr relOpr arExpr THEN { $$ = new BinLogicExprNode($3,$2,$4); } | IF arExpr ASSIGN_OR_EQ arExpr THEN { $$ = new BinLogicExprNode(Relation.EQUAL,$2,$4); };

whileBracket : WHILE arExpr relOpr arExpr { $$ = new BinLogicExprNode($3,$2,$4); } | WHILE arExpr ASSIGN_OR_EQ arExpr{ $$ = new BinLogicExprNode(Relation.EQUAL,$2,$4); };

forOperator : FOR ident ASSIGN_OR_EQ arExpr TO arExpr STEP PLUS stList NEXT { $$ = new ForNode($4,$6,$2 as IdNode,$9,ForNode.ForCicleType.To); }
            | FOR ident ASSIGN_OR_EQ arExpr TO arExpr STEP MINUS stList NEXT { $$ = new ForNode($4,$6,$2 as IdNode,$9,ForNode.ForCicleType.DownTo); };

ifOperator : ifBracket stList ENDIF { $$ = new IfNode($1,$2,new BlockNode()); } | ifBracket stList ELSE stList ENDIF { $$ = new IfNode($1,$2,$4); };

whileOperator : whileBracket stList  ENDWHILE { $$ = new WhileNode($1, $2); };

relOpr  : LT {
            $$ = Relation.LESS;
        }
        | GT {
            $$ = Relation.GREATER;
        }
        | LE {
            $$ = Relation.LESS_EQUAL;
        }
        | GE {
            $$ = Relation.GREATER_EQUAL;
        }
        | NE {
            $$ = Relation.NOT_EQUAL;
        }
        ;


%%