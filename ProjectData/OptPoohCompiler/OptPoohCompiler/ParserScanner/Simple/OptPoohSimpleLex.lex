%namespace OptPoohCompiler.SimpleScanner

%using OptPoohCompiler.SimpleParser;
%using QUT.Gppg;

/* ********************************* ���������� ��������� ******************************* */

Alpha 	[a-zA-Z_]
INTNUM  [0-9]+
REALNUM {INTNUM}\.{INTNUM}
ID [a-zA-Z_][a-zA-Z0-9_]* 

%%

/* *********************************** ��������� ������� ******************************** */

":=" { return (int)Tokens.ASSIGN; }
";"  { return (int)Tokens.SEMICOLON; }

{ID}  { 
  int res = ScannerHelper.GetIDToken(yytext);
  if (res == (int)Tokens.ID)
	yylval.sVal = yytext;
  return res;
}

{INTNUM} { 
  yylval.iVal = int.Parse(yytext); 
  return (int)Tokens.INTNUM; 
}

%{
  yylloc = new LexLocation(tokLin, tokCol, tokELin, tokECol);
%}

%%

/* ********************************** �������������� ��� ******************************** */

/// ��������������� ����� ��� �������
class ScannerHelper 
{
    /*
    /// ���������� ����������� ������
    public override void yyerror(string format, params object[] args) 
    {
        string errorMsg = PT.CreateErrorString(args);
        //PT.AddSyntaxError(errorMsg, yylloc);
    }
    */
  
    /// �������� �����
    private static Dictionary<string,int> keywords;

    static ScannerHelper() 
    {
        keywords = new Dictionary<string,int>();
        keywords.Add("begin",(int)Tokens.BEGIN);
        keywords.Add("end",(int)Tokens.END);
        keywords.Add("cycle",(int)Tokens.CYCLE);
    }
  
    /// <summary>
    /// ���������� ��������� �����, ��������������� ������, 
    /// ��� ����� ��������������
    /// </summary>
    public static int GetIDToken(string s)
    {
        if (keywords.ContainsKey(s.ToLower()))
            return keywords[s];
        else
            return (int)Tokens.ID;
    }
  
}
