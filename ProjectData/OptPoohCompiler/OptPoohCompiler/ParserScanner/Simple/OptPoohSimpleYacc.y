%{
    // ��� ���������� ����������� � ����� GPPGParser, �������������� ����� ������, ������������ �������� gppg
    public ProgrNode root; // �������� ���� ��������������� ������ 
    public Parser(AbstractScanner<ValueType, LexLocation> scanner) : base(scanner) { }
%}

%output = ParserScanner\Simple\OptPoohSimpleYacc.cs

%union { 
			public double dVal; 
			public int iVal; 
			public string sVal; 
            
			public Node nVal;
			public BlockNode blVal;
			public ProgrNode prVal;
            
            public ArithmExprNode arExprVal;
       }

%using System.IO;
%using OptPoohCompiler.ProgramTree;

%namespace OptPoohCompiler.SimpleParser

%start progr

%token BEGIN END CYCLE ASSIGN SEMICOLON
%token <iVal> INTNUM 
%token <sVal> ID

%type <arExprVal> expr
%type <nVal> assign st comp cycl ident
%type <blVal> stlist comp
%type <prVal> progr

%%

progr   : stlist { root = new ProgrNode($1); }
		;

stlist	: st 
			{ 
				$$ = new BlockNode($1 as StatementNode); 
			}
		| stlist SEMICOLON st 
			{ 
				$1.Add($3 as StatementNode); 
				$$ = $1; 
			}
		;

st	: assign { $$ = $1; }
	| comp  { $$ = $1; }
	| cycl  { $$ = $1; }
	;

ident : ID { $$ = new IdNode($1); }	
	;
	
assign 	: ident ASSIGN expr { 
            $$ = new AssignNode($1 as IdNode, $3 as ArithmExprNode); 
          }
		;

expr	: ident  { $$ = $1 as IdNode; }
		| INTNUM { $$ = new NumNode($1); }
		;

comp	: BEGIN stlist END { $$ = $2; }
		;

cycl	: CYCLE expr st { $$ = new CycleNode($2 as ExprNode,$3 as StatementNode); }
		;
	
%%

