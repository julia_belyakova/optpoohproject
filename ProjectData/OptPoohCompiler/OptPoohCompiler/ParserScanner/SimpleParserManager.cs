﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OptPoohCompiler.SimpleScanner;
using OptPoohCompiler.SimpleParser;

namespace OptPoohCompiler.ParserScanner
{
    /// <summary>
    /// Класс, обеспечивающий интерфейс парсера тестового языка
    /// </summary>
    class SimpleParserManager : ParserManager
    {
        private Scanner scanner = new Scanner(); 

        /// <summary>
        /// Класс, обеспечивающий интерфейс парсера тестового языка
        /// </summary>
        public SimpleParserManager(){ }

        /// <summary>
        /// Парсит исходный код программы и строит синтаксическое
        /// дерево программы, если оно существует (то есть программа
        /// синтаксически корректна для тестового языка)
        /// </summary>
        /// <returns>Истину, если дерево построено, и ложь в противном случае</returns>
        override public bool Parse() 
        {
            _CheckSrcText();

            bool success = false;
            try
            {
                scanner.SetSource(srcText, 0);
                Parser parser = new Parser(scanner);
                success = parser.Parse();
                treeRoot = parser.root;
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при разборе исходного кода:\n"
                    + ex.Message);
            }
            return success;
        }
    }
}
