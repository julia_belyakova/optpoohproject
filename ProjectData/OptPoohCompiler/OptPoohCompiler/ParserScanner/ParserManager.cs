﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using OptPoohCompiler.ProgramTree;

namespace OptPoohCompiler.ParserScanner
{
    /// <summary>
    /// Класс, обеспечивающий интерфейс для использования парсера
    /// конкретного яыка
    /// </summary>
    public abstract class ParserManager
    {
        /// Текст программы
        protected string srcText = null;
        /// Корень синтаксического дерева программы
        protected ProgrNode treeRoot = null;

        /// <summary>
        /// Считывает файл с исходным кодом
        /// </summary>
        /// <param name="srcFileName">Имя файла исходного кода</param>
        public void ReadSourceCode(string srcFileName) 
        {
            StreamReader sr = null;
            bool error = false;
            string errorText = "";
            try
            {
                sr = new StreamReader(srcFileName);
                srcText = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                error = false;
                errorText = ex.Message;
            }
            finally { sr.Close(); }
            if (error)
                throw new Exception("Проблемы с файлом исходного кода:\n"
                    + errorText);
        }

        /// <summary>
        /// Парсит исходный код программы и строит синтаксическое
        /// дерево программы, если оно существует (то есть программа
        /// синтаксически корректна)
        /// </summary>
        /// <returns>Истину, если дерево построено, и ложь в противном случае</returns>
        abstract public bool Parse();

        /// <summary>
        /// Возвращает ссылку на корневой узел дерева программы
        /// </summary>
        /// <returns>Корень синтаксического дерева</returns>
        public ProgrNode GetProgramTreeRoot() 
        {
            return treeRoot;
        }

        /// <summary>
        /// Проверяет, что задан исходный код для парсинга.
        /// Должна быть вызвана первой в Parse
        /// </summary>
        protected void _CheckSrcText() 
        {
            if (srcText == null)
                throw new Exception("Не задан исходный код программы");
        }
    }
}
