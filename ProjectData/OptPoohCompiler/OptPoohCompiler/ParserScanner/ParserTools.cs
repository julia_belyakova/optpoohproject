﻿using System.Collections.Generic;

namespace OptPoohCompiler.ParserTools
{   
    /// <summary>
    /// Класс глобальных описаний и статических методов
    /// для использования различными подсистемами парсера и сканера
    /// </summary>
    public static class PT // PT - parser tools
    {
        /// <summary>
        /// Конструирование отображаемой строки с ошибкой 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string CreateErrorString(params object[] args)
        {
            string[] ww = new string[args.Length - 1];
            for (int i = 1; i < args.Length; i++)
                ww[i - 1] = (string)args[i];
            string w = string.Join(" или ", ww);
            return string.Format("Синтаксическая ошибка: встречено {0}, а ожидалось {1}", args[0], w);
        }
    }
}