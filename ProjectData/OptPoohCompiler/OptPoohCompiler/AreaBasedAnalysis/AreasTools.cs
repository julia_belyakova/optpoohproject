﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.AreaBasedAnalysis.Areas;
using OptPoohCompiler.AreaBasedAnalysis.TransferFunctions;

namespace OptPoohCompiler.AreaBasedAnalysis
{
    /// <summary>
    /// Вспомогательные инструменты для анализа
    /// на основе областей
    /// </summary>
    public static class AreasTools
    {
        /// Все доступные алгоритмы анализа на основе областей
        public static IAreaBasedAlgoApplyer[] AllDataFlowAnalysis = { 
                  new AvailableExprsAreaBasedAlgoApplyer()
        };

        /// <summary>
        /// Имя области
        /// </summary>
        public const string AREA_NAME = "R";

        /// <summary>
        /// Номер последней выданной области
        /// </summary>
        private static int currAreaNum = -1;

        /// <summary>
        /// Генерирует новое имя области
        /// </summary>
        /// <returns>Имя для области, которое еще не использовалось</returns>
        public static string NextAreaName()
        {
            ++currAreaNum;
            return (AREA_NAME + currAreaNum.ToString());
        }
        /// <summary>
        /// Сбрасывает генератор имен областей в начальное значение
        /// </summary>
        public static void ResetAreaGenerator()
        {
            currAreaNum = -1;
        }

        /// <summary>
        /// Связывает вершины source и dest графа областей дугой (source, dest)
        /// </summary>
        /// <param name="source">Вершина-начало дуги</param>
        /// <param name="dest">Вершина-конец дуги</param>
        public static void ConnectVertices(VertexArea from, VertexArea to)
        {
            from.AddOutVertex(to);
            to.AddInVertex(from);
        }

        public static void DisconnectVertices(VertexArea from, VertexArea to)
        {
            from.RemoveOutVertex(to);
            to.RemoveInVertex(from);
        }
    }

    /// <summary>
    /// Пара: добавляемое и удаляемое множество в передаточной функции,
    /// работающей со множествами
    /// </summary>
    /// <typeparam name="T">Тип элементов множества</typeparam>
    public struct BasicBlockSetTFData<T>
    {
        public SetDFADataType<T> addingSet;
        public SetDFADataType<T> subtractingSet;

        /// <summary>
        /// Пара: добавляемое и удаляемое множество в передаточной функции,
        /// работающей со множествами
        /// </summary>
        /// <param name="addingSet">Добавляемое множество</param>
        /// <param name="subtractingSet">Удаляемое множество</param>
        public BasicBlockSetTFData(SetDFADataType<T> addingSet, SetDFADataType<T> subtractingSet)
        {
            this.addingSet = addingSet;
            this.subtractingSet = subtractingSet;
        }
    }

}
