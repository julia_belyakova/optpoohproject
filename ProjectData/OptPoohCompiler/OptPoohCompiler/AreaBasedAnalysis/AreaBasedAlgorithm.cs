﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.AreaBasedAnalysis.TransferFunctions;
using OptPoohCompiler.AreaBasedAnalysis.Areas;
using OptPoohCompiler.ControlFlowGraphAnalysis.NaturalLoops;

namespace OptPoohCompiler.AreaBasedAnalysis
{
    /// <summary>
    /// Алгоритм анализа потоков данных 
    /// на основе анализа областей
    /// </summary>
    /// <typeparam name="T">Тип элементов множества данных анализа</typeparam>
    /// <typeparam name="Joiner">Оператор сбора (UnionJoiner или IntersectJoiner в зависимости от задачи)</typeparam>
    public abstract class AreaBasedAlgorithm<T, Joiner>
        where Joiner : IDFADataTypeJoiner<T>, new()
    {
        //struct AreaTransferFunctionPair<T, Joiner>
        //    where Joiner : IDFADataTypeJoiner<T>
        //{
        //    public AreaTransferFunction<T, Joiner> f_R_InB;
        //    public AreaTransferFunction<T, Joiner> f_R_OutB;

        //    public AreaTransferFunctionPair(AreaTransferFunction<T, Joiner> f_R_InB, AreaTransferFunction<T, Joiner> f_R_OutB)
        //    {
        //        this.f_R_InB = f_R_InB;             //??????НАПИСАТЬ КОНСТРУКТОР КОПИИ??????
        //        this.f_R_OutB = f_R_OutB;
        //    }
        //}

        //AreaTransferFunctionPair<T, Joiner>[,] AreaTFMap;

        /// <summary>
        /// Граф потоков управления
        /// </summary>
        private GraphBB cfg;

        /// <summary>
        /// Всё множество данных анализа (должно быть инициализировано)
        /// </summary>
        protected SetDFADataType<T> fullAnalysisSet;
        /// <summary>
        /// Класс, реализующий оператор сбора
        /// </summary>
        private Joiner joiner = new Joiner();

        /// <summary>
        /// Самая внешняя область -- одна точка
        /// </summary>
        private Area outerArea;

        private Dictionary<Area, Dictionary<Area, AreaTransferFunction<T, Joiner>>> f_R_INs =
            new Dictionary<Area, Dictionary<Area, AreaTransferFunction<T, Joiner>>>();
        private Dictionary<Area, Dictionary<GraphBBVertex, AreaTransferFunction<T, Joiner>>> f_R_OUTs =
            new Dictionary<Area, Dictionary<GraphBBVertex, AreaTransferFunction<T, Joiner>>>();

        private Dictionary<GraphBBVertex, AreaTransferFunction<T, Joiner>> basicBlockFuns =
            new Dictionary<GraphBBVertex, AreaTransferFunction<T, Joiner>>();


        /// <summary>
        /// Граф потоков управления
        /// </summary>
        protected GraphBB CFG
        {
            get { return cfg; }
        }

        /// <summary>
        /// Алгоритм на основе анализа областей
        /// </summary>
        /// <param name="cfg">Граф потока управления</param>
        public AreaBasedAlgorithm(GraphBB cfg)
        {
            if (cfg == null)
                throw new ArgumentNullException("Граф CFG не должен быть null");
            this.cfg = cfg;
            _InitAlgoData();
        }

        /// <summary>
        /// Применяет к графу CFG алгоритм анализа областей
        /// </summary>
        public void Execute()
        {
            if (outerArea == null)
                MakeStep1_AreaConstructing();
            MakeStep2_TransferFunsInit();
            MakeStep3_AnalysisDataFill();
        }

        public void SetOuterArea(Area outerArea)
        {
            this.outerArea = outerArea;
        }

        /// <summary>
        /// Здесь должно быть инициализировано множество fullAnalysisSet
        /// </summary>
        protected abstract void InitializeFullSet();

        /// <summary>
        /// Функция для данной вершины GraphBBVertex дожна вернуть пару множеств:
        /// добавляемое множество (addingSet) и удаляемое множество (subtractingSet) 
        /// в формуле передаточной функции для базового блока вершины bbVertex
        /// </summary>
        /// <param name="bbVertex">Вершина с базовым блоком</param>
        /// <returns>Пару множеств, определяющих передаточную функцию базового блока</returns>
        protected abstract BasicBlockSetTFData<T> InitBBTransferFunData(GraphBBVertex bbVertex);

        /// <summary>
        /// Инициализация вспомогательных данных для алгоритма
        /// </summary>
        private void _InitAlgoData()
        {
            InitializeFullSet();
            joiner.InitFullSet(this.fullAnalysisSet);
            //AreaTFMap = new AreaTransferFunctionPair<T, Joiner>[this.CFG.Verticies.Count, this.CFG.Verticies.Count];
        }

        /// <summary>
        /// Первый шаг алгоритма -- построение  восходящей последовательности
        /// областей. В результате должна быть инициализирована самая внешняя
        /// область -- outerArea
        /// </summary>
        private void MakeStep1_AreaConstructing()
        {
            //AreasTools.ResetAreaGenerator();
            //outerArea = GraphArea.FromGraphBB(cfg).ToArea();
            // строим граф областей из графа потока управления
            GraphArea areasGraph = _Step1GetAreaGraph();
            // трансформируем его, пока это возможно
            while (_Step1TransformAreasGraph(ref areasGraph)) ;
            // осталось построить область
            // нужно пометить выходные вершины (не содержат выходных дуг)
            foreach (VertexArea vertex in areasGraph.Verticies)
                if (vertex.OutVerticies.Count == 0)
                    vertex.IsOut = true;
            outerArea = new AreaBody(AreasTools.NextAreaName(), areasGraph);
        }

        /// <summary>
        /// Второй шаг алгоритма -- построение передаточных функций
        /// для всех областей
        /// </summary>
        private void MakeStep2_TransferFunsInit()
        {
            _ProcessArea(this.outerArea);
        }

        /// <summary>
        /// Третий шаг алгоритма -- заполнение графа данными
        /// анализа с помощью полученных передаточных функций
        /// </summary>
        private void MakeStep3_AnalysisDataFill()
        {
            this.outerArea.IN = new SetDFADataType<T>();
            _ProcessAreaStep3(outerArea);
        }

        /// <summary>
        /// Формирует граф областей-базовых блоков, соответствующих графу потока управления
        /// </summary>
        /// <returns>Граф областей-базовых блоков</returns>
        private GraphArea _Step1GetAreaGraph()
        {
            Dictionary<GraphBBVertex, VertexArea> areaVertices = 
                new Dictionary<GraphBBVertex, VertexArea>();
            // сбрасываем генератор имён областей
            AreasTools.ResetAreaGenerator();
            int blockNameLen = BasicBlocksTools.BASIC_BLOCK_NAME.Length;
            foreach (GraphBBVertex sourceVertex in cfg.Verticies)
            {
                string name = AreasTools.AREA_NAME + sourceVertex.Data.Name.Substring(blockNameLen);
                // пока пропускаем имя
                AreasTools.NextAreaName();
                AreaBasicBlock blockArea = new AreaBasicBlock(name, sourceVertex);
                areaVertices.Add(sourceVertex, new VertexArea(blockArea));
            }
            // настраиваем связи
            foreach (GraphBBVertex source in cfg.Verticies)
                foreach (GraphBBVertex dest in source.OutVerticies)
                    AreasTools.ConnectVertices(areaVertices[source], areaVertices[dest]);
            return new GraphArea(areaVertices.Values, areaVertices[cfg.InputVertex]);
        }

        /// <summary>
        /// Применяет один шаг построения областей к графу и меняет его, если возможно
        /// </summary>
        /// <param name="graph">Граф областей</param>
        /// <returns>Истину, если граф изменился, и ложь, если менять больше нечего</returns>
        private bool _Step1TransformAreasGraph(ref GraphArea graph)
        {
            // находим все внутренние циклы на графе
            HashSet<NaturalLoop<Area, VertexArea>> loops = 
                NaturalLoopsTools.GetInnerNaturalLoops<Area, VertexArea, GraphArea>(graph);
            // если циклов нет, то граф можно свернуть в одну точку, это конец
            if (loops.Count == 0)
                return false;
            VertexArea inputVertex = graph.InputVertex;
            // берём какой-нибудь один цикл
            var enumerator = loops.GetEnumerator();
            enumerator.MoveNext();
            NaturalLoop<Area, VertexArea> loop = enumerator.Current;
            // циклы из одной вершины нужно превратить в области цикла,
            // а циклы из нескольких -- в области тела
            // область цикла
            if (loop.Vertices.Count == 1)
            {
                VertexArea loopBody = loop.Header;
                // сохраняем входящие и исходящие вершины
                HashSet<VertexArea> sourceVertices = new HashSet<VertexArea>(loopBody.InVerticies);
                // кроме петли
                sourceVertices.Remove(loopBody);
                HashSet<VertexArea> destVertices = new HashSet<VertexArea>(loopBody.OutVerticies);
                // кроме петли
                destVertices.Remove(loopBody);
                // очищаем на графе все связи с данной вершиной
                foreach (VertexArea source in sourceVertices)
                    AreasTools.DisconnectVertices(source, loopBody);
                foreach (VertexArea dest in destVertices)
                    AreasTools.DisconnectVertices(loopBody, dest);
                AreaCycle loopAreaCycle = new AreaCycle(AreasTools.NextAreaName(), loopBody);
                VertexArea loopFull = new VertexArea(loopAreaCycle);
                // настраиваем связи с областю цикла
                foreach (VertexArea source in sourceVertices)
                    AreasTools.ConnectVertices(source, loopFull);
                foreach (VertexArea dest in destVertices)
                    AreasTools.ConnectVertices(loopFull, dest);
                // если входная вершина графа принадлежала циклу, её нужно изменить
                if (loop.Vertices.Contains(inputVertex))
                    inputVertex = loopFull;
                graph.Verticies.Add(loopFull);
            }
            // область тела
            else
            { 
                // сохраняем связи вершин цикла с остальным графом, удаляем их
                // у самих вершин
                HashSet<VertexArea> sourceVertices = new HashSet<VertexArea>();
                HashSet<VertexArea> destVertices = new HashSet<VertexArea>();
                foreach (VertexArea loopVertex in loop.Vertices)
                { 
                    foreach (VertexArea source in loopVertex.InVerticies)
                        if (!loop.Vertices.Contains(source))
                            sourceVertices.Add(source);
                    foreach (VertexArea source in sourceVertices)
                        AreasTools.DisconnectVertices(source, loopVertex);
                    foreach (VertexArea dest in loopVertex.OutVerticies)
                        if (!loop.Vertices.Contains(dest))
                        {
                            destVertices.Add(dest);
                            // раз есть связь с внешним миром, значит это выходная вершина области
                            loopVertex.IsOut = true;
                        }
                    foreach (VertexArea dest in destVertices)
                        AreasTools.DisconnectVertices(loopVertex, dest);
                }
                // нужно удалить обратные дуги и пометить соответствующие вершины 
                // выходными
                HashSet<VertexArea> loopSourceVertices = new HashSet<VertexArea>();
                loopSourceVertices.UnionWith(loop.Header.InVerticies);
                foreach (VertexArea loopPrev in loopSourceVertices)
                {
                    AreasTools.DisconnectVertices(loopPrev, loop.Header);
                    loopPrev.IsOut = true;
                }
                // теперь вершины области тела связаны только внутренними дугами,
                // выходные вершины помечены
                AreaBody loopAreaBody = new AreaBody(AreasTools.NextAreaName(), new GraphArea(
                    loop.Vertices, loop.Header));
                VertexArea loopBody = new VertexArea(loopAreaBody);
                // добавляем петлю
                AreasTools.ConnectVertices(loopBody, loopBody);
                // настраиваем связи с внешним миром
                foreach (VertexArea source in sourceVertices)
                    AreasTools.ConnectVertices(source, loopBody);
                foreach (VertexArea dest in destVertices)
                    AreasTools.ConnectVertices(loopBody, dest);
                // если входная вершина графа принадлежала циклу, её нужно изменить
                if (loop.Vertices.Contains(inputVertex))
                    inputVertex = loopBody;
                graph.Verticies.Add(loopBody);
            }
            graph.Verticies.ExceptWith(loop.Vertices);
            graph = new GraphArea(graph.Verticies, inputVertex);
            return true;
        }

        /// <summary>
        /// Рекурсивная часть второго шага алгоритма -- обработка области
        /// </summary>
        /// <param name="currentArea">Текущая область</param>
        private void _ProcessArea(Area currentArea)
        {
            switch (currentArea.Kind)
            {
                case AreaKind.BasicBlock:
                    _ProcessAreaBasicBlock(currentArea as AreaBasicBlock);
                    break;
                case AreaKind.Body:
                    _ProcessAreaBody(currentArea as AreaBody);
                    break;
                case AreaKind.Cycle:
                    _ProcessAreaCycle(currentArea as AreaCycle);
                    break;
            }
        }

        /// <summary>
        /// Рекурсивная часть второго шага алгоритма -- обработка области
        /// </summary>
        /// <param name="currentArea">Текущая область</param>
        private void _ProcessAreaStep3(Area currentArea)
        {
            switch (currentArea.Kind)
            {
                case AreaKind.BasicBlock:
                    _ProcessAreaBasicBlockStep3(currentArea as AreaBasicBlock);
                    break;
                case AreaKind.Body:
                    _ProcessAreaBodyStep3(currentArea as AreaBody);
                    break;
                case AreaKind.Cycle:
                    _ProcessAreaCycleStep3(currentArea as AreaCycle);
                    break;
            }
        }


        /// <summary>
        /// Обработка области-базового блока на втором шаге алгоритма
        /// </summary>
        /// <param name="areaBB">Область-базовый блок</param>
        private void _ProcessAreaBasicBlock(AreaBasicBlock areaBB)
        {
            // данные передаточной функции для базового блока (Fb)
            BasicBlockSetTFData<T> bbTransferFunData = InitBBTransferFunData(
                areaBB.InnerBasicBlockVertex);
            // F_R_IN[B]
            AreaTransferFunction<T, Joiner> f_R_InB = 
                AreaTransferFunction<T, Joiner>.IdentTransferFunction(joiner);
            // F_R_OUT[B]
            AreaTransferFunction<T, Joiner> f_R_OutB = new AreaTransferFunction<T, Joiner>(
                this.joiner, bbTransferFunData.addingSet, bbTransferFunData.subtractingSet);
            basicBlockFuns.Add(areaBB.InnerBasicBlockVertex, f_R_OutB);

            var inB = new Dictionary<Area, AreaTransferFunction<T, Joiner>>();
            inB.Add(areaBB, f_R_InB);
            f_R_INs.Add(areaBB, inB);

            var outB = new Dictionary<GraphBBVertex, AreaTransferFunction<T, Joiner>>();
            outB.Add(areaBB.InnerBasicBlockVertex, f_R_OutB);
            f_R_OUTs.Add(areaBB, outB);
        }

        /// <summary>
        /// Обработка области тела на втором шаге алгоритма
        /// </summary>
        /// <param name="areaBody">Область тела</param>
        private void _ProcessAreaBody(AreaBody areaBody)
        {
            // сначала обрабатываем все подобласти, это обеспечивает
            // восходящий порядок
            foreach (VertexArea subAreaVertex in areaBody.SubAreasGraph.Verticies)
                _ProcessArea(subAreaVertex.Data);
            var rINs = new Dictionary<Area, AreaTransferFunction<T, Joiner>>();
            f_R_INs.Add(areaBody, rINs);
            var rOUTs = new Dictionary<GraphBBVertex, AreaTransferFunction<T, Joiner>>();
            f_R_OUTs.Add(areaBody, rOUTs);
            // теперь в топологическом порядке обрабатываем вершины-подобласти 
            // текущей области тела
            foreach (VertexArea subAreaVertex in areaBody.SubAreasGraph.SortedVerticies)
            {
                // сама подобласть
                Area subArea = subAreaVertex.Data;
                AreaTransferFunction<T, Joiner> inS;
                var ancestors = areaBody.GetSubAreaAncestors(subAreaVertex).ToArray();
                if (ancestors.Length == 0)
                    inS = AreaTransferFunction<T, Joiner>.IdentTransferFunction(joiner);
                else
                {
                    var tf = (f_R_OUTs[areaBody])[ancestors[0]];
                    inS = new AreaTransferFunction<T, Joiner>(joiner, tf.AddingSet, tf.SubtractingSet);
                }
                for (int i = 1; i < ancestors.Length; ++i)
                {
                    var tf = (f_R_OUTs[areaBody])[ancestors[i]];
                    inS.JoinWith(tf);
                }
                rINs.Add(subArea, inS);

                foreach (GraphBBVertex outVert in subArea.GetOutBlockVertices())
                {
                    var outB = AreaTransferFunction<T, Joiner>.IdentTransferFunction(joiner);
                    outB.ComposeWith((f_R_INs[areaBody])[subArea]);
                    outB.ComposeWith((f_R_OUTs[subArea])[outVert]);
                    rOUTs.Add(outVert, outB);
                }
            }
        }

        /// <summary>
        /// Обработка области цикла на втором шаге алгоритма
        /// </summary>
        /// <param name="areaCycle">Область цикла</param>
        private void _ProcessAreaCycle(AreaCycle areaCycle)
        {
            // сначала обрабатываем подобласть тела, это обеспечивает
            // восходящий порядок
            _ProcessArea(areaCycle.SubAreaVertex.Data);
            // сама подобласть тела
            Area subArea = areaCycle.SubAreaVertex.Data;
            var rINs = new Dictionary<Area, AreaTransferFunction<T, Joiner>>();
            f_R_INs.Add(areaCycle, rINs);
            var rOUTs = new Dictionary<GraphBBVertex, AreaTransferFunction<T, Joiner>>();
            f_R_OUTs.Add(areaCycle, rOUTs);
            // in[S]
            AreaTransferFunction<T, Joiner> inS;
            var ancestors = areaCycle.GetSubAreaAncestors().ToArray();
            if (ancestors.Length == 0)
                inS = AreaTransferFunction<T, Joiner>.IdentTransferFunction(joiner);
            else
            {
                var tf = (f_R_OUTs[subArea])[ancestors[0]];
                inS = new AreaTransferFunction<T, Joiner>(joiner, tf.AddingSet, tf.SubtractingSet);
            }
            for (int i = 1; i < ancestors.Length; ++i)
            {
                var tf = (f_R_OUTs[subArea])[ancestors[i]];
                inS.JoinWith(tf);
            }
            /*

            var inS = AreaTransferFunction<T, Joiner>.IdentTransferFunction(joiner);
            foreach (GraphBBVertex prevVert in areaCycle.GetSubAreaAncestors())
                inS.JoinWith((f_R_OUTs[subArea])[prevVert]);*/
            rINs.Add(subArea, inS.GetClosure());
            // out[S]
            foreach (GraphBBVertex outVert in subArea.GetOutBlockVertices())
            {
                var outB = AreaTransferFunction<T, Joiner>.IdentTransferFunction(joiner);
                outB.ComposeWith((f_R_INs[areaCycle])[subArea]);
                outB.ComposeWith((f_R_OUTs[subArea])[outVert]);
                rOUTs.Add(outVert, outB);
            }
        }

        /// <summary>
        /// 3 шаг алгоритма для области базового блока
        /// </summary>
        /// <param name="areaBlock"></param>
        private void _ProcessAreaBasicBlockStep3(AreaBasicBlock areaBlock)
        {
            areaBlock.InnerBasicBlockVertex.IN = areaBlock.IN;
            areaBlock.InnerBasicBlockVertex.OUT =
                basicBlockFuns[areaBlock.InnerBasicBlockVertex].Apply(
                areaBlock.InnerBasicBlockVertex.IN as SetDFADataType<T>);
        }

        /// <summary>
        /// 3 шаг алгоритма для области тела
        /// </summary>
        /// <param name="areaBody"></param>
        private void _ProcessAreaBodyStep3(AreaBody areaBody)
        {
            foreach (VertexArea subAreaVertex in areaBody.SubAreasGraph.SortedVerticies)
            {
                var subArea = subAreaVertex.Data;
                subArea.IN = (f_R_INs[areaBody])[subArea].Apply(
                    areaBody.IN as SetDFADataType<T>);
                _ProcessAreaStep3(subArea);
            }
        }

        /// <summary>
        /// 3 шаг алгоритма для области цикла
        /// </summary>
        /// <param name="areaBody"></param>
        private void _ProcessAreaCycleStep3(AreaCycle areaCycle)
        {
            var subArea = areaCycle.SubAreaVertex.Data;
            subArea.IN = (f_R_INs[areaCycle])[subArea].Apply(
                areaCycle.IN as SetDFADataType<T>);
            _ProcessAreaStep3(subArea);
        }
    }
}
