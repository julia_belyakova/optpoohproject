﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.AreaBasedAnalysis
{
    /// <summary>
    /// Интерфейс применителя алгоритма анализа на основе областей
    /// </summary>
    public interface IAreaBasedAlgoApplyer
    {
        /// <summary>
        /// Применяет алгоритм анализа к данному графу потоков управления
        /// </summary>
        /// <param name="cfg">Граф потоков управления</param>
        void Apply(GraphBB cfg);
    }
}
