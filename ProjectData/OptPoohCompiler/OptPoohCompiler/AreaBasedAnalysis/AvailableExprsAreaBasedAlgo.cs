﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.OptimizationsInBB.SharedExpressions;
using OptPoohCompiler.AreaBasedAnalysis.TransferFunctions;
using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.Addr3Code;
using OptPoohCompiler.DataFlowAnalysis.AvailableExpressions;

namespace OptPoohCompiler.AreaBasedAnalysis
{
    /// <summary>
    /// Алгоритм анализа доступных выражений 
    /// на основе анализа областей
    /// </summary>
    public class AvailableExprsAreaBasedAlgo : AreaBasedAlgorithm<SharedExpression, IntersectJoiner<SharedExpression>>
    {
        /// <summary>
        /// Алгоритм анализа доступных выражений 
        /// на основе анализа областей
        /// </summary>
        /// <param name="cfg">Граф потоков управления</param>
        public AvailableExprsAreaBasedAlgo(GraphBB cfg)
            : base(cfg)
        { }

        /// <summary>
        /// Инициализируем множество fullAnalysisSet всеми выражениями
        /// в графе CFG
        /// </summary>
        protected override void InitializeFullSet()
        {
            this.fullAnalysisSet = new SetDFADataType<SharedExpression>();
            foreach (GraphBBVertex vertex in this.CFG.SortedVerticies)
                foreach (Addr3Command cmd in vertex.Data.Body)
                    // работаем с командами-выражениями
                    if (cmd.HasAssign() && (cmd.cmdType != Addr3CmdType.ASSIGN))
                        fullAnalysisSet.Add(new SharedExpression(cmd));
        }

        /// <summary>
        /// Функция для данной вершины GraphBBVertex дожна вернуть пару множеств:
        /// добавляемое множество (addingSet) и удаляемое множество (subtractingSet) 
        /// в формуле передаточной функции для базового блока вершины bbVertex
        /// </summary>
        /// <param name="bbVertex">Вершина с базовым блоком</param>
        /// <returns>Пару множеств, определяющих передаточную функцию базового блока</returns>
        protected override BasicBlockSetTFData<SharedExpression> InitBBTransferFunData(GraphBBVertex bbVertex)
        {
            BasicBlockSetTFData<SharedExpression> result;
            AvailableExprTransferFunction transferFun = new AvailableExprTransferFunction();
            transferFun.GlobalInitialize(fullAnalysisSet);
            transferFun.Initialize(bbVertex.Data);
            result.addingSet = new SetDFADataType<SharedExpression>(transferFun.AddingSet);
            result.subtractingSet = new SetDFADataType<SharedExpression>(transferFun.SubtractingSet);
            return result;
        }
    }
}
