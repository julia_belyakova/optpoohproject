﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.AreaBasedAnalysis.TransferFunctions
{
    /// <summary>
    /// Компонент, опеределяющий оператор сбора для множеств анализа потока данных
    /// в анализе областей
    /// </summary>
    /// <typeparam name="T">Тип элементов во множестве</typeparam>
    public interface IDFADataTypeJoiner<T>
    {
        /// <summary>
        /// Инициализирует универсальное множество
        /// </summary>
        /// <param name="fullSet">Универсальное множество</param>
        void InitFullSet(SetDFADataType<T> fullSet);

        /// <summary>
        /// Оператор сбора для двух множеств анализа потока данных
        /// </summary>
        /// <param name="a">Первое множество</param>
        /// <param name="b">Второе множество</param>
        /// <returns>Множество-результат применения оператора сбора</returns>
        SetDFADataType<T> Join(SetDFADataType<T> a, SetDFADataType<T> b);

        /// <summary>
        /// Вычисляет пару (kill, gen) для (kill1, gen1) ^ (kill2, gen2)
        /// </summary>
        /// <param name="add1">Первое добавляемое множество</param>
        /// <param name="sub1">Первое отнимемое множество</param>
        /// <param name="add2">Второе добавляемое множество</param>
        /// <param name="sub2">Второе отнимемое множество</param>
        /// <returns>Результат оператора сбора для функций</returns>
        BasicBlockSetTFData<T> CalcTransferFunData(
            SetDFADataType<T> add1, SetDFADataType<T> sub1, SetDFADataType<T> add2, SetDFADataType<T> sub2);

        /// <summary>
        /// Вычисляет пару (kill, gen) замыкания передаточной функции (add, sub)
        /// </summary>
        /// <param name="add">Добавляемое множество</param>
        /// <param name="sub">Отнимаемое множество</param>
        /// <returns>Замыкание передаточной функции</returns>
        BasicBlockSetTFData<T> CalcTransferFunClosureData(SetDFADataType<T> add, SetDFADataType<T> sub);
    }
}
