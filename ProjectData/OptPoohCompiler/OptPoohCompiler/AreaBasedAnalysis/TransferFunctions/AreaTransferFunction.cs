﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.AreaBasedAnalysis.TransferFunctions
{
    /// <summary>
    /// Передаточная функция для области
    /// </summary>
    /// <typeparam name="T">Тип элементов множества</typeparam>
    public class AreaTransferFunction<T, Joiner>
        where Joiner : IDFADataTypeJoiner<T>, new()
    {
        /// <summary>
        /// Добавляемое множество
        /// </summary>
        private SetDFADataType<T> addingSet = new SetDFADataType<T>();
        /// <summary>
        /// Удаляемое множество
        /// </summary>
        private SetDFADataType<T> subtractingSet = new SetDFADataType<T>();

        /// <summary>
        /// Компонент, определяющий оператор сбора
        /// </summary>
        private Joiner joiner;// = new Joiner();

        /// <summary>
        /// Добавляемое множество
        /// </summary>
        public SetDFADataType<T> AddingSet
        {
            get { return addingSet; }
        }
        /// <summary>
        /// Удаляемое множество
        /// </summary>
        public SetDFADataType<T> SubtractingSet
        {
            get { return subtractingSet; }
        }

        /// <summary>
        /// Конструирует тождественную передаточную функцию
        /// </summary>
        /// <param name="fullSet">Универсальное множество</param>
        /// <returns>Тождественную функцию</returns>
        public static AreaTransferFunction<T, Joiner> IdentTransferFunction(Joiner joiner)
        {
            return new AreaTransferFunction<T, Joiner>(joiner);
        }

        /// <summary>
        /// Передаточная функция для областей --  тождественная
        /// </summary>
        /// <param name="joiner">Компонент, определяющий оператор сбора</param>
        public AreaTransferFunction(Joiner joiner)
        {
            this.joiner = joiner;
        }

        /// <summary>
        /// Передаточная функция для областей (копирует множества-параметры)
        /// </summary>
        /// <param name="joiner">Компонент, определяющий оператор сбора</param>
        /// <param name="addingSet">Добавляемое множество</param>
        /// <param name="subtractingSet">Удаляемое множество</param>
        public AreaTransferFunction(Joiner joiner, SetDFADataType<T> addingSet, SetDFADataType<T> subtractingSet)
        {
            this.joiner = joiner;                
            this.addingSet.UnionWith(addingSet);
            this.subtractingSet.UnionWith(subtractingSet);
        }

        /// <summary>
        /// Применяет передаточную функцию к множеству данных анализа x
        /// и вычисляет результат
        /// </summary>
        /// <param name="x">Данные анализа</param>
        /// <returns>Результат применения функции к данным анализа x</returns>
        public SetDFADataType<T> Apply(SetDFADataType<T> x)
        {
            SetDFADataType<T> result = new SetDFADataType<T>();
            result.UnionWith(x);
            result.ExceptWith(this.subtractingSet);
            result.UnionWith(this.addingSet);
            return result;
        }

        /// <summary>
        /// Конструирует передаточную-функцию замыкание текущей
        /// </summary>
        /// <returns>Передаточную функцию-замыкание текущей</returns>
        public AreaTransferFunction<T, Joiner> GetClosure()
        {
            var result = this.joiner.CalcTransferFunClosureData(this.addingSet, this.subtractingSet);
            return new AreaTransferFunction<T, Joiner>(this.joiner,
                result.addingSet, result.subtractingSet);
        }

        /// <summary>
        /// Заменяет текущую функцию на композицию с внешней передаточной функцией other
        /// </summary>
        /// <param name="other">Внешняя передаточная функция композиции</param>
        public void ComposeWith(AreaTransferFunction<T, Joiner> other)
        {
            this.addingSet.ExceptWith(other.subtractingSet);
            this.addingSet.UnionWith(other.addingSet);
            this.subtractingSet.UnionWith(other.subtractingSet);
        }

        /// <summary>
        /// Заменяет текущую функцию на оператор сбора с передаточной функцией other
        /// </summary>
        /// <param name="other">Передаточная функция</param>
        public void JoinWith(AreaTransferFunction<T, Joiner> other)
        {
            var result = this.joiner.CalcTransferFunData(
                this.addingSet, this.subtractingSet, other.addingSet, other.subtractingSet);
            this.addingSet = result.addingSet;
            this.subtractingSet = result.subtractingSet;
        }
    }
}
