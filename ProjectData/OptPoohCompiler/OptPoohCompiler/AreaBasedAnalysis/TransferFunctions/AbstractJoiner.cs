﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.AreaBasedAnalysis.TransferFunctions
{
    /// <summary>
    /// Абстрактный предок Joiner'ов
    /// </summary>
    /// <typeparam name="T">Тип элементов множества</typeparam>
    public abstract class AbstractJoiner<T>
    {
        /// <summary>
        /// Универсальное множество
        /// </summary>
        protected SetDFADataType<T> fullSet = new SetDFADataType<T>();

        /// <summary>
        /// Инициализирует универсальное множество
        /// </summary>
        /// <param name="fullSet">Универсальное множество</param>
        public void InitFullSet(SetDFADataType<T> fullSet)
        {
            if (fullSet == null)
                throw new ArgumentException("Универсальное множество не должно быть null");
            this.fullSet.UnionWith(fullSet);
        }
    }
}
