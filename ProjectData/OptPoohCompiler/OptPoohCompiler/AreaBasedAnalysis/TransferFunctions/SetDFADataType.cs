﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.AreaBasedAnalysis.TransferFunctions
{
    /// <summary>
    /// Тип множества данных анализа областей
    /// </summary>
    /// <typeparam name="T">Тип элементов множества</typeparam>
    public class SetDFADataType<T> : HashSet<T>
    {
        /// <summary>
        /// Данные анализа -- множество значений типа T 
        /// (пустое множество)
        /// </summary>
        public SetDFADataType()
            : base()
        {
        }

        /// <summary>
        /// Данные анализа -- множество значений типа T 
        /// (множество значений из collection)
        /// </summary>
        /// <param name="collection">Коллекция начальных элементов множества</param>
        public SetDFADataType(IEnumerable<T> collection)
            : base(collection)
        { 
        
        }

        public bool Equals(SetDFADataType<T> other)
        {
            return this.SetEquals(other);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (this.Count > 0)
            {
                var enumer = this.GetEnumerator();
                enumer.MoveNext();
                sb.Append(enumer.Current.ToString());
                while (enumer.MoveNext())
                    sb.Append(", " + enumer.Current.ToString());
            }
            return sb.ToString();
        }
    }
}
