﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.AreaBasedAnalysis.TransferFunctions
{
    /// <summary>
    /// Компонент, опеределяющий оператор сбора-пересечение для множеств 
    /// анализа потока данных в анализе областей
    /// </summary>
    /// <typeparam name="T">Тип элементов во множестве</typeparam>
    public class IntersectJoiner<T> : AbstractJoiner<T>, IDFADataTypeJoiner<T>
    {

        /// <summary>
        /// Оператор сбора-пересечение для двух множеств анализа потока данных
        /// </summary>
        /// <param name="a">Первое множество</param>
        /// <param name="b">Второе множество</param>
        /// <returns>Множество-результат применения оператора сбора</returns>
        public SetDFADataType<T> Join(SetDFADataType<T> a, SetDFADataType<T> b)
        {
            SetDFADataType<T> result = new SetDFADataType<T>();
            result.UnionWith(a);
            result.IntersectWith(b);
            return result;
        }

        /// <summary>
        /// Вычисляет пару (kill, gen) для (kill1, gen1) ^ (kill2, gen2)
        /// </summary>
        /// <param name="add1">Первое добавляемое множество</param>
        /// <param name="sub1">Первое отнимемое множество</param>
        /// <param name="add2">Второе добавляемое множество</param>
        /// <param name="sub2">Второе отнимемое множество</param>
        /// <returns>Результат оператора сбора для функций</returns>
        public BasicBlockSetTFData<T> CalcTransferFunData(
            SetDFADataType<T> add1, SetDFADataType<T> sub1, SetDFADataType<T> add2, SetDFADataType<T> sub2)
        {
            SetDFADataType<T> add = new SetDFADataType<T>();
            add.UnionWith(add1);
            add.IntersectWith(add2);
            // sub...
            SetDFADataType<T> notSub1 = new SetDFADataType<T>();
            notSub1.UnionWith(fullSet);
            notSub1.ExceptWith(sub1);
            SetDFADataType<T> notSub2 = new SetDFADataType<T>();
            notSub2.UnionWith(fullSet);
            notSub2.ExceptWith(sub2);
            SetDFADataType<T> part1 = new SetDFADataType<T>();
            part1.UnionWith(add1);
            part1.IntersectWith(notSub2);
            SetDFADataType<T> part2 = new SetDFADataType<T>();
            part2.UnionWith(add2);
            part2.IntersectWith(notSub1);
            SetDFADataType<T> part3 = new SetDFADataType<T>();
            part3.UnionWith(notSub1);
            part3.IntersectWith(notSub2);
            SetDFADataType<T> notSub = new SetDFADataType<T>();
            notSub.UnionWith(part1);
            notSub.UnionWith(part2);
            notSub.UnionWith(part3);
            SetDFADataType<T> sub = new SetDFADataType<T>();
            sub.UnionWith(fullSet);
            sub.ExceptWith(notSub);
            return new BasicBlockSetTFData<T>(add, sub);
        }

        /// <summary>
        /// Вычисляет пару (kill, gen) замыкания передаточной функции (add, sub)
        /// </summary>
        /// <param name="add">Добавляемое множество</param>
        /// <param name="sub">Отнимаемое множество</param>
        /// <returns>Замыкание передаточной функции</returns>
        public BasicBlockSetTFData<T> CalcTransferFunClosureData(SetDFADataType<T> add, SetDFADataType<T> sub)
        {
            SetDFADataType<T> addClosure = new SetDFADataType<T>();
            SetDFADataType<T> subClosure = new SetDFADataType<T>();
            SetDFADataType<T> notAdd = new SetDFADataType<T>();
            notAdd.UnionWith(fullSet);
            notAdd.ExceptWith(add);
            subClosure.UnionWith(notAdd);
            subClosure.IntersectWith(sub);
            return new BasicBlockSetTFData<T>(addClosure, subClosure);
        }
    }
}
