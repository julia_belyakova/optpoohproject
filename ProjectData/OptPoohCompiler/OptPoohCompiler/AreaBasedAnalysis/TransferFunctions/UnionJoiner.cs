﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.AreaBasedAnalysis.TransferFunctions
{
    /// <summary>
    /// Компонент, опеределяющий оператор сбора-объединение для множеств 
    /// анализа потока данных в анализе областей
    /// </summary>
    /// <typeparam name="T">Тип элементов во множестве</typeparam>
    public class UnionJoiner<T> : AbstractJoiner<T>, IDFADataTypeJoiner<T>
    {

        /// <summary>
        /// Оператор сбора-объединение для двух множеств анализа потока данных
        /// </summary>
        /// <param name="a">Первое множество</param>
        /// <param name="b">Второе множество</param>
        /// <returns>Множество-результат применения оператора сбора</returns>
        public SetDFADataType<T> Join(SetDFADataType<T> a, SetDFADataType<T> b)
        {
            SetDFADataType<T> result = new SetDFADataType<T>();
            result.UnionWith(a);
            result.UnionWith(b);
            return result;
        }

        /// <summary>
        /// Вычисляет пару (kill, gen) для (kill1, gen1) ^ (kill2, gen2)
        /// </summary>
        /// <param name="add1">Первое добавляемое множество</param>
        /// <param name="sub1">Первое отнимемое множество</param>
        /// <param name="add2">Второе добавляемое множество</param>
        /// <param name="sub2">Второе отнимемое множество</param>
        /// <returns>Результат оператора сбора для функций</returns>
        public BasicBlockSetTFData<T> CalcTransferFunData(
            SetDFADataType<T> add1, SetDFADataType<T> sub1, SetDFADataType<T> add2, SetDFADataType<T> sub2)
        {
            SetDFADataType<T> add = new SetDFADataType<T>();
            add.UnionWith(add1);
            add.UnionWith(add2);
            SetDFADataType<T> sub = new SetDFADataType<T>();
            sub.UnionWith(sub1);
            sub.IntersectWith(sub2);
            return new BasicBlockSetTFData<T>(add, sub);
        }

        /// <summary>
        /// Вычисляет пару (kill, gen) замыкания передаточной функции (add, sub)
        /// </summary>
        /// <param name="add">Добавляемое множество</param>
        /// <param name="sub">Отнимаемое множество</param>
        /// <returns>Замыкание передаточной функции</returns>
        public BasicBlockSetTFData<T> CalcTransferFunClosureData(SetDFADataType<T> add, SetDFADataType<T> sub)
        {
            SetDFADataType<T> addClosure = new SetDFADataType<T>();
            addClosure.UnionWith(add);
            SetDFADataType<T> subClosure = new SetDFADataType<T>();
            return new BasicBlockSetTFData<T>(addClosure, subClosure);
        }
    }
}
