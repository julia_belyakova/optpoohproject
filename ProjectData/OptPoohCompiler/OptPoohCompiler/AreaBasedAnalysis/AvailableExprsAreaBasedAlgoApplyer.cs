﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.AreaBasedAnalysis
{
    /// <summary>
    /// Применитель алгоритма анализа доступных выражений
    /// на основе анализа областей
    /// </summary>
    public class AvailableExprsAreaBasedAlgoApplyer : IAreaBasedAlgoApplyer
    {
        /// <summary>
        /// Применяет к данному графу потоков управления алгоритм 
        /// анализа доступных выражений на основе анализа областей 
        /// </summary>
        /// <param name="cfg">Граф потоков управления</param>
        public void Apply(BasicBlocks.GraphBB cfg)
        {
            AvailableExprsAreaBasedAlgo algo = new AvailableExprsAreaBasedAlgo(cfg);
            algo.Execute();
        }

        public override string ToString()
        {
            return "Анализ доступных выражений";
        }
    }
}
