﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.AreaBasedAnalysis.Areas
{
    
    /// <summary>
    /// Область тела
    /// </summary>
    public class AreaBody : Area
    {
        /// <summary>
        /// Граф подобластей
        /// </summary>
        private GraphArea subAreasGraph;
        /// <summary>
        /// Выходные подобласти текущей области
        /// </summary>
        private HashSet<VertexArea> outAreaVertices;

        /// <summary>
        /// Тип области -- область тела цикла
        /// </summary>
        public override AreaKind Kind
        {
            get { return AreaKind.Body; }
        }

        /// <summary>
        /// Базовый блок -- заголовок области (вершина CFG)
        /// </summary>
        public override GraphBBVertex Header 
        {
            get { return subAreasGraph.InputVertex.Data.Header; }
        }

        /// <summary>
        /// Граф подобластей
        /// </summary>
        public GraphArea SubAreasGraph
        {
            get { return this.subAreasGraph; }
        }

        /// <summary>
        /// Область -- область тела цикла
        /// </summary>
        /// <param name="name">Имя области</param>
        /// <param name="subAreasGraph">Граф подобластей</param>
        public AreaBody(string name, GraphArea subAreasGraph)
            : base(name)
        {
            if (subAreasGraph == null)
                throw new ArgumentNullException("Граф подобластей не может быть null");
            _CheckGraph(subAreasGraph);
            this.subAreasGraph = subAreasGraph;
            _InitOutAreaVertices();
        }

        /// <summary>
        /// Вычисляет по подобластям множество выходных базовых блоков -- вершин графа CFG 
        /// </summary>
        /// <returns>Множество выходных базовых блоков-вершин CFG</returns>
        public override HashSet<GraphBBVertex> GetOutBlockVertices()
        {
            HashSet<GraphBBVertex> outBlockVertices = new HashSet<GraphBBVertex>();
            foreach (VertexArea outVertex in outAreaVertices)
                outBlockVertices.UnionWith(outVertex.Data.GetOutBlockVertices());
            return outBlockVertices;
        }

        /// <summary>
        /// Определяет предшественников заголовка подобласти subAreaVertex,
        /// принадлежащих текущей области
        /// </summary>
        /// <param name="subAreaVertex">Вершина графа подобластей</param>
        /// <returns>Множество базовых блоков-вершин CFG, являющихся предшественниками
        /// заголовка подобласти subAreaVertex в текущей области</returns>
        public HashSet<GraphBBVertex> GetSubAreaAncestors(VertexArea subAreaVertex)
        {
            // подобласти-предшественники subAreaVertex
            HashSet<VertexArea> inSubAreas = subAreaVertex.InVerticies;
            // по всем подобластям-предшественникам собираем выходные базовые блоки
            HashSet<GraphBBVertex> allInBlockVertices = new HashSet<GraphBBVertex>();
            foreach (VertexArea area in inSubAreas)
                allInBlockVertices.UnionWith(area.Data.GetOutBlockVertices());
            // берём базовые блоки-предшественники заголовка искомой подобласти
            // и пересекаем их с найденными базовыми блоками
            allInBlockVertices.IntersectWith(subAreaVertex.Data.Header.InVerticies);
            return allInBlockVertices;
        }

        /// <summary>
        /// Проверка допустимости графа подобластей
        /// </summary>
        /// <param name="subAreasGraph">Граф подобластей</param>
        private void _CheckGraph(GraphArea subAreasGraph)
        {
            if (subAreasGraph.Verticies.Count == 0)
                throw new ArgumentOutOfRangeException("Граф не может быть пустым");
            if (subAreasGraph.Verticies.Count == 1)
                throw new ArgumentOutOfRangeException("Граф области тела не может быть из одной вершины");
            // область тела не должна содержать дуг, ведущих в её вход
            if (subAreasGraph.InputVertex.InVerticies.Count != 0)
                throw new ArgumentOutOfRangeException(
                    "Граф области тела не должен содержать дуг, ведущих во вход");
            // все дуги должны принадлежать подграфу
            foreach (VertexArea fromVertex in subAreasGraph.Verticies)
                foreach (VertexArea toVertex in fromVertex.OutVerticies)
                    if (!subAreasGraph.Verticies.Contains(toVertex))
                        throw new ArgumentOutOfRangeException(
                        "Все дуги области тела должны принадлежать вершинам её графа");
        }

        /// <summary>
        /// Инициализирует множество выходных подобластей текущей области
        /// </summary>
        private void _InitOutAreaVertices()
        {
            outAreaVertices = new HashSet<VertexArea>();
            foreach (VertexArea vertex in subAreasGraph.Verticies)
                if (vertex.IsOut)
                    outAreaVertices.Add(vertex);
            if (outAreaVertices.Count == 0)
                throw new ArgumentOutOfRangeException(
                    "Граф области тела должен содержать хоть одну выходную (граничную) вершину");
        }
    }
}
