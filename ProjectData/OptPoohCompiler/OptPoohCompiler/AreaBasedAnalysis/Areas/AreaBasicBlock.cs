﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.AreaBasedAnalysis.Areas
{
    /// <summary>
    /// Область -- один базовый блок
    /// </summary>
    public class AreaBasicBlock : Area
    {
        /// <summary>
        /// Базовый блок -- вершина графа CFG
        /// </summary>
        private GraphBBVertex innerBasicBlockVertex;

        /// <summary>
        /// Базовый блок -- вершина графа CFG
        /// </summary>
        public GraphBBVertex InnerBasicBlockVertex
        {
            get { return innerBasicBlockVertex; }
        }

        /// <summary>
        /// Тип области -- элементарная область базового блока
        /// </summary>
        public override AreaKind Kind
        {
            get { return AreaKind.BasicBlock; }
        }

        /// <summary>
        /// Базовый блок -- заголовок области (вершина CFG)
        /// </summary>
        public override GraphBBVertex Header 
        {
            get { return innerBasicBlockVertex; }
        }

        /// <summary>
        /// Элементарная область из одного базового блока
        /// </summary>
        /// <param name="name">Имя области</param>
        /// <param name="blockVertex">Вершина графа CFG -- базовый блок</param>
        public AreaBasicBlock(string name, GraphBBVertex blockVertex)
            : base(name)
        {
            if (blockVertex == null)
                throw new ArgumentNullException("Базовый блок в области не может быть null");
            this.innerBasicBlockVertex = blockVertex;
        }

        /// <summary>
        /// Вычисляет множество выходных базовых блоков -- вершин графа CFG по подобластям.
        /// В данном случае это только внутренний ББл
        /// </summary>
        /// <returns>Множество выходных базовых блоков-вершин CFG</returns>
        public override HashSet<GraphBBVertex> GetOutBlockVertices()
        {
            HashSet<GraphBBVertex> output = new HashSet<GraphBBVertex>();
            output.Add(this.innerBasicBlockVertex);
            return output;
        }
    }
}
