﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.GraphTools;

namespace OptPoohCompiler.AreaBasedAnalysis.Areas
{
    /// <summary>
    /// Тип области 
    /// </summary>
    public enum AreaKind { BasicBlock, Body, Cycle };

    /// <summary>
    /// Область
    /// </summary>
    public abstract class Area
    {
        /// <summary>
        /// Имя области
        /// </summary>
        private string name;

        /// <summary>
        /// Данные на входе в область
        /// </summary>
        public object IN = null;

        /// <summary>
        /// Имя области
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Тип области
        /// </summary>
        public abstract AreaKind Kind { get; }

        /// <summary>
        /// Базовый блок -- заголовок области (вершина CFG)
        /// </summary>
        public abstract GraphBBVertex Header { get; }

        /// <summary>
        /// Вычисляет множество выходных базовых блоков -- вершин графа CFG по подобластям 
        /// </summary>
        /// <returns>Множество выходных базовых блоков-вершин CFG</returns>
        public abstract HashSet<GraphBBVertex> GetOutBlockVertices();

        /// <summary>
        /// Область
        /// </summary>
        /// <param name="name">Имя области</param>
        public Area(string name)
        {
            if (name == "")
                throw new ArgumentOutOfRangeException("Имя области не должно быт пустым");
            this.name = name;
        }

        public override string ToString()
        {
            return "(" + this.name + ")";
        }
    }
}
