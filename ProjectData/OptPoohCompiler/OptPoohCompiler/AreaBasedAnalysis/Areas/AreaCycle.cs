﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.AreaBasedAnalysis.Areas
{
    /// <summary>
    /// Область цикла
    /// </summary>
    public class AreaCycle : Area
    {
        /// <summary>
        /// Одна подобласть тела
        /// </summary>
        private VertexArea subAreaVertex;

        /// <summary>
        /// Тип области -- область цикла
        /// </summary>
        public override AreaKind Kind
        {
            get { return AreaKind.Cycle; }
        }

        public override GraphBBVertex Header
        {
            get { return subAreaVertex.Data.Header; }
        }

        /// <summary>
        /// Одна подобласть тела
        /// </summary>
        public VertexArea SubAreaVertex
        {
            get { return subAreaVertex; }
        }

        /// <summary>
        /// Область -- область цикла
        /// </summary>
        /// <param name="name">Имя области</param>
        /// <param name="subAreaVertex">Вершина-подобласть тела цикла с дугами в себя</param>
        public AreaCycle(string name, VertexArea subAreaVertex)
            : base(name)
        {
            if (subAreaVertex == null)
                throw new ArgumentNullException("Вершина-подобласть не может быть null");
            _CheckSubAreaVertex(subAreaVertex);
            this.subAreaVertex = subAreaVertex;
        }

        /// <summary>
        /// Вычисляет по подобласти множество выходных базовых блоков -- вершин графа CFG 
        /// </summary>
        /// <returns>Множество выходных базовых блоков-вершин CFG</returns>
        public override HashSet<GraphBBVertex> GetOutBlockVertices()
        {
            HashSet<GraphBBVertex> outBlockVertices = new HashSet<GraphBBVertex>();
            outBlockVertices.UnionWith(this.subAreaVertex.Data.GetOutBlockVertices());
            return outBlockVertices;
        }

        /// <summary>
        /// Определяет предшественников заголовка подобласти (области тела)
        /// </summary>
        /// <returns>Множество базовых блоков-вершин CFG, являющихся предшественниками
        /// заголовка подобласти (тела) в текущей области</returns>
        public HashSet<GraphBBVertex> GetSubAreaAncestors()
        {
            // берём все выходные блоки подобласти
            HashSet<GraphBBVertex> allInBlockVertices = new HashSet<GraphBBVertex>();
            allInBlockVertices.UnionWith(subAreaVertex.Data.GetOutBlockVertices());
            // берём базовые блоки-предшественники заголовка подобласти
            // и пересекаем их с найденными базовыми блоками
            allInBlockVertices.IntersectWith(subAreaVertex.Data.Header.InVerticies);
            return allInBlockVertices;
        }

        /// <summary>
        /// Проверяет корректность вершины-подобласти
        /// </summary>
        /// <param name="subAreaVertex">Вершина-подобласть</param>
        private void _CheckSubAreaVertex(VertexArea subAreaVertex)
        {
            if (subAreaVertex.Data.Kind != AreaKind.Body && subAreaVertex.Data.Kind != AreaKind.BasicBlock)
                throw new ArgumentNullException("Вершина-подобласть должна быть областью тела или базовым блоком");
            if (subAreaVertex.OutVerticies.Count != 1)
                throw new ArgumentNullException("Вершина-подобласть тела должна содержать только петлю на себя");
            else if (!subAreaVertex.OutVerticies.Contains(subAreaVertex))
                throw new ArgumentNullException("Вершина-подобласть тела должна содержать только петлю на себя");
        }
    }
}
