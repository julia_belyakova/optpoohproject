﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.GraphTools;

namespace OptPoohCompiler.AreaBasedAnalysis.Areas
{
    /// <summary>
    /// Вершина, содержащая область
    /// </summary>
    public class VertexArea : IVertex<VertexArea, Area>
    {
        /// <summary>
        /// Область в вершине
        /// </summary>
        private Area data;
        /// <summary>
        /// Блоки-потомки (исходящие дуги)
        /// </summary>
        private HashSet<VertexArea> outVerticies = new HashSet<VertexArea>();
        /// <summary>
        /// Блоки-предшественники (входящие дуги)
        /// </summary>
        private HashSet<VertexArea> inVerticies = new HashSet<VertexArea>();
        /// <summary>
        /// Является ли данная вершина-область выходной для над-области
        /// </summary>
        private bool isOut = false;

        /// <summary>
        /// Область в вершине
        /// </summary>
        public Area Data
        {
            get { return this.data; }
        }

        /// <summary>
        /// Список выходных дуг
        /// (Не использовать для изменения списка!)
        /// </summary>
        public HashSet<VertexArea> OutVerticies
        {
            get { return this.outVerticies; }
        }
        /// <summary>
        /// Список входных дуг
        /// (Не использовать для изменения списка!)
        /// </summary>
        public HashSet<VertexArea> InVerticies
        {
            get { return this.inVerticies; }
        }

        /// <summary>
        /// Является ли данная вершина-область выходной для над-области
        /// </summary>
        public bool IsOut
        {
            get { return isOut; }
            set { this.isOut = value; }
        }

        /// <summary>
        /// Входные данные анализа
        /// </summary>
        public object IN { get; set; }
        /// <summary>
        /// Выходные данные анализа
        /// </summary>
        public object OUT { get; set; }

        /// <summary>
        /// Вершина графа областей
        /// </summary>
        /// <param name="data">Область в вершине</param>
        public VertexArea(Area area)
        {
            if (area == null)
                throw new ArgumentNullException("Область не может быть null");
            this.data = area;
        }
        /// <summary>
        /// Вершина графа областей
        /// </summary>
        /// <param name="data">Область в вершине</param>
        public VertexArea(Area area, bool isOut)
            : this(area)
        {
            this.isOut = isOut;
        }

        /// <summary>
        /// Добавляет входящую дугу (ссылку на вершину-предшественника)
        /// </summary>
        /// <param name="vertex">Вершина-предшестенник</param>
        public void AddInVertex(VertexArea vertex)
        {
            if (vertex == null)
                throw new ArgumentNullException("Вершина не может быть null");
            this.inVerticies.Add(vertex);
        }

        /// <summary>
        /// Удаляет входящую дугу (ссылку на вершину-предшественника)
        /// </summary>
        /// <param name="vertex">Вершина-предшестенник</param>
        /// <returns>true, если удалось, иначе false</returns>
        public bool RemoveInVertex(VertexArea vertex)
        {
            return this.inVerticies.Remove(vertex);
        }

        /// <summary>
        /// Удаляет исходящую дугу (ссылку на вершину-потомка).
        /// </summary>
        /// <param name="vertex">Вершина-потомок</param>
        /// <returns>true, если удалось, иначе false</returns>
        public bool RemoveOutVertex(VertexArea vertex)
        {
            return this.outVerticies.Remove(vertex);
        }

        /// <summary>
        /// Добавляет исходящую дугу (ссылку на вершину-потомка).
        /// Исходящих дуг не может быть больше двух.
        /// </summary>
        /// <param name="vertex">Вершина-потомок</param>
        public void AddOutVertex(VertexArea vertex)
        {
            if (vertex == null)
                throw new ArgumentNullException("Вершина не может быть null");
            this.outVerticies.Add(vertex);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.data.ToString());
            if (this.outVerticies.Count > 0)
            {
                sb.AppendLine();
                sb.Append("Arcs [" + this.data.Name + "]: ");
                var currDest = this.outVerticies.GetEnumerator();
                for (int i = 1; i < this.outVerticies.Count; ++i)
                {
                    currDest.MoveNext();
                    sb.Append(currDest.Current.Data.Name + ", ");
                }
                currDest.MoveNext();
                sb.Append(currDest.Current.Data.Name);
            }
            sb.AppendLine();
            sb.Append("IN = {" 
                + ((IN != null) ? IN.ToString() : "<empty>")
                + "}");
            sb.AppendLine();
            sb.Append("OUT = {"
                + ((OUT != null) ? OUT.ToString() : "<empty>")
                + "}");
               
            return sb.ToString();
        }

        /// <summary>
        /// Короткая строка-описатель вершины
        /// </summary>
        /// <returns>Короткую-строку описатель</returns>
        public string ShortString()
        {
            return this.Data.Name;
        }
    }
}
