﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.GraphTools;
using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.ControlFlowGraphAnalysis;


namespace OptPoohCompiler.AreaBasedAnalysis.Areas
{
    /// <summary>
    /// Граф областей
    /// </summary>
    public class GraphArea : Graph<VertexArea, Area>
    {
        DFSTAndArcsCFGData<Area, VertexArea, GraphArea> arcsData;

        /// <summary>
        /// Граф областей
        /// </summary>
        /// <param name="vertexCollection">Коллекция вершин</param>
        /// <param name="inputVertex">Входная вершина нового графа</param>
        public GraphArea(ICollection<VertexArea> vertexCollection, VertexArea inputVertex)
            : base(vertexCollection, inputVertex)
        {
            arcsData = new DFSTAndArcsCFGData<Area, VertexArea, GraphArea>(this);
            initHeads();
        }

        Dictionary<VertexArea, HashSet<VertexArea>> Heads;

        /// <summary>
        /// Создаёт новый граф областей из графа базовых блоков
        /// </summary>
        /// <param name="cfg">Граф базовых блоков, из которого требуется получить граф областей</param>
        /// <returns>Новый граф областей</returns>
        public static GraphArea FromGraphBB(GraphBB cfg)
        {
            List<VertexArea> VAList = new List<VertexArea>(from bbV in cfg.SortedVerticies select new VertexArea(new AreaBasicBlock(AreasTools.NextAreaName(), bbV)));
            foreach (GraphBBVertex fromBB in cfg.SortedVerticies)
                foreach (GraphBBVertex toBB in fromBB.OutVerticies)
                    AreasTools.ConnectVertices(VAList.First(new Func<VertexArea, bool>(delegate(VertexArea VA) { return (VA.Data as AreaBasicBlock).InnerBasicBlockVertex == fromBB; })),
                        VAList.First(new Func<VertexArea, bool>(delegate(VertexArea VA) { return (VA.Data as AreaBasicBlock).InnerBasicBlockVertex == toBB; })));
            return new GraphArea(VAList, VAList.First(new Func<VertexArea, bool>(delegate(VertexArea VA) { return (VA.Data as AreaBasicBlock).InnerBasicBlockVertex == cfg.InputVertex; })));
        }

        void initHeads()
        {
            Heads = new Dictionary<VertexArea, HashSet<VertexArea>>();
            HashSet<VertexArea> rev;
            foreach (VertexArea item in SortedVerticies)
            {
                rev = GetReverseArcs(item);
                if (rev.Count > 0)
                    Heads.Add(item, rev);
            }
        }

        public static int TopCountStep = 1000000;

        /// <summary>
        /// Преобразует текущий граф в восходящую  последовательность вложенных областей и возвращает область, в которой содержатся все остальные
        /// </summary>
        /// <returns>Самая большая область.</returns>
        public Area ToArea()
        {
            GraphArea GA = this;
            int CountStep = 0;
            while (GA.Heads.Count > 0)
            {
                GA = GA.CollapseInBody();
                GA = GA.CollapseInCycle();
                CountStep++;
                if (CountStep > TopCountStep)
                    throw new StackOverflowException("Программе не удалось построить восходящую последовательность областей за " + TopCountStep.ToString() + " шагов.");
            }
            return GA.FinalCollapse();
        }

        /// <summary>
        /// Финальное сворачивание. Желательно вызывать только после CollapseInBody() и CollapseInCycle(), так как метод работает исходя из-того, что в графе более не осталось циклов не свёрнутых в области
        /// </summary>
        /// <returns></returns>
        private Area FinalCollapse()
        {
            foreach (VertexArea item in this.SortedVerticies)
                if (item.OutVerticies.Count == 0)
                    item.IsOut = true;
            AreaBody Body = new AreaBody(AreasTools.NextAreaName(), this);
            return Body;
        }

        /// <summary>
        /// Сворачивает все тела циклов в области тел
        /// </summary>
        /// <returns>Новый граф, в котором больше нет тел циклов</returns>
        private GraphArea CollapseInBody()
        {
            GraphArea GA;
            foreach (VertexArea item in this.SortedVerticies)
            {
                if (TryInBody(item, out GA))
                    return GA;
            }
            return this;
        }

        /// <summary>
        /// Сворачивает все циклы в области циклов, желательно вызывать после CollapseInBody(), так как метод работает исходя из-того, что в графе более не осталось тел циклов, не свёрнутых в области тел.
        /// </summary>
        /// <returns>Новый граф, в котором больше нет циклов, вообще.</returns>
        private GraphArea CollapseInCycle()
        {
            GraphArea GA;
            foreach (VertexArea item in this.SortedVerticies)
            {
                if (TryInCycle(item, out GA))
                    return GA;
            }
            return this;
        }

        /// <summary>
        /// Пытается создать на основе текущего графа областей новый граф, объеденив несколько вершин в область цикла, с входной вершиной InputVertex.
        /// Если получилось, то возвращает true и помещает результат в GA, иначе возвращает false.
        /// </summary>
        /// <param name="InputVertex">Предполагаемая входная вершина новой области цикла</param>
        /// <param name="GA">Переманная в которую нужно записать новый граф</param>
        /// <returns>true, если всё вышеуказанное удалось, иначе false.</returns>
        private bool TryInCycle(VertexArea InputVertex, out GraphArea GA)
        {
            GA = null;
            if ((!InputVertex.InVerticies.Any()) && (!InputVertex.OutVerticies.Any())) return false;
            //HashSet<VertexArea> ReverseArcs = GetReverseArcs(InputVertex);
            //if ((ReverseArcs.Count > 0) & (ReverseArcs.Contains(InputVertex)))
            if (Heads.ContainsKey(InputVertex))
            {
                HashSet<VertexArea> ReverseArcs = Heads[InputVertex];
                if (ReverseArcs.Contains(InputVertex))
                {
                    HashSet<VertexArea> InVertices = new HashSet<VertexArea>();
                    HashSet<VertexArea> Inputs = new HashSet<VertexArea>(InputVertex.InVerticies);
                    foreach (VertexArea item in Inputs)
                    {

                        if (!ReverseArcs.Contains(item))
                        {
                            AreasTools.DisconnectVertices(item, InputVertex);
                            InVertices.Add(item);
                        }
                    }

                    //InputVertex.InVerticies = null;
                    HashSet<VertexArea> OutVertices = new HashSet<VertexArea>();
                    HashSet<VertexArea> Outputs = new HashSet<VertexArea>(InputVertex.OutVerticies);
                    foreach (VertexArea item in Outputs)
                    {
                        if (!item.Equals(InputVertex))
                        {
                            AreasTools.DisconnectVertices(InputVertex, item);
                            OutVertices.Add(item);
                        }
                    }


                    HashSet<VertexArea> GAVertices;
                    VertexArea GAInpudVertex;
                    AreaCycle areaCycle = new AreaCycle(AreasTools.NextAreaName(), InputVertex);
                    InputVertex.IsOut = true;
                    VertexArea vertexAreaCycle = new VertexArea(areaCycle);

                    foreach (VertexArea item in InVertices)
                    {
                        AreasTools.ConnectVertices(item, vertexAreaCycle);
                    }

                    foreach (VertexArea item in OutVertices)
                    {
                        if (!item.Equals(InputVertex))
                            AreasTools.ConnectVertices(vertexAreaCycle, item);
                    }

                    GAVertices = new HashSet<VertexArea>(this.SortedVerticies);
                    GAVertices.Remove(InputVertex);
                    GAVertices.Add(vertexAreaCycle);

                    GAInpudVertex = (this.InputVertex.Equals(InputVertex)) ? vertexAreaCycle : this.InputVertex;

                    GA = new GraphArea(GAVertices, GAInpudVertex);
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Пытается создать на основе текущего графа областей новый граф, объеденив несколько вершин в область тела, с входной вершиной InputVertex.
        /// Если получилось, то возвращает true и помещает результат в GA, иначе возвращает false.
        /// </summary>
        /// <param name="InputVertex">Предполагаемая входная вершина новой области тела</param>
        /// <param name="GA">Переманная в которую нужно записать новый граф</param>
        /// <returns>true, если всё вышеуказанное удалось, иначе false.</returns>
        private bool TryInBody(VertexArea InputVertex, out GraphArea GA)
        {
            GA = null;

            if ((!InputVertex.InVerticies.Any()) && (!InputVertex.OutVerticies.Any())) return false;

            //HashSet<VertexArea> ReverseArcs = GetReverseArcs(InputVertex);

            //if ((ReverseArcs.Count > 0) && (!ReverseArcs.Contains(InputVertex)))
            if (Heads.ContainsKey(InputVertex))
            {
                HashSet<VertexArea> ReverseArcs = Heads[InputVertex];
                if (!ReverseArcs.Contains(InputVertex))
                {
                    HashSet<VertexArea> GAVertices;
                    VertexArea GAInpudVertex;

                    HashSet<VertexArea> Body = GetBody(InputVertex, ReverseArcs);
                    if (Body == null)
                        return false;
                    else
                    {
                        HashSet<VertexArea> InVertices = new HashSet<VertexArea>();
                        HashSet<VertexArea> Inputs = new HashSet<VertexArea>(InputVertex.InVerticies);
                        foreach (VertexArea item in Inputs)
                        {

                            AreasTools.DisconnectVertices(item, InputVertex);
                            if (!ReverseArcs.Contains(item))
                            {
                                InVertices.Add(item);
                            }
                        }
                        //InputVertex.InVerticies = null;
                        HashSet<VertexArea> OutVertices = new HashSet<VertexArea>();
                        HashSet<VertexArea> Outputs = new HashSet<VertexArea>();
                        foreach (VertexArea reArc in ReverseArcs)
                        {
                            foreach (VertexArea item in reArc.OutVerticies)
                            {
                                AreasTools.DisconnectVertices(reArc, item);
                                if (!item.Equals(InputVertex))
                                    OutVertices.Add(item);
                            }
                        }
                        HashSet<VertexArea> OutInVert = new HashSet<VertexArea>(InputVertex.OutVerticies);
                        foreach (VertexArea item in OutInVert)
                        {
                            if (!Body.Contains(item))
                            {
                                AreasTools.DisconnectVertices(InputVertex, item);
                                OutVertices.Add(item);
                            }
                        }
                        
                        AreaBody areaBody = new AreaBody(AreasTools.NextAreaName(), new GraphArea(Body, InputVertex));
                        VertexArea vertexAreaBody = new VertexArea(areaBody);

                        foreach (VertexArea item in InVertices)
                        {
                            AreasTools.ConnectVertices(item, vertexAreaBody);
                        }

                        foreach (VertexArea item in OutVertices)
                        {
                            if (!item.Equals(InputVertex))
                                AreasTools.ConnectVertices(vertexAreaBody, item);
                        }


                        AreasTools.ConnectVertices(vertexAreaBody, vertexAreaBody);

                        GAVertices = new HashSet<VertexArea>(this.SortedVerticies);
                        GAVertices.ExceptWith(Body);
                        GAVertices.Remove(InputVertex);
                        GAVertices.Add(vertexAreaBody);

                        GAInpudVertex = (this.InputVertex.Equals(InputVertex)) ? vertexAreaBody : this.InputVertex;

                        GA = new GraphArea(GAVertices, GAInpudVertex);
                        return true;
                    }
                }
                else
                    return false;
            }
            else
                return false;

        }

        /// <summary>
        /// Возвращает список обратных дуг для данной вершины
        /// </summary>
        /// <param name="Vertex">Вершины</param>
        /// <returns>список обратных дуг</returns>
        HashSet<VertexArea> GetReverseArcs(VertexArea Vertex)
        {
            return new HashSet<VertexArea>(from V in Vertex.InVerticies where (arcsData.GetArcInfo(V, Vertex).Backward || V == Vertex) select V);
        }

        /// <summary>
        /// Возвращает Список вершин тела для предполагаемого цикла или null, если среди вершин тела встретится другой заголовок, отличный от предполагаемого заголовка Head
        /// </summary>
        /// <param name="Head">Предполагаемый заголовок</param>
        /// <param name="ReverseArcs">Список обратных дуг заголовка</param>
        /// <returns>Список вершин тела</returns>
        public HashSet<VertexArea> GetBody(VertexArea Head, HashSet<VertexArea> ReverseArcs)
        {
            HashSet<VertexArea> Body = new HashSet<VertexArea>(ReverseArcs);
            HashSet<VertexArea> Parents;
            foreach (VertexArea item in ReverseArcs)
            {
                Parents = GetParentTo(item, Head);
                if (Parents == null)
                    return null;
                else
                    Body.UnionWith(Parents);
            }
            foreach (VertexArea item in ReverseArcs)
                item.IsOut = true;
            if (Head.OutVerticies.Except(Body).Count() > 0)
            {
                Head.IsOut = true;
            }
            return Body;
        }

        /// <summary>
        /// Возвращает всех предков вершины This вплоть до Final, включительно, или null, если по пути встретится заголовок цикла отличный от Final
        /// </summary>
        /// <param name="This">Вершина</param>
        /// <param name="Final">Вершина</param>
        /// <returns>Список предков</returns>
        public HashSet<VertexArea> GetParentTo(VertexArea This, VertexArea Final)
        {
            HashSet<VertexArea> res = new HashSet<VertexArea>(This.InVerticies);
            HashSet<VertexArea> Parents;

            if (!This.InVerticies.Contains(Final))
            {
                foreach (VertexArea item in This.InVerticies)
                {
                    Parents = GetParentTo(item, Final);
                    if (Parents == null)
                        return null;
                    else
                        res.UnionWith(Parents);
                }
                if (res.Any(new Func<VertexArea, bool>(delegate(VertexArea va) { return Heads.ContainsKey(va) && (!va.Equals(Final)); })))
                    return null;
            }
            return res;
        }
    }
}
