﻿namespace OptPoohCompiler
{
    /// <summary>
    /// Оснавная ворма проекта
    /// </summary>
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSrcFile = new System.Windows.Forms.Button();
            this.textBoxSrcFileName = new System.Windows.Forms.TextBox();
            this.openFileDialogSrc = new System.Windows.Forms.OpenFileDialog();
            this.buttonCompile = new System.Windows.Forms.Button();
            this.textBoxCompilationResult = new System.Windows.Forms.TextBox();
            this.comboBoxParser = new System.Windows.Forms.ComboBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.labelLanguage = new System.Windows.Forms.Label();
            this.splitContainerMainStages = new System.Windows.Forms.SplitContainer();
            this.buttonApplyDFAAreaAlgo = new System.Windows.Forms.Button();
            this.listBoxDataFlowAreaAnalysis = new System.Windows.Forms.ListBox();
            this.labelDataFlowAreaAnalysisTitle = new System.Windows.Forms.Label();
            this.buttonDFST = new System.Windows.Forms.Button();
            this.buttonAnalyseArcs = new System.Windows.Forms.Button();
            this.labelCFGAnalysisTitle = new System.Windows.Forms.Label();
            this.buttonApplyDFAAlgo = new System.Windows.Forms.Button();
            this.listBoxDataFlowAnalysis = new System.Windows.Forms.ListBox();
            this.labelDataFlowAnalysisTitle = new System.Windows.Forms.Label();
            this.buttonApplyInBBOpts = new System.Windows.Forms.Button();
            this.listBoxOptimizations = new System.Windows.Forms.ListBox();
            this.labelOptimizationTitle = new System.Windows.Forms.Label();
            this.buttonDivideToBB = new System.Windows.Forms.Button();
            this.buttonGenerate3AC = new System.Windows.Forms.Button();
            this.buttonBuildTree = new System.Windows.Forms.Button();
            this.checkBoxClearForNew = new System.Windows.Forms.CheckBox();
            this.buttonCheckGraphReducible = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMainStages)).BeginInit();
            this.splitContainerMainStages.Panel1.SuspendLayout();
            this.splitContainerMainStages.Panel2.SuspendLayout();
            this.splitContainerMainStages.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSrcFile
            // 
            this.buttonSrcFile.Location = new System.Drawing.Point(12, 35);
            this.buttonSrcFile.Name = "buttonSrcFile";
            this.buttonSrcFile.Size = new System.Drawing.Size(111, 23);
            this.buttonSrcFile.TabIndex = 0;
            this.buttonSrcFile.Text = "Выбрать файл";
            this.buttonSrcFile.UseVisualStyleBackColor = true;
            this.buttonSrcFile.Click += new System.EventHandler(this.buttonSrcFile_Click);
            // 
            // textBoxSrcFileName
            // 
            this.textBoxSrcFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSrcFileName.Location = new System.Drawing.Point(142, 37);
            this.textBoxSrcFileName.Name = "textBoxSrcFileName";
            this.textBoxSrcFileName.ReadOnly = true;
            this.textBoxSrcFileName.Size = new System.Drawing.Size(762, 20);
            this.textBoxSrcFileName.TabIndex = 1;
            // 
            // buttonCompile
            // 
            this.buttonCompile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCompile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCompile.Location = new System.Drawing.Point(12, 3);
            this.buttonCompile.Name = "buttonCompile";
            this.buttonCompile.Size = new System.Drawing.Size(294, 24);
            this.buttonCompile.TabIndex = 2;
            this.buttonCompile.Text = "Компилировать";
            this.buttonCompile.UseVisualStyleBackColor = true;
            this.buttonCompile.Click += new System.EventHandler(this.buttonCompile_Click);
            // 
            // textBoxCompilationResult
            // 
            this.textBoxCompilationResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCompilationResult.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCompilationResult.Location = new System.Drawing.Point(3, 3);
            this.textBoxCompilationResult.Multiline = true;
            this.textBoxCompilationResult.Name = "textBoxCompilationResult";
            this.textBoxCompilationResult.ReadOnly = true;
            this.textBoxCompilationResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxCompilationResult.Size = new System.Drawing.Size(559, 644);
            this.textBoxCompilationResult.TabIndex = 4;
            // 
            // comboBoxParser
            // 
            this.comboBoxParser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxParser.FormattingEnabled = true;
            this.comboBoxParser.Location = new System.Drawing.Point(142, 8);
            this.comboBoxParser.Name = "comboBoxParser";
            this.comboBoxParser.Size = new System.Drawing.Size(156, 21);
            this.comboBoxParser.TabIndex = 5;
            this.comboBoxParser.SelectedIndexChanged += new System.EventHandler(this.comboBoxParser_SelectedIndexChanged);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.Location = new System.Drawing.Point(436, 653);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(126, 23);
            this.buttonClear.TabIndex = 6;
            this.buttonClear.Text = "Очистить";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // labelLanguage
            // 
            this.labelLanguage.AutoSize = true;
            this.labelLanguage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelLanguage.Location = new System.Drawing.Point(12, 9);
            this.labelLanguage.Name = "labelLanguage";
            this.labelLanguage.Size = new System.Drawing.Size(90, 15);
            this.labelLanguage.TabIndex = 7;
            this.labelLanguage.Text = "Входной язык:";
            // 
            // splitContainerMainStages
            // 
            this.splitContainerMainStages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerMainStages.Location = new System.Drawing.Point(0, 64);
            this.splitContainerMainStages.Name = "splitContainerMainStages";
            // 
            // splitContainerMainStages.Panel1
            // 
            this.splitContainerMainStages.Panel1.AutoScroll = true;
            this.splitContainerMainStages.Panel1.Controls.Add(this.buttonCheckGraphReducible);
            this.splitContainerMainStages.Panel1.Controls.Add(this.buttonApplyDFAAreaAlgo);
            this.splitContainerMainStages.Panel1.Controls.Add(this.listBoxDataFlowAreaAnalysis);
            this.splitContainerMainStages.Panel1.Controls.Add(this.labelDataFlowAreaAnalysisTitle);
            this.splitContainerMainStages.Panel1.Controls.Add(this.buttonDFST);
            this.splitContainerMainStages.Panel1.Controls.Add(this.buttonAnalyseArcs);
            this.splitContainerMainStages.Panel1.Controls.Add(this.labelCFGAnalysisTitle);
            this.splitContainerMainStages.Panel1.Controls.Add(this.buttonApplyDFAAlgo);
            this.splitContainerMainStages.Panel1.Controls.Add(this.listBoxDataFlowAnalysis);
            this.splitContainerMainStages.Panel1.Controls.Add(this.labelDataFlowAnalysisTitle);
            this.splitContainerMainStages.Panel1.Controls.Add(this.buttonApplyInBBOpts);
            this.splitContainerMainStages.Panel1.Controls.Add(this.listBoxOptimizations);
            this.splitContainerMainStages.Panel1.Controls.Add(this.labelOptimizationTitle);
            this.splitContainerMainStages.Panel1.Controls.Add(this.buttonDivideToBB);
            this.splitContainerMainStages.Panel1.Controls.Add(this.buttonGenerate3AC);
            this.splitContainerMainStages.Panel1.Controls.Add(this.buttonBuildTree);
            this.splitContainerMainStages.Panel1.Controls.Add(this.buttonCompile);
            this.splitContainerMainStages.Panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            // 
            // splitContainerMainStages.Panel2
            // 
            this.splitContainerMainStages.Panel2.Controls.Add(this.checkBoxClearForNew);
            this.splitContainerMainStages.Panel2.Controls.Add(this.textBoxCompilationResult);
            this.splitContainerMainStages.Panel2.Controls.Add(this.buttonClear);
            this.splitContainerMainStages.Size = new System.Drawing.Size(904, 679);
            this.splitContainerMainStages.SplitterDistance = 326;
            this.splitContainerMainStages.TabIndex = 8;
            // 
            // buttonApplyDFAAreaAlgo
            // 
            this.buttonApplyDFAAreaAlgo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApplyDFAAreaAlgo.Enabled = false;
            this.buttonApplyDFAAreaAlgo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonApplyDFAAreaAlgo.Location = new System.Drawing.Point(12, 643);
            this.buttonApplyDFAAreaAlgo.Name = "buttonApplyDFAAreaAlgo";
            this.buttonApplyDFAAreaAlgo.Size = new System.Drawing.Size(294, 24);
            this.buttonApplyDFAAreaAlgo.TabIndex = 17;
            this.buttonApplyDFAAreaAlgo.Text = "Выполнить анализ потоков данных";
            this.buttonApplyDFAAreaAlgo.UseVisualStyleBackColor = true;
            this.buttonApplyDFAAreaAlgo.Click += new System.EventHandler(this.buttonApplyDFAAreaAlgo_Click);
            // 
            // listBoxDataFlowAreaAnalysis
            // 
            this.listBoxDataFlowAreaAnalysis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxDataFlowAreaAnalysis.FormattingEnabled = true;
            this.listBoxDataFlowAreaAnalysis.HorizontalScrollbar = true;
            this.listBoxDataFlowAreaAnalysis.ItemHeight = 15;
            this.listBoxDataFlowAreaAnalysis.Location = new System.Drawing.Point(12, 558);
            this.listBoxDataFlowAreaAnalysis.Name = "listBoxDataFlowAreaAnalysis";
            this.listBoxDataFlowAreaAnalysis.ScrollAlwaysVisible = true;
            this.listBoxDataFlowAreaAnalysis.Size = new System.Drawing.Size(291, 79);
            this.listBoxDataFlowAreaAnalysis.TabIndex = 16;
            this.listBoxDataFlowAreaAnalysis.SelectedIndexChanged += new System.EventHandler(this.listBoxDataFlowAreaAnalysis_SelectedIndexChanged);
            // 
            // labelDataFlowAreaAnalysisTitle
            // 
            this.labelDataFlowAreaAnalysisTitle.AutoSize = true;
            this.labelDataFlowAreaAnalysisTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDataFlowAreaAnalysisTitle.Location = new System.Drawing.Point(12, 538);
            this.labelDataFlowAreaAnalysisTitle.Name = "labelDataFlowAreaAnalysisTitle";
            this.labelDataFlowAreaAnalysisTitle.Size = new System.Drawing.Size(266, 17);
            this.labelDataFlowAreaAnalysisTitle.TabIndex = 15;
            this.labelDataFlowAreaAnalysisTitle.Text = "Анализ потоков данных (по областям):";
            // 
            // buttonDFST
            // 
            this.buttonDFST.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDFST.Enabled = false;
            this.buttonDFST.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDFST.Location = new System.Drawing.Point(12, 439);
            this.buttonDFST.Name = "buttonDFST";
            this.buttonDFST.Size = new System.Drawing.Size(294, 24);
            this.buttonDFST.TabIndex = 14;
            this.buttonDFST.Text = "Глубинное остовное дерево";
            this.buttonDFST.UseVisualStyleBackColor = true;
            this.buttonDFST.Click += new System.EventHandler(this.buttonDFST_Click);
            // 
            // buttonAnalyseArcs
            // 
            this.buttonAnalyseArcs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAnalyseArcs.Enabled = false;
            this.buttonAnalyseArcs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAnalyseArcs.Location = new System.Drawing.Point(12, 469);
            this.buttonAnalyseArcs.Name = "buttonAnalyseArcs";
            this.buttonAnalyseArcs.Size = new System.Drawing.Size(294, 24);
            this.buttonAnalyseArcs.TabIndex = 13;
            this.buttonAnalyseArcs.Text = "Анализ дуг графа";
            this.buttonAnalyseArcs.UseVisualStyleBackColor = true;
            this.buttonAnalyseArcs.Click += new System.EventHandler(this.buttonAnalyseArcs_Click);
            // 
            // labelCFGAnalysisTitle
            // 
            this.labelCFGAnalysisTitle.AutoSize = true;
            this.labelCFGAnalysisTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCFGAnalysisTitle.Location = new System.Drawing.Point(12, 419);
            this.labelCFGAnalysisTitle.Name = "labelCFGAnalysisTitle";
            this.labelCFGAnalysisTitle.Size = new System.Drawing.Size(236, 17);
            this.labelCFGAnalysisTitle.TabIndex = 12;
            this.labelCFGAnalysisTitle.Text = "Анализ графа потока управления:";
            // 
            // buttonApplyDFAAlgo
            // 
            this.buttonApplyDFAAlgo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApplyDFAAlgo.Enabled = false;
            this.buttonApplyDFAAlgo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonApplyDFAAlgo.Location = new System.Drawing.Point(12, 381);
            this.buttonApplyDFAAlgo.Name = "buttonApplyDFAAlgo";
            this.buttonApplyDFAAlgo.Size = new System.Drawing.Size(294, 24);
            this.buttonApplyDFAAlgo.TabIndex = 11;
            this.buttonApplyDFAAlgo.Text = "Выполнить анализ потоков данных";
            this.buttonApplyDFAAlgo.UseVisualStyleBackColor = true;
            this.buttonApplyDFAAlgo.Click += new System.EventHandler(this.buttonApplyDFAAlgo_Click);
            // 
            // listBoxDataFlowAnalysis
            // 
            this.listBoxDataFlowAnalysis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxDataFlowAnalysis.FormattingEnabled = true;
            this.listBoxDataFlowAnalysis.HorizontalScrollbar = true;
            this.listBoxDataFlowAnalysis.ItemHeight = 15;
            this.listBoxDataFlowAnalysis.Location = new System.Drawing.Point(15, 296);
            this.listBoxDataFlowAnalysis.Name = "listBoxDataFlowAnalysis";
            this.listBoxDataFlowAnalysis.ScrollAlwaysVisible = true;
            this.listBoxDataFlowAnalysis.Size = new System.Drawing.Size(292, 79);
            this.listBoxDataFlowAnalysis.TabIndex = 10;
            this.listBoxDataFlowAnalysis.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxDataFlowAnalysis_MouseDoubleClick);
            // 
            // labelDataFlowAnalysisTitle
            // 
            this.labelDataFlowAnalysisTitle.AutoSize = true;
            this.labelDataFlowAnalysisTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDataFlowAnalysisTitle.Location = new System.Drawing.Point(12, 276);
            this.labelDataFlowAnalysisTitle.Name = "labelDataFlowAnalysisTitle";
            this.labelDataFlowAnalysisTitle.Size = new System.Drawing.Size(280, 17);
            this.labelDataFlowAnalysisTitle.TabIndex = 9;
            this.labelDataFlowAnalysisTitle.Text = "Анализ потоков данных (итерационный):";
            // 
            // buttonApplyInBBOpts
            // 
            this.buttonApplyInBBOpts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApplyInBBOpts.Enabled = false;
            this.buttonApplyInBBOpts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonApplyInBBOpts.Location = new System.Drawing.Point(12, 249);
            this.buttonApplyInBBOpts.Name = "buttonApplyInBBOpts";
            this.buttonApplyInBBOpts.Size = new System.Drawing.Size(294, 24);
            this.buttonApplyInBBOpts.TabIndex = 8;
            this.buttonApplyInBBOpts.Text = "Применить оптимизации внутри ББл";
            this.buttonApplyInBBOpts.UseVisualStyleBackColor = true;
            this.buttonApplyInBBOpts.Click += new System.EventHandler(this.buttonApplyInBBOpts_Click);
            // 
            // listBoxOptimizations
            // 
            this.listBoxOptimizations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxOptimizations.FormattingEnabled = true;
            this.listBoxOptimizations.HorizontalScrollbar = true;
            this.listBoxOptimizations.ItemHeight = 15;
            this.listBoxOptimizations.Location = new System.Drawing.Point(15, 164);
            this.listBoxOptimizations.Name = "listBoxOptimizations";
            this.listBoxOptimizations.ScrollAlwaysVisible = true;
            this.listBoxOptimizations.Size = new System.Drawing.Size(291, 79);
            this.listBoxOptimizations.TabIndex = 7;
            this.listBoxOptimizations.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxOptimizations_MouseDoubleClick);
            // 
            // labelOptimizationTitle
            // 
            this.labelOptimizationTitle.AutoSize = true;
            this.labelOptimizationTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelOptimizationTitle.Location = new System.Drawing.Point(12, 143);
            this.labelOptimizationTitle.Name = "labelOptimizationTitle";
            this.labelOptimizationTitle.Size = new System.Drawing.Size(177, 17);
            this.labelOptimizationTitle.TabIndex = 6;
            this.labelOptimizationTitle.Text = "Локальные оптимизации:";
            // 
            // buttonDivideToBB
            // 
            this.buttonDivideToBB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDivideToBB.Enabled = false;
            this.buttonDivideToBB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDivideToBB.Location = new System.Drawing.Point(12, 104);
            this.buttonDivideToBB.Name = "buttonDivideToBB";
            this.buttonDivideToBB.Size = new System.Drawing.Size(294, 24);
            this.buttonDivideToBB.TabIndex = 5;
            this.buttonDivideToBB.Text = "Разбить на ББи";
            this.buttonDivideToBB.UseVisualStyleBackColor = true;
            this.buttonDivideToBB.Click += new System.EventHandler(this.buttonDivideToBB_Click);
            // 
            // buttonGenerate3AC
            // 
            this.buttonGenerate3AC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGenerate3AC.Enabled = false;
            this.buttonGenerate3AC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonGenerate3AC.Location = new System.Drawing.Point(12, 74);
            this.buttonGenerate3AC.Name = "buttonGenerate3AC";
            this.buttonGenerate3AC.Size = new System.Drawing.Size(294, 24);
            this.buttonGenerate3AC.TabIndex = 4;
            this.buttonGenerate3AC.Text = "Генерировать 3AC";
            this.buttonGenerate3AC.UseVisualStyleBackColor = true;
            this.buttonGenerate3AC.Click += new System.EventHandler(this.buttonGenerate3AC_Click);
            // 
            // buttonBuildTree
            // 
            this.buttonBuildTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBuildTree.Enabled = false;
            this.buttonBuildTree.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonBuildTree.Location = new System.Drawing.Point(12, 44);
            this.buttonBuildTree.Name = "buttonBuildTree";
            this.buttonBuildTree.Size = new System.Drawing.Size(294, 24);
            this.buttonBuildTree.TabIndex = 3;
            this.buttonBuildTree.Text = "Построить дерево";
            this.buttonBuildTree.UseVisualStyleBackColor = true;
            this.buttonBuildTree.Click += new System.EventHandler(this.buttonBuildTree_Click);
            // 
            // checkBoxClearForNew
            // 
            this.checkBoxClearForNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBoxClearForNew.AutoSize = true;
            this.checkBoxClearForNew.Checked = true;
            this.checkBoxClearForNew.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxClearForNew.Location = new System.Drawing.Point(4, 653);
            this.checkBoxClearForNew.Name = "checkBoxClearForNew";
            this.checkBoxClearForNew.Size = new System.Drawing.Size(206, 17);
            this.checkBoxClearForNew.TabIndex = 7;
            this.checkBoxClearForNew.Text = "Очищать при выборе нового файла";
            this.checkBoxClearForNew.UseVisualStyleBackColor = true;
            // 
            // buttonCheckGraphReducible
            // 
            this.buttonCheckGraphReducible.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCheckGraphReducible.Enabled = false;
            this.buttonCheckGraphReducible.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCheckGraphReducible.Location = new System.Drawing.Point(13, 499);
            this.buttonCheckGraphReducible.Name = "buttonCheckGraphReducible";
            this.buttonCheckGraphReducible.Size = new System.Drawing.Size(294, 24);
            this.buttonCheckGraphReducible.TabIndex = 18;
            this.buttonCheckGraphReducible.Text = "Проверка приводимости графа";
            this.buttonCheckGraphReducible.UseVisualStyleBackColor = true;
            this.buttonCheckGraphReducible.Click += new System.EventHandler(this.buttonCheckGraphReducible_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 743);
            this.Controls.Add(this.splitContainerMainStages);
            this.Controls.Add(this.labelLanguage);
            this.Controls.Add(this.comboBoxParser);
            this.Controls.Add(this.textBoxSrcFileName);
            this.Controls.Add(this.buttonSrcFile);
            this.Name = "MainForm";
            this.Text = "OptPooh оптимизирующий компилятор";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainerMainStages.Panel1.ResumeLayout(false);
            this.splitContainerMainStages.Panel1.PerformLayout();
            this.splitContainerMainStages.Panel2.ResumeLayout(false);
            this.splitContainerMainStages.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMainStages)).EndInit();
            this.splitContainerMainStages.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSrcFile;
        private System.Windows.Forms.TextBox textBoxSrcFileName;
        private System.Windows.Forms.OpenFileDialog openFileDialogSrc;
        private System.Windows.Forms.Button buttonCompile;
        private System.Windows.Forms.TextBox textBoxCompilationResult;
        private System.Windows.Forms.ComboBox comboBoxParser;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label labelLanguage;
        private System.Windows.Forms.SplitContainer splitContainerMainStages;
        private System.Windows.Forms.Button buttonGenerate3AC;
        private System.Windows.Forms.Button buttonBuildTree;
        private System.Windows.Forms.Button buttonDivideToBB;
        private System.Windows.Forms.ListBox listBoxOptimizations;
        private System.Windows.Forms.Label labelOptimizationTitle;
        private System.Windows.Forms.Button buttonApplyInBBOpts;
        private System.Windows.Forms.Button buttonApplyDFAAlgo;
        private System.Windows.Forms.ListBox listBoxDataFlowAnalysis;
        private System.Windows.Forms.Label labelDataFlowAnalysisTitle;
        private System.Windows.Forms.CheckBox checkBoxClearForNew;
        private System.Windows.Forms.Button buttonAnalyseArcs;
        private System.Windows.Forms.Label labelCFGAnalysisTitle;
        private System.Windows.Forms.Button buttonDFST;
        private System.Windows.Forms.Button buttonApplyDFAAreaAlgo;
        private System.Windows.Forms.ListBox listBoxDataFlowAreaAnalysis;
        private System.Windows.Forms.Label labelDataFlowAreaAnalysisTitle;
        private System.Windows.Forms.Button buttonCheckGraphReducible;
    }
}

