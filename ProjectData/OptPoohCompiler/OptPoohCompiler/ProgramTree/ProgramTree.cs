﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OptPoohCompiler.Addr3Code;

namespace OptPoohCompiler.ProgramTree
{

    // *************************************************************
    // Основные блоки
    // *************************************************************

    /// <summary>
    /// Базовый класс для всех узлов
    /// </summary>
    public class Node 
    {
        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Список трехадресных команд</returns>
        public virtual LinkedList<Addr3Command> GetAddr3Code()
        {
            return new LinkedList<Addr3Command>();
        }
    }

    /// <summary>
    /// Базовый класс для всех операторов
    /// </summary>
    public class StatementNode : Node
    {
    }

    /// <summary>
    /// Блок операторов
    /// </summary>
    public class BlockNode : StatementNode
    {
        /// <summary>
        /// Список операторов блока
        /// </summary>
        public List<StatementNode> StatementList { get; set; }

        /// <summary>
        /// Блок операторов
        /// </summary>
        public BlockNode()
        {
            StatementList = new List<StatementNode>();
        }
        /// <summary>
        /// Блок операторов
        /// </summary>
        /// <param name="statement">Первый оператор блока</param>
        public BlockNode(StatementNode statement)
        {
            StatementList = new List<StatementNode>();
            StatementList.Add(statement);
        }

        /// <summary>
        /// Добавляет оператор в блок
        /// </summary>
        /// <param name="statement">Оператор</param>
        public void Add(StatementNode statement)
        {
            StatementList.Add(statement);
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Список трехадресных команд</returns>
        public override LinkedList<Addr3Command> GetAddr3Code()
        {
            // TODO
            // пока просто склеивает код, генерируемый операторами блока
            // нужно хоть что-то для тестирования
            LinkedList<Addr3Command> code = new LinkedList<Addr3Command>();
            foreach (StatementNode stmnt in StatementList)
            {
                Addr3CodeGenTools.MergeLinkedLists(code, stmnt.GetAddr3Code());
                //code = code.Concat(stmnt.GetAddr3Code()) as LinkedList<Addr3Command>;
            }
            return code;
        }
    }

    /// <summary>
    /// Программа
    /// </summary>
    public class ProgrNode : BlockNode
    {
        /// <summary>
        /// Тело программы
        /// </summary>
        public BlockNode Body { get; set; }

        /// <summary>
        /// Программа
        /// </summary>
        /// <param name="body">Тело программы</param>
        public ProgrNode(BlockNode body)
        {
            Body = body;
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Список трехадресных команд</returns>
        public override LinkedList<Addr3Command> GetAddr3Code()
        {
            return Body.GetAddr3Code();
        }
    }

    /// <summary>
    /// Базовый класс для всех выражений
    /// </summary>
    public class ExprNode : Node
    {
        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Структуру (список команд + имя, хранящее выражение)</returns>
        public virtual Addr3CodeExprData GetAddr3CodeData()
        {
            throw new InvalidOperationException("Этот метод должен быть перегружен потомками");
            //Addr3CodeExprData data = new Addr3CodeExprData(null, "");
            //return data;
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Список трехадресных команд</returns>
        public override LinkedList<Addr3Command> GetAddr3Code()
        {
            return GetAddr3CodeData().code;
        }
    }

    // *************************************************************
    // Выражения
    // *************************************************************

    /// <summary>
    /// Унарный арифметический оператор
    /// </summary>
    public enum UnaryArithmOperator { UPLUS, UMINUS };
    /// <summary>
    /// Бинарный арифметический оператор сложения
    /// </summary>
    public enum ArithmPlusOperator { PLUS, MINUS };
    /// <summary>
    ///  Бинарный арифметический оператор умножения
    /// </summary>
    public enum ArithmMultOperator { MULT, DIVIDE };
    /// <summary>
    /// Отношение
    /// </summary>
    public enum Relation { EQUAL, NOT_EQUAL, LESS, LESS_EQUAL, GREATER, GREATER_EQUAL };

    /// <summary>
    /// Базовый класс для всех арифметических выражений
    /// </summary>
    public class ArithmExprNode : ExprNode
    {
    }

    /// <summary>
    /// Арифметическое выражение-идентификатор
    /// </summary>
    public class IdNode : ArithmExprNode
    {
        ///  Имя идентификатора
        public string Name { get; set; }

        /// <summary>
        /// Арифметическое выражение-идентификатор
        /// </summary>
        /// <param name="name">Имя идентификатора</param>
        public IdNode(string name) { Name = name; }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Структуру (список команд + имя, хранящее выражение)</returns>
        public override Addr3CodeExprData GetAddr3CodeData()
        {
            return new Addr3CodeExprData(
                new LinkedList<Addr3Command>(),     // пустой список команд
                this.Name                           // имя идентификатора
            );
        }
    }

    /// <summary>
    /// Арифметическое выражение-число (целое)
    /// </summary>
    public class NumNode : ArithmExprNode
    {
        /// Целое зачение
        public int Num { get; set; }

        /// <summary>
        /// Арифметическое выражение-число (целое)
        /// </summary>
        /// <param name="num">Целое зачение</param>
        public NumNode(int num) { Num = num; }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Структуру (список команд + имя, хранящее выражение)</returns>
        public override Addr3CodeExprData GetAddr3CodeData()
        {
            return new Addr3CodeExprData(
                new LinkedList<Addr3Command>(),     // пустой список команд
                this.Num.ToString()                 // строковое представление числа
            );
        }
    }

    /// <summary>
    /// Арифметическое выражение со знаком
    /// </summary>
    public class SignedArithmExprNode : ArithmExprNode
    {
        /// Знак выражние
        public UnaryArithmOperator Sign { get; set; }
        /// Выражение под знаком
        public ArithmExprNode Expr { get; set; }

        /// <summary>
        /// Арифметическое выражение со знаком
        /// </summary>
        /// <param name="num">Выражение под знаком</param>
        public SignedArithmExprNode(UnaryArithmOperator sign, ArithmExprNode expr)
        {
            Expr = expr;
            Sign = sign;
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Структуру (список команд + имя, хранящее выражение)</returns>
        public override Addr3CodeExprData GetAddr3CodeData()
        {
            // получаем код и имя подвыражения
            Addr3CodeExprData subData = this.Expr.GetAddr3CodeData();
            LinkedList<Addr3Command> code = subData.code;
            // пусть t[n] -- имя выражения, хранящегося в Expr
            // нужно добавить трехадресную команду
            // t[new] = <Sign> t[n]
            string currHolder = Addr3CodeGenTools.NextTempVarName();
            Addr3Command unOpCmd = new Addr3Command(currHolder, subData.holder,
                Addr3CodeGenTools.GetUnaryOperator(this.Sign));
            code.AddLast(unOpCmd);
            // возвращаем данные для текущего узла
            return new Addr3CodeExprData(
                code,                       // новый список команд
                currHolder                  // имя текущего выражения
            );
        }
    }

    /// <summary>
    /// Бинарное арифметическое выражение
    /// </summary>
    public class BinArithmExprNode : ArithmExprNode
    {
        /// Арифметическое выражение-левый аргумент
        public ArithmExprNode LeftArg { get; set; }
        /// Арифметическое выражение-правый аргумент
        public ArithmExprNode RightArg { get; set; }

        /// <summary>
        /// Бинарное выражение
        /// </summary>
        /// <param name="leftArg">Арифметическое выражение-левый аргумент</param>
        /// <param name="rightArg">Арифметическое выражение-правый аргумент</param>
        public BinArithmExprNode(ArithmExprNode leftArg, ArithmExprNode rightArg)
        {
            LeftArg = leftArg;
            RightArg = rightArg;
        }
    }

    /// <summary>
    /// Бинарное арифметическое выражение сложения
    /// </summary>
    public class BinArithmPlusExprNode : BinArithmExprNode
    {
        /// Арифметический оператор
        public ArithmPlusOperator Operator { get; set; }

        /// <summary>
        /// Бинарное арифметическое выражение сложения
        /// </summary>
        /// <param name="op">Арифметический оператор сложения</param>
        /// <param name="leftArg">Арифметическое выражение-левый аргумент</param>
        /// <param name="rightArg">Арифметическое выражение-правый аргумент</param>
        public BinArithmPlusExprNode(ArithmPlusOperator op, ArithmExprNode leftArg, ArithmExprNode rightArg)
            : base(leftArg, rightArg)
        {
            Operator = op;
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Структуру (список команд + имя, хранящее выражение)</returns>
        public override Addr3CodeExprData GetAddr3CodeData()
        {
            LinkedList<Addr3Command> code;
            string currHolder;
            Addr3CodeGenTools.PrepareBinExprAddr3Code(this.LeftArg, this.RightArg,
                Addr3CodeGenTools.GetBinaryPlusOperator(this.Operator), out code, out currHolder);
            // возвращаем данные для текущего узла
            return new Addr3CodeExprData(
                code,                       // новый список команд
                currHolder                  // имя текущего выражения
            );
        }
    }

    /// <summary>
    /// Бинарное арифметическое выражение умножения
    /// </summary>
    public class BinArithmMultExprNode : BinArithmExprNode
    {
        /// Арифметический оператор
        public ArithmMultOperator Operator { get; set; }

        /// <summary>
        /// Бинарное арифметическое выражение умножения
        /// </summary>
        /// <param name="op">Арифметический операторумножения </param>
        /// <param name="leftArg">Арифметическое выражение-левый аргумент</param>
        /// <param name="rightArg">Арифметическое выражение-правый аргумент</param>
        public BinArithmMultExprNode(ArithmMultOperator op, ArithmExprNode leftArg, ArithmExprNode rightArg)
            : base(leftArg, rightArg)
        {
            Operator = op;
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Структуру (список команд + имя, хранящее выражение)</returns>
        public override Addr3CodeExprData GetAddr3CodeData()
        {
            LinkedList<Addr3Command> code;
            string currHolder;
            Addr3CodeGenTools.PrepareBinExprAddr3Code(this.LeftArg, this.RightArg,
                Addr3CodeGenTools.GetBinaryMultOperator(this.Operator), out code, out currHolder);
            // возвращаем данные для текущего узла
            return new Addr3CodeExprData(
                code,                       // новый список команд
                currHolder                  // имя текущего выражения
            );
        }
    }

    /// <summary>
    /// Бинарное логическое выражение
    /// </summary>
    public class BinLogicExprNode : ExprNode
    {
        /// Оператор отношения
        public Relation Operator { get; set; }
        /// Арифметическое выражение-левый аргумент
        public ArithmExprNode LeftArg { get; set; }
        /// Арифметическое выражение-правый аргумент
        public ArithmExprNode RightArg { get; set; }

        /// <summary>
        /// Бинарное логическое выражение
        /// </summary>
        /// <param name="op">Оператор отношения</param>
        /// <param name="leftArg">Арифметическое выражение-левый аргумент</param>
        /// <param name="rightArg">Арифметическое выражение-правый аргумент</param>
        public BinLogicExprNode(Relation op, ArithmExprNode leftArg, ArithmExprNode rightArg)
        {
            LeftArg = leftArg;
            RightArg = rightArg;
            Operator = op;
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Структуру (список команд + имя, хранящее выражение)</returns>
        public override Addr3CodeExprData GetAddr3CodeData()
        {
            LinkedList<Addr3Command> code;
            string currHolder;
            Addr3CodeGenTools.PrepareBinExprAddr3Code(this.LeftArg, this.RightArg,
                Addr3CodeGenTools.GetRelationOperator(this.Operator), out code, out currHolder);
            // возвращаем данные для текущего узла
            return new Addr3CodeExprData(
                code,                       // новый список команд
                currHolder                  // имя текущего выражения
            );
        }
    }



    // *************************************************************
    // Операторы
    // *************************************************************

    /// <summary>
    /// Оператор присваивания
    /// </summary>
    public class AssignNode : StatementNode
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public IdNode Id { get; set; }
        /// <summary>
        /// Выражение в правой части
        /// </summary>
        public ArithmExprNode Expr { get; set; }

        /// <summary>
        /// Оператор присваивания
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="expr">Выражение в правой части </param>
        public AssignNode(IdNode id, ArithmExprNode expr)
        {
            Id = id;
            Expr = expr;
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Список трехадресных команд</returns>
        public override LinkedList<Addr3Command> GetAddr3Code()
        {
            // получаем код и имя выражения справа
            Addr3CodeExprData subData = this.Expr.GetAddr3CodeData();
            LinkedList<Addr3Command> code = subData.code;
            // пусть t[n] -- имя выражения, хранящегося в Expr
            // нужно добавить трехадресную команду
            // id = t[n]
            Addr3Command assignCmd = new Addr3Command(Id.Name, subData.holder);
            code.AddLast(assignCmd);
            // возвращаем данные для текущего узла
            return code;
        }
    }

    /// <summary>
    /// Оператор цикла
    /// </summary>
    public class CycleNode : StatementNode
    {
        /// <summary>
        /// Условие цикла
        /// </summary>
        public ExprNode Expr { get; set; }
        /// <summary>
        /// Оператор
        /// </summary>
        public StatementNode Stat { get; set; }
        /// <summary>
        /// Оператор цикла
        /// </summary>
        /// <param name="expr">Условие цикла</param>
        /// <param name="stat">Оператор</param>
        public CycleNode(ExprNode expr, StatementNode stat)
        {
            Expr = expr;
            Stat = stat;
        }
    }

    /// <summary>
    /// Оператор цикла while
    /// </summary>
    public class WhileNode : StatementNode
    {
        /// <summary>
        /// Условие цикла
        /// </summary>
        public BinLogicExprNode Expr { get; set; }
        /// <summary>
        /// Оператор
        /// </summary>
        public StatementNode Stat { get; set; }
        /// <summary>
        /// Оператор цикла while
        /// </summary>
        /// <param name="expr">Условие цикла</param>
        /// <param name="stat">Оператор</param>
        public WhileNode(BinLogicExprNode expr, StatementNode stat)
        {
            Expr = expr;
            Stat = stat;
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Список трехадресных команд</returns>
        public override LinkedList<Addr3Command> GetAddr3Code()
        {
            var exprCode = Expr.GetAddr3Code(); //трехадресный код уловия
            var condVar = exprCode.Last.Value.result;   //переменная с результатом

            //через вычленение->копирование->изменение->удаление->добавление меняем метку
            var _v1 = exprCode.First.Value; 
            if (!_v1.HasLabel())
                _v1.cmdLabel = Addr3CodeGenTools.NextLabelName(); //l1
            exprCode.RemoveFirst();
            exprCode.AddFirst(_v1);
            var lblToExpr = exprCode.First.Value.cmdLabel;      //запоминаем её  //l1

            var lblToIf = Addr3CodeGenTools.NextLabelName();    //метка для IF //l2
            var lblToEnd = Addr3CodeGenTools.NextLabelName();   //придумываем метку для перехода к след. блоку  //l3
            
            Addr3Command ifCom = new Addr3Command(true, condVar, lblToIf); //делаем код для if

            //собираем все вместе
            var code = exprCode;
            code.AddLast(ifCom);
            code.AddLast(new Addr3Command(true, lblToEnd));  //добавляем код GOTO

            var statCode = Stat.GetAddr3Code();
            var _v2 = statCode.First.Value;
            _v2.cmdLabel = lblToIf;  //l2
            statCode.RemoveFirst();
            statCode.AddFirst(_v2);

            Addr3CodeGenTools.MergeLinkedLists(code, statCode);
            code.AddLast(new Addr3Command(true, lblToExpr));
            code.AddLast(new Addr3Command(lblToEnd, Addr3CmdType.NOP, "","","", Addr3CmdOperator.EMPTY));   //добавляем метку вконец

            return code;
        }
    }
	
	/// <summary>
    /// Условный оператор
    /// </summary>
    public class IfNode : StatementNode
    {
        /// <summary>
        /// Условие
        /// </summary>
        public BinLogicExprNode Condition { get; set; }

        /// <summary>
        /// Оператор ветки тру
        /// </summary>
        public StatementNode ThenStatement { get; set; }

        /// <summary>
        /// Оператор ветки фолс
        /// </summary>
        public StatementNode ElseStatement { get; set; }

        /// <summary>
        /// Условный оператор
        /// </summary>
        /// <param name="Condition">Условие</param>
        /// <param name="ThenStatement">false</param>
        /// <param name="ElseStatement">true</param>
        public IfNode(BinLogicExprNode Condition, StatementNode ThenStatement, StatementNode ElseStatement)
        {
            this.Condition = Condition;
            this.ThenStatement = ThenStatement;
            this.ElseStatement = ElseStatement;
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Список трехадресных команд</returns>
        public override LinkedList<Addr3Command> GetAddr3Code()
        {
            if (this.ElseStatement != null)
            {
                // получаем код и имя выражения справа
                var condCode = Condition.GetAddr3Code();
                var cond = condCode.Last.Value.result;
                var thnStmnt3AddrCd = ThenStatement.GetAddr3Code();
                var elsStmnt3AddrCd = ElseStatement.GetAddr3Code();

                var _vl = thnStmnt3AddrCd.First();
                if (!_vl.HasLabel())
                    _vl.cmdLabel = Addr3CodeGenTools.NextLabelName();

                thnStmnt3AddrCd.RemoveFirst();
                thnStmnt3AddrCd.AddFirst(_vl);
                string thnStnmtLabel = thnStmnt3AddrCd.First.Value.cmdLabel; //!!!! может не быть First

                string endOpLabel = Addr3CodeGenTools.NextLabelName();

                Addr3Command endEmptyCommand = new Addr3Command(endOpLabel, Addr3CmdType.NOP, "", "", "", Addr3CmdOperator.EMPTY);

                //вычисление условия
                LinkedList<Addr3Command> code = new LinkedList<Addr3Command>();
                Addr3CodeGenTools.MergeLinkedLists(code, condCode); //добавление
                //построение трехадресного if`a
                Addr3Command ifCom = new Addr3Command(true, cond, thnStnmtLabel);
                code.AddLast(ifCom);    //добавление
                //добавление then
                Addr3CodeGenTools.MergeLinkedLists(code, elsStmnt3AddrCd);
                //добавление GOTO на выход из if`а
                Addr3Command gtCom = new Addr3Command(true, endOpLabel);
                code.AddLast(gtCom);                                       //!!!! куда выходить из then?
                //добавление else
                Addr3CodeGenTools.MergeLinkedLists(code, thnStmnt3AddrCd);
                code.AddLast(endEmptyCommand);
                return code;
            }
            else
            {
                // получаем код и имя выражения справа
                var condCode = Condition.GetAddr3Code();
                var cond = condCode.Last.Value.result;

                var thnStmnt3AddrCd = ThenStatement.GetAddr3Code();

                var _vl = thnStmnt3AddrCd.First();
                if (!_vl.HasLabel())
                    _vl.cmdLabel = Addr3CodeGenTools.NextLabelName();

                thnStmnt3AddrCd.RemoveFirst();
                thnStmnt3AddrCd.AddFirst(_vl);

                string thnStnmtLabel = thnStmnt3AddrCd.First.Value.cmdLabel;

                string endOpLabel = Addr3CodeGenTools.NextLabelName();

                Addr3Command endEmptyCommand = new Addr3Command(endOpLabel, Addr3CmdType.NOP, "", "", "", Addr3CmdOperator.EMPTY);

                //добавление GOTO на выход из if`а
                Addr3Command gtCom = new Addr3Command(true, endOpLabel);

                //вычисление условия
                LinkedList<Addr3Command> code = new LinkedList<Addr3Command>();
                Addr3CodeGenTools.MergeLinkedLists(code, condCode); //добавление
                Addr3Command ifCom = new Addr3Command(true, cond, thnStnmtLabel);
                code.AddLast(ifCom);    //добавление
                code.AddLast(gtCom);
                Addr3CodeGenTools.MergeLinkedLists(code, thnStmnt3AddrCd);
                code.AddLast(endEmptyCommand);
                return code;
            }

        }
    }

    /// <summary>
    /// Цикл с параметром
    /// </summary>
    public class ForNode : StatementNode
    {
        /// <summary>
        /// Начальное значение
        /// </summary>
        public ArithmExprNode Start;

        /// <summary>
        /// Конечное значение
        /// </summary>
        public ArithmExprNode End;

        /// <summary>
        /// Счетчик
        /// </summary>
        public IdNode Counter { get; set; }

        /// <summary>
        /// Тип цикла
        /// </summary>
        public enum ForCicleType { To, DownTo };

        ForCicleType CicleType;

        /// <summary>
        /// Оператор 
        /// </summary>
        public StatementNode Statement { get; set; }
        
        /// <summary>
        /// Цикл с параметром
        /// </summary>
        /// <param name="Start">А</param>
        /// <param name="End">B</param>
        /// <param name="Counter">i</param>
        /// <param name="Statement">Оператор</param>
        /// <param name="CicleType">Тип цикла</param>
        public ForNode(ArithmExprNode Start, ArithmExprNode End, IdNode Counter, StatementNode Statement, ForCicleType CicleType)
        {
            this.Start = Start;
            this.End = End;
            this.Counter = Counter;
            this.Statement = Statement;
            this.CicleType = CicleType;
        }

        /// <summary>
        /// Генерирует список трехадресных команд, соответствующих
        /// данному узлу синтаксического дерева
        /// </summary>
        /// <returns>Список трехадресных команд</returns>
        public override LinkedList<Addr3Command> GetAddr3Code()
        {
            BlockNode b = new BlockNode(Statement);
            //if (CicleType == ForCicleType.To)
            b.Add(new ProgramTree.AssignNode(Counter,new BinArithmPlusExprNode(
                CicleType == ForCicleType.To ? ArithmPlusOperator.PLUS : ArithmPlusOperator.MINUS, Counter, new NumNode(1))));
            /*else
                b.Add(new ProgramTree.AssignNode(Counter, new BinArithmPlusExprNode(ArithmPlusOperator.MINUS, Counter, new NumNode(1))));
            */
            WhileNode wn = new WhileNode(new BinLogicExprNode(CicleType == ForCicleType.To? Relation.LESS: Relation.GREATER, Counter, End), b);
            AssignNode firstCom = new AssignNode(Counter, Start);
            var code = firstCom.GetAddr3Code();
            Addr3CodeGenTools.MergeLinkedLists(code, wn.GetAddr3Code());

            return code;
        }
    }

}
