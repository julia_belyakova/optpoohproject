﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.GraphTools;

namespace OptPoohCompiler.ControlFlowGraphAnalysis.NaturalLoops
{
    /// <summary>
    /// Естественный цикл
    /// </summary>
    /// <typeparam name="Data">Тип данных в мета-вершине</typeparam>
    /// <typeparam name="Vertex">Тип мета-вершины</typeparam>
    public class NaturalLoop<Data, Vertex>
        where Vertex : IVertex<Vertex, Data>
    {
        /// <summary>
        /// Заголовок цикла
        /// </summary>
        private Vertex header;

        /// <summary>
        /// Вершины цикла
        /// </summary>
        private HashSet<Vertex> vertices;

        /// <summary>
        /// Заголовок цикла
        /// </summary>
        public Vertex Header
        {
            get { return header; }
        }

        /// <summary>
        /// Вершины цикла
        /// </summary>
        public HashSet<Vertex> Vertices
        {
            get { return vertices; }
        }

        /// <summary>
        /// Естественный цикл, состоящий из одного заголовка header
        /// </summary>
        /// <param name="header">Узел графа -- заголовок цикла</param>
        public NaturalLoop(Vertex header)
        {
            if (header == null)
                throw new ArgumentNullException("Заголовок цикла не может быть Null");
            this.header = header;
            this.vertices = new HashSet<Vertex>();
            this.vertices.Add(header);
        }

        /// <summary>
        /// Добавляет вершину в цикл
        /// </summary>
        /// <param name="loopVertex">Вершина цикла</param>
        public void AddVertex(Vertex loopVertex)
        {
            if (loopVertex == null)
                throw new ArgumentNullException("Вершина цикла не может быть Null");
            this.vertices.Add(loopVertex);
        }
    }
}
