﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.GraphTools;
using OptPoohCompiler.ControlFlowGraphAnalysis;

namespace OptPoohCompiler.ControlFlowGraphAnalysis.NaturalLoops
{
    /// <summary>
    /// Статический класс с инструментами поиска
    /// естественных циклов
    /// </summary>
    public static class NaturalLoopsTools
    {
        /// <summary>
        /// Нахдит все внутренние естественные циклы приводимомго графа cfg
        /// </summary>
        /// <typeparam name="Data">Тип данных в мета-вершине</typeparam>
        /// <typeparam name="Vertex">Тип мета-вершины</typeparam>
        /// <typeparam name="Graph">Тип графа</typeparam>
        /// <param name="cfg">Приводимый граф</param>
        /// <returns>Множество внутренних естесвенных циклов</returns>
        public static HashSet<NaturalLoop<Data, Vertex>> GetInnerNaturalLoops<Data, Vertex, Graph>(Graph cfg)
            where Vertex : class, IVertex<Vertex, Data>
            where Graph : IGraph<Vertex, Data>
        {
            // работаем только с приводимым графом
            if (!CFGAnalysisTools.IsReducible<Data, Vertex, Graph>(cfg))
                throw new ArgumentException("Естественные циклы нельзя искать в неприводимом графе");

            // словарь из заголовков в циклы
            // здесь будут только самые внутренние циклы, внешние отбрасываем
            Dictionary<Vertex, NaturalLoop<Data, Vertex>> loops = 
                new Dictionary<Vertex, NaturalLoop<Data, Vertex>>();
            DFSTAndArcsCFGData<Data, Vertex, Graph> arcsInfo = new DFSTAndArcsCFGData<Data, Vertex, Graph>(cfg);
            // Идём по всем дугам графа и находим обратные
            foreach (Vertex source in cfg.Verticies)
                foreach (Vertex dest in source.OutVerticies)
                {
                    if (!arcsInfo.GetArcInfo(source, dest).Backward && source != dest)
                        continue;
                    // работаем с обратной дугой source -> dest
                    // находим естественный цикл
                    NaturalLoop<Data, Vertex> loop = GetNaturalLoop<Data, Vertex, Graph>(cfg, source, dest);
                    // если какой-то из найденных циклов строго включает текущий, то его нужно заменить
                    var superLoopPair = loops.FirstOrDefault(pair => pair.Value.Vertices.IsProperSupersetOf(loop.Vertices));
                    // нашли такой элемент
                    if (superLoopPair.Key != null)
                    {
                        loops.Remove(superLoopPair.Key);
                        loops.Add(loop.Header, loop);
                    }
                    // если какой-то из найденных циклов является внутренним для текущего, то текущий отбрасываем
                    else
                    {
                        var subLoopPair = loops.FirstOrDefault(pair => pair.Value.Vertices.IsProperSubsetOf(loop.Vertices));
                        // не нашли такого
                        if (subLoopPair.Key == null)
                        { 
                            // если цикл с таким заголовком уже есть, то нужно их слить,
                            // иначе просто добавляем новый цикл
                            if (loops.ContainsKey(loop.Header))
                                loops[loop.Header].Vertices.UnionWith(loop.Vertices);
                            else
                                loops.Add(loop.Header, loop);
                        }
                    }
                }
            return new HashSet<NaturalLoop<Data, Vertex>>(loops.Values);
        }

        /// <summary>
        /// Ищет естественный цикл, соответствующий обратной дуге
        /// backSource -> backDest
        /// </summary>
        /// <typeparam name="Data">Тип данных в мета-вершине</typeparam>
        /// <typeparam name="Vertex">Тип мета-вершины</typeparam>
        /// <typeparam name="Graph">Тип графа</typeparam>
        /// <param name="cfg">Граф</param>
        /// <param name="backSource">Начало обратной дуги</param>
        /// <param name="backDest">Конец обратной дуги</param>
        /// <returns>Естественный цикл, соответствующий обратной дуге</returns>
        private static NaturalLoop<Data, Vertex> GetNaturalLoop<Data, Vertex, Graph>(Graph cfg, Vertex backSource, Vertex backDest)
            where Vertex : class, IVertex<Vertex, Data>
            where Graph : IGraph<Vertex, Data>
        {
            NaturalLoop<Data, Vertex> loop = new NaturalLoop<Data, Vertex>(backDest);
            // цикл может состоять из одной вершины
            if (backSource == backDest)
                return loop;
            Stack<Vertex> pathStack = new Stack<Vertex>();
            pathStack.Push(backSource);
            loop.AddVertex(backSource);
            // обходим предшественников и помещаем их в цикл
            while (!(pathStack.Count == 0))
            { 
                Vertex curr = pathStack.Pop();
                foreach (Vertex prev in curr.InVerticies)
                    if (!loop.Vertices.Contains(prev))
                    {
                        loop.AddVertex(prev);
                        pathStack.Push(prev);
                    }
            }
            return loop;
        }
    }
}
