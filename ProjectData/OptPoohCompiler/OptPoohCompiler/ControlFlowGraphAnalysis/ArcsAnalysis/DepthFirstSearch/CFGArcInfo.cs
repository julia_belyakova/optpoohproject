﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.GraphTools;

namespace OptPoohCompiler.ControlFlowGraphAnalysis
{
    /// <summary>
    /// Информация о дуге графа потока управления
    /// </summary>
    /// <typeparam name="Data">Тип данных в вершинах графа</typeparam>
    /// <typeparam name="Vertex">Тип вершин графа</typeparam>
    /// <typeparam name="Graph">Граф</typeparam>
    public class CFGArcInfo<Data, Vertex, Graph>
        where Vertex : IVertex<Vertex, Data>
        where Graph  : IGraph<Vertex, Data>
    {
        /// <summary>
        /// Тип дуги (наступающая, отступающая, поперечная)
        /// </summary>
        private CFGArcKind kind;
        /// <summary>
        /// Является ли дуга обратной
        /// </summary>
        private bool backward;

        /// <summary>
        /// Тип дуги (наступающая, отступающая, поперечная)
        /// </summary>
        public CFGArcKind Kind
        {
            get { return kind; }
            /*set 
            {
                if (value != CFGArcKind.UNDEFINED)
                    kind = value;
                else
                    throw new ArgumentException("Нельзя задать дуге неопределнный тип");
            }*/
        }
        /// <summary>
        /// Является ли дуга обратной
        /// </summary>
        public bool Backward
        {
            get { return backward; }
        }

        /// <summary>
        /// Информация о дуге графа потока управления
        /// </summary>
        /// <param name="kind">Тип дуги (наступающая, отступающая, поперечная)</param>
        /// <param name="backward">Является ли дуга обратной</param>
        public CFGArcInfo(CFGArcKind kind, bool backward)
        {
            this.kind = kind;
            this.backward = backward;
        }

        public override string ToString()
        {
            return string.Format("{0}{1}, {2}{3}", "{", kind,
                (backward ? "back" : "not back"), "}");
        }
    }
}
