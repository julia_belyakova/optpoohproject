﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.GraphTools;
using OptPoohCompiler.BasicBlocks;
using OptPoohCompiler.ControlFlowGraphAnalysis.DominationRelation;

using System.Diagnostics;

namespace OptPoohCompiler.ControlFlowGraphAnalysis
{
    /// <summary>
    /// Информация о дугах графа
    /// </summary>
    /// <typeparam name="Data">Тип данных в вершинах графа</typeparam>
    /// <typeparam name="Vertex">Тип вершин графа</typeparam>
    /// <typeparam name="Graph">Граф</typeparam>
    public class DFSTAndArcsCFGData<Data, Vertex, Graph>
        where Vertex : IVertex<Vertex, Data>
        where Graph : IGraph<Vertex, Data>
    {
        /// <summary>
        /// Граф 
        /// </summary>
        private Graph cfg;
        /// <summary>
        /// Информация об отношении доминации в графе
        /// </summary>
        private DominationInCFGData<Data, Vertex, Graph> domData;

        /// <summary>
        /// Нумерация вершин в соответствии с глубинным остовным деревом
        /// </summary>
        private Dictionary<Vertex, int> dfstNumeration =
            new Dictionary<Vertex, int>();
        /// <summary>
        /// Дуги графа CFG и информация о них
        /// </summary>
        private Dictionary<Vertex, Dictionary<Vertex, CFGArcInfo<Data, Vertex, Graph>>> arcsInfos =
            new Dictionary<Vertex, Dictionary<Vertex, CFGArcInfo<Data, Vertex, Graph>>>();

        /// <summary>
        /// Информация о графе 
        /// </summary>
        /// <param name="CFG">Граф потока</param>
        public DFSTAndArcsCFGData(Graph CFG)
        {
            if (CFG == null)
                throw new ArgumentNullException("Граф CFG не должен быть null");
            this.cfg = CFG;
            domData = new DominationInCFGData<Data, Vertex, Graph>(cfg);
            _FillDFSTNumeration();
            _FillArcsInfo();
        }

        /// <summary>
        /// Возвращает номер вершины в соответствии с нумерацией в 
        /// глубинном остовном дереве
        /// </summary>
        /// <param name="vertex">Вершина графа</param>
        /// <returns>Номер вершины в соответствии с нумерацией</returns>
        public int GetVertexNumber(Vertex vertex)
        {
            Debug.Assert(dfstNumeration.ContainsKey(vertex), string.Format(
                "Номер вершины по DFST: вершина {0} не принадлежит графу", vertex));
            return dfstNumeration[vertex];
        }

        /// <summary>
        /// Получает информацию о дуге графа (source -> dest)
        /// если она (дуга) есть (в графе).
        /// </summary>
        /// <param name="source">Вершина-источник</param>
        /// <param name="dest">Вершина-приемник</param>
        /// <returns>Информация о дуге</returns>
        public CFGArcInfo<Data, Vertex, Graph> GetArcInfo(Vertex source, Vertex dest)
        {
            return (arcsInfos[source])[dest];
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Vertex source in cfg.SortedVerticies)
            {
                if (arcsInfos.ContainsKey(source))
                    foreach (KeyValuePair<Vertex, CFGArcInfo<Data, Vertex, Graph>> dest in arcsInfos[source])
                        sb.AppendLine(string.Format("{0} -> {1}: {2}",
                            source.ShortString(), dest.Key.ShortString(), dest.Value));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Заполняет нумерацию вершин графа
        /// </summary>
        private void _FillDFSTNumeration()
        {
            for (int i = 0; i < cfg.SortedVerticies.Count; ++i)
                dfstNumeration.Add(cfg.SortedVerticies[i], i);
        }

        /// <summary>
        /// Заполняет информацию о дугах графа
        /// </summary>
        private void _FillArcsInfo()
        {
            for (int i = 0; i < cfg.SortedVerticies.Count; ++i)
            {
                Vertex source = cfg.SortedVerticies[i];
                foreach (Vertex dest in source.OutVerticies)
                    _ProcessArc(source, dest);
            }
        }

        /// <summary>
        /// Вносит необходимые данные о дуге графа source -> dest
        /// </summary>
        /// <param name="source">Вершина-источник</param>
        /// <param name="dest">Вершина-приемник</param>
        private void _ProcessArc(Vertex source, Vertex dest)
        {
            CFGArcKind kind;
            bool backward;
            // наступающая дуга -- дуга графа от предка к потомку
            // в глубинном остовном дереве
            if (cfg.DFST.FirstIsAncestorOfSecond(source, dest))
                kind = CFGArcKind.ADVANCING;
            // если дуга от потомка к предку по дереву, то
            // она отступающая
            else if (cfg.DFST.FirstIsAncestorOfSecond(dest, source))
                kind = CFGArcKind.RETREATING;
            else
                kind = CFGArcKind.CROSS;
            // обратная дуга определяется доминированием
            backward = domData.FirstDominatesSecond(dest, source);// TODO
            // добавляем информацию
            Dictionary<Vertex, CFGArcInfo<Data, Vertex, Graph>> arcInfo;
            if (arcsInfos.ContainsKey(source))
                arcInfo = arcsInfos[source];
            else
            {
                arcInfo = new Dictionary<Vertex, CFGArcInfo<Data, Vertex, Graph>>();
                arcsInfos.Add(source, arcInfo);
            }
            arcInfo.Add(dest, new CFGArcInfo<Data, Vertex, Graph>(kind, backward));
        }
    }
}
