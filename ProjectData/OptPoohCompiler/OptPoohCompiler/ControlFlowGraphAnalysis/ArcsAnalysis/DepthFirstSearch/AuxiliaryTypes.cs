﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptPoohCompiler.ControlFlowGraphAnalysis
{
    /// <summary>
    /// Тип дуги CFG: наступающая, отступающая, поперечная
    /// </summary>
    public enum CFGArcKind { UNDEFINED, ADVANCING, RETREATING, CROSS };


}
