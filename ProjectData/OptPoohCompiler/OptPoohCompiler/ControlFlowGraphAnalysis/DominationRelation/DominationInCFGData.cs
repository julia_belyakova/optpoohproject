﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.GraphTools;
using OptPoohCompiler.BasicBlocks;

namespace OptPoohCompiler.ControlFlowGraphAnalysis.DominationRelation
{
    /// <summary>
    /// Информация о доминации вершин в графе
    /// </summary>
    /// <typeparam name="Data">Тип данных в вершинах графа</typeparam>
    /// <typeparam name="Vertex">Тип вершин графа</typeparam>
    /// <typeparam name="Graph">Граф</typeparam>
    public class DominationInCFGData<Data, Vertex, Graph>
        where Vertex : IVertex<Vertex, Data>
        where Graph : IGraph<Vertex, Data>
    {
        Graph cfg;

        Dictionary<Vertex, HashSet<Vertex>> dominators;               //Словарь доминаторов. Хранит доминаторов для каждой вершины              

        Dictionary<Vertex, HashSet<Vertex>> treeOfDominator;          //Дерево доминаторов. Для каждой вершины хранит список тех вершин, над которыми данная непосредственно доминирует.

        public Graph CFG
        {
            get
            {
                return cfg;
            }
            set
            {
                cfg = value;
                EnumerateDominators();                                                              //Если изменился граф потока управления, то и Доминаторов надо пересчитать
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void EnumerateDominators()
        {
            //Создаём словарь и дерево(которое, по факту, тоже словарь) доминаторов, предполагая, что для каждой вершины мы найдём доминатора. (Вроде бы должно быстрее работать, если заранее предупредить словарь о том, сколько у нас будет элементов)
            dominators = new Dictionary<Vertex, HashSet<Vertex>>(cfg.SortedVerticies.Count);
            treeOfDominator = new Dictionary<Vertex, HashSet<Vertex>>(cfg.SortedVerticies.Count);

            foreach (Vertex item in cfg.SortedVerticies)
            {
                dominators.Add(item, new HashSet<Vertex>(cfg.SortedVerticies));              //Для начала считаем, что все над всеми доминируют, так как потом будем исключать лишних

                treeOfDominator.Add(item, new HashSet<Vertex>());
            }
            HashSet<Vertex> INVertices = new HashSet<Vertex>();                       //Пустой хешсет с вершинами, что бы с чего-то начать.
            FindDominators(cfg.InputVertex, INVertices);                                            //Вызов Функции поиска всех доминаторов для каждой вершины, начиная с первой

            DominatorTreeCreation();                                                                //Вызов функции генерации дерева доминаторов по найденым спискам доминаторов для каждой вершины
        }

        /// <summary>
        /// Функция поиска доминаторов. Вызывается для первой вершины графа и рекурсивно проходит все вершины, попутно находя всех доминаторов для каждой вершины
        /// </summary>
        /// <param name="vertex">Вершина с которой нужно начать рекурсивный поиск</param>
        /// <param name="INVertices">Список предполагаемых доминаторов данной вершины</param>
        /// <remarks>Функция ничего не возвращает, а изменяет словарь доминаторов <code>dominators</code></remarks>
        void FindDominators(Vertex vertex, HashSet<Vertex> INVertices)
        {
            dominators[vertex].IntersectWith(INVertices);                                           //Находим пересечение переданых в функцию предков с предполагаемыми доминаторами

            HashSet<Vertex> descs = new HashSet<Vertex>(vertex.OutVerticies);         //Создаём хешсет с вершинами-потомками данной
            descs.ExceptWith(INVertices);                                                           //и удаляем из него те, из которых мы сюда пришли, что бы не ходить кругами.

            HashSet<Vertex> adds = new HashSet<Vertex>();
            adds.Add(vertex);                                                                       //Создаём список с текущей вершиной                                                                

            IEnumerable<Vertex> OUTVertices = dominators[vertex].Union(adds);                //Добавляем к доминаторам текущую вершину, надеясь, что она тоже над кем-то доминирует

            foreach (Vertex item in descs)                                                   //Вызываем Функцию для каждого потомка, который не является в тоже время и предком вершины
                FindDominators(item, new HashSet<Vertex>(OUTVertices));                      //Передаём в функцию копию выходных данных данной вершины, что бы можно было их там смело изменять
        }


        /// <summary>
        /// Функция находящая для каждого доминатора те вершины, над которыми он непосредственно доминирует
        /// </summary>
        void DominatorTreeCreation()
        {
            foreach (KeyValuePair<Vertex, HashSet<Vertex>> doms in dominators)    //Обходим все вершины
            {
                if (doms.Value.Count > 0)                                                       //Если у вершины нет доминаторов, то разговор с ней окончен
                {
                    Vertex cgv = doms.Value.First();                                     //Выбираем непосредственным доминатором первого попавшегося доминатора данной вершины

                    foreach (Vertex gVertex in doms.Value)                               //Проходим по всем доминаторам вершины
                    {
                        if (FirstDominatesSecond(cgv,gVertex))                                  //и если находим ту, над которой доминирует предполагаемый непосредственный доминатор
                        {
                            cgv = gVertex;                                                      //то выбераем непосредственным доминатором её
                        }
                    }
                                                                                                //Найдя непосредственного доминатора для данной вершины
                    treeOfDominator[cgv].Add(doms.Key);                                         //добавляем текущую вершину к ней в список непосредственных потомков в дереве доминаторов.
                }
            }
        }

        /// <summary>
        /// Информация о доминации вершин в графе потока управления
        /// </summary>
        /// <param name="cfg">Граф потока управления</param>
        public DominationInCFGData(Graph cfg)
        {
            this.cfg = cfg;
            EnumerateDominators();
        }

        /// <summary>
        /// Определяет, является ли вершина first доминатором second
        /// (доминирует ли first над second)
        /// </summary>
        /// <param name="first">Вершина графа</param>
        /// <param name="second">Вершина графа</param>
        /// <returns>Истину, если вершина first доминирует над вершиной second</returns>
        public bool FirstDominatesSecond(Vertex first, Vertex second)
        {
            if (!cfg.SortedVerticies.Contains(first))
                throw new ArgumentException("Вершина first не пренадлежит графу потока управления.", "first");      //Если first не пренадлежит нашему cfg, то сие действо бессмысленно
            if (!cfg.SortedVerticies.Contains(second))
                throw new ArgumentException("Вершина second не пренадлежит графу потока управления.", "second");    //Если second не пренадлежит нашему cfg, то сие действо столь же бессмысленно, как и предыдущее
            //Хотя я подумываю над тем, что бы заменить эти исключения на возвращение false;
            return dominators[second].Contains(first);
        }
    }
}











//////////////////////////////////Код, который пока не нужен, но выбрасывать жалко

//if (second.InVerticies.Count == 0) return false;                                                        //Если у second нет предков, то вряд-ли над ним кто-то доминирует


//if (second.InVerticies.Contains(first))                                                                 //Если first непосредственный предок second, тогда
//    if (second.InVerticies.Count == 1)                                                                  //Если он единственный, то доминирует
//        return true;
//    else                                                                                                //Иначе нет
//        return false;

//bool isDom = true;
//foreach (GraphBBVertex vertex in second.InVerticies)                                                    //Просто перебор всех предков и проверка
//    isDom = isDom && FirstDominatesSecond(first, vertex);                                               //на предмет того, что бы над каждым из них доминировал first