﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OptPoohCompiler.GraphTools;

namespace OptPoohCompiler.ControlFlowGraphAnalysis
{
    /// <summary>
    /// Статический класс методов анализа графа потока управления
    /// </summary>
    public static class CFGAnalysisTools
    {
        /// <summary>
        /// Проверяет приводимость графа
        /// </summary>
        /// <param name="cfg">Граф потока управления</param>
        /// <returns>Истину, если граф приводим, и ложь в противном случае</returns>
        public static bool IsReducible<Data, Vertex, Graph>(Graph cfg)
            where Vertex : IVertex<Vertex, Data>
            where Graph : IGraph<Vertex, Data>
        {
            // данные о дугах графа
            DFSTAndArcsCFGData<Data, Vertex, Graph> arcsInfo =
                new DFSTAndArcsCFGData<Data, Vertex, Graph>(cfg);

            // ControlFlowGraphAnalysis.CFGArcInfo
            for (int i = 0; i < cfg.SortedVerticies.Count; ++i)
            {
                Vertex source = cfg.SortedVerticies[i];
                foreach (Vertex dest in source.OutVerticies)
                    if ((arcsInfo.GetArcInfo(source, dest).Kind == CFGArcKind.RETREATING) &&
                         (!arcsInfo.GetArcInfo(source, dest).Backward))
                        return false;
            }
            return true;
        }
    }
}
